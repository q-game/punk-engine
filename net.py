# encoding: utf-8
import json
from datetime import datetime

import pykka
import gevent
import zmq.green as zmq
from pykka.gevent import GeventActor as Actor

import logging
log = logging.getLogger('[net]')
log.addHandler(logging.NullHandler())

#log.setLevel(logging.DEBUG)
#log.addHandler(logging.StreamHandler())
#log.debug('test')

zmq_ctx = zmq.Context.instance()

class NetBase(Actor):

    def _close_socket(self):
        self.sock.setsockopt(zmq.LINGER, 0)
        self.sock.close()

    def on_stop(self):
        self._close_socket()

    def on_failure(self, exception_type, exception_value, traceback):
        #import pudb; pu.db
        log.error("Requester %s failed: %s: %s" % (self.address, exception_type, exception_value))
        self._close_socket()


class Requester(NetBase):
    def __init__(self, address):
        super(Requester, self).__init__()
        self.address = address

    def on_start(self):
        self.sock = zmq_ctx.socket(zmq.REQ)
        self.sock.setsockopt(zmq.LINGER, 1000)
        self.sock.setsockopt(zmq.SNDHWM, 100)
        self.sock.setsockopt(zmq.RCVHWM, 100)
        self.sock.setsockopt(zmq.REQ_RELAXED, 1)
        self.sock.setsockopt(zmq.REQ_CORRELATE, 1)
        self.sock.connect(self.address)

        self.poller = zmq.Poller()
        self.poller.register(self.sock, zmq.POLLIN|zmq.POLLOUT)

    def request(self, msg, out_timeout=None, in_timeout=None):
        #import pudb; pu.db
        attempts = 1
        poll_timeout = 100
        if out_timeout:
            a, b = divmode(out_timeout, poll_timeout)
            attempts = a + bool(b)

        while attempts:
            socks = dict(self.poller.poll(poll_timeout))
            if self.sock in socks and socks[self.sock] == zmq.POLLOUT:
                #import pudb; pu.db
                self.sock.send_string(json.dumps(msg))
                #log.debug('Requester %s: Successfuly send request: %s' % (self.address, msg))
                break
            else:
                attempts = attempts - 1 if out_timeout else attempts
        else:
            log.error('Requester %s: Failed to send request' % self.address)
            return {'status': 'fail'}

        attempts = 1
        poll_timeout = 100
        if in_timeout:
            a, b = divmode(in_timeout, poll_timeout)
            attempts = a + bool(b)
        else:
            attemtps = 1

        while attempts:
            socks = dict(self.poller.poll(poll_timeout))
            if self.sock in socks and socks[self.sock] == zmq.POLLIN:
                #import pudb; pu.db
                resp = json.loads(self.sock.recv_string())
                #log.debug('Requester %s:Got response: %s' % (self.address, resp))
                return resp
            else:
                attempts = attempts - 1 if out_timeout else attempts
        else:
            log.error('Requester %s: Failed to recv response' % self.address)
            return {'status': 'fail'}


class Responder(NetBase):
    def __init__(self, address, handler, timeout=None):
        super(Responder, self).__init__()
        self.address = address
        self.handler = handler
        self.timeout = timeout/1000.0 if timeout else None

    def proxy(self):
        return self.actor_ref.proxy()

    def on_start(self):
        self.sock = zmq_ctx.socket(zmq.REP)
        self.sock.setsockopt(zmq.LINGER, 1000)
        self.sock.setsockopt(zmq.SNDHWM, 100)
        self.sock.setsockopt(zmq.RCVHWM, 100)
        print self.address
        self.sock.bind(self.address)

        self.poller = zmq.Poller()
        self.poller.register(self.sock, zmq.POLLIN)
        self.proxy().next_request()

    def next_request(self):
        socks = dict(self.poller.poll(100))
        if self.sock in socks and socks[self.sock] == zmq.POLLIN:
            #import pudb; pu.db
            request = json.loads(self.sock.recv_string())
            #log.debug("Responder {0}: Got new request! {1}".format(self.address, request))
            resp = None
            try:
                resp = self.handler.ask({'request': request}, timeout=self.timeout)
            except pykka.Timeout as e:
                log.error("Responder {0}: Timeout! {1}".format(self.address, e))
            except pykka.ActorDeadError as e:
                log.error("Responder {0}: Actor Dead! {1}".format(self.address, e))
            finally:
                self.sock.send_string(json.dumps(resp))

        self.proxy().next_request()


class EchoHandler(Actor):
    def on_receive(self, msg):
        return msg['request']


class Publisher(NetBase):
    def __init__(self, address):
        super(Publisher, self).__init__()
        self.address = address

    def on_start(self):
        self.sock = zmq_ctx.socket(zmq.PUB)
        self.sock.bind(self.address)

    def publish(self, msg):
        self.sock.send_string(json.dumps(msg))

    def log(self, log, log_level='info'):
        #log levels:
        # - error
        # - info
        # - tip
        # - legend
        msg = {
            "type" : "log",
            "data" : log,
            "log_level" : log_level,
            "time": str(datetime.now())
        }
        self.publish(msg)

    def event(self, log):
        msg = {
            "type" : "event",
            "data" : log,
            "time": str(datetime.now())
        }
        self.publish(msg)

    def height(self, h):
        msg = {
            "type" : "height",
            "data" : h,
            "time": str(datetime.now())
        }
        self.publish(msg)

    def controls(self, descr,  controls):
        msg = {
            "type" : "controls",
            "controls" : controls,
            "descr": descr,
            "time": str(datetime.now())
        }
        self.publish(msg)

    def manual_controls(self, descr,  controls):
        msg = {
            "type" : "manual_controls",
            "controls" : controls,
            "descr": descr,
            "time": str(datetime.now())
        }
        self.publish(msg)

    def sound(self, sound, stop=False, repeat=False, echo=False, volume=1):
        msg = {
            "type" : "sound",
            "sound" : sound,
            "stop" : stop,
            "repeat" : repeat,
            "echo" : echo,
            "volume" : volume,
            "time": str(datetime.now())
        }
        self.publish(msg)


    def step_event(self, log):
        msg = {
            "type" : "step_event",
            "data" : log,
            "time": str(datetime.now())
        }
        self.publish(msg)

class Video(NetBase):
    def __init__(self, address):
        super(Video, self).__init__()
        self.address = address

    def on_start(self):
        self.sock = zmq_ctx.socket(zmq.PUB)
        self.sock.bind(self.address)

    def publish(self, msg):
        self.sock.send_string(json.dumps(msg))

    def queue(self, queue):
        msg = {
            "type" : "queue",
            "data" : queue,
            "time": str(datetime.now())
        }
        self.publish(msg)

    def stop_video(self):
        msg = {
            "type" : "system",
            "data" : 'stop',
            "time": str(datetime.now())
        }
        self.publish(msg)


class Subscriber(NetBase):
    def __init__(self, address, handler):
        super(Subscriber, self).__init__()
        self.address = address
        self.handler = handler

    def proxy(self):
        return self.actor_ref.proxy()

    def on_start(self):
        self.sock = zmq_ctx.socket(zmq.SUB)
        self.sock.setsockopt(zmq.SUBSCRIBE, "")
        self.sock.connect(self.address)
        self.poller = zmq.Poller()
        self.poller.register(self.sock, zmq.POLLIN)
        self.proxy().next_request()

    def next_request(self):
        connect = self.poller.poll(100)
        if connect:
            request = json.loads(self.sock.recv())
            #print "Subscriber: Got new pub!"
            #print request
            self.handler.tell({'request': request})
        else:
            #print "Got nothing. Next try"
            pass
        self.proxy().next_request()
