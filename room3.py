# encoding: utf-8
from functools import partial
import itertools

from engine import Resources, shex
from engine import Puzzle
from engine import SensorDevice, OkulyarDevice, ScriptDevice

class Pochinka(Puzzle):
    glossary_name = u"Починка(штука с маленьками красными ветилями)"
    glossary_code = u"G2R3O1"
    #device_addr = 15

    def __init__(self, device_addr1, slot1, device_addr2, slot2,  *args, **kwargs):
        super(Pochinka, self).__init__(*args, **kwargs)
        self.device_addr1 = device_addr1
        self.slot1 = slot1
        self.device_addr2 = device_addr2
        self.slot2 = slot2

        self.valves_map = [[
            'valve00', 'valve01', 'valve02', 'valve03', ], [
            'valve10', 'valve11', 'valve12', 'valve13',
        ]]

        self.turns_needed = 1
        self.bomb_loaded = False

    def on_start(self):
        self.device1 = SensorDevice.start(self.device_addr1, self.slot1).proxy()
        self.device1.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr1,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])
        self.device2 = SensorDevice.start(self.device_addr2, self.slot2).proxy()
        self.device2.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr2,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])
        self.device_dim = ScriptDevice.start(self.device_addr1).proxy()
        self.device_dim.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr1,
                unicode(u"Дым машина")
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def smoke(self):
        self.device_dim.send_script_cmd(0x7101)

    def init(self):
        self.device1.init().get()
        self.device2.init().get()
        self.device_dim.init().get()

    def setup(self):
        pass

    def reset(self):
        self.device1.reset_counter(0)
        self.device1.reset_counter(1)
        self.device1.reset_counter(2)
        self.device1.reset_counter(3)
        self.device1.reset_counter(5)
        self.device2.reset_counter(0)
        self.device2.reset_counter(1)
        self.device2.reset_counter(2)
        self.device2.reset_counter(3)
        self.device1.send_cmd('reset')
        self.device2.send_cmd('reset')

    def activate(self):
        for line in range(0, 4):
            self.device1.subscribe(
                'stat.line{0}_counter_tozero'.format(line),
                lambda data, line=line: self.proxy().valve_turns([0, line], data[0]),
                3000
            ).get()
        for line in range(0, 4):
            self.device2.subscribe(
                'stat.line{0}_counter_tozero'.format(line),
                lambda data, line=line: self.proxy().valve_turns([1, line], data[0]),
                3000
            ).get()

    def activate_bombpriem(self):
        self.device1.subscribe(
            'stat.line5_counter_tozero',
            lambda data: self.proxy().bomb_event(data[0]),
            2000
        ).get()

    def bomb_event(self, bomb):
        print 'bomb_event: {0}'.format(bomb)
        if not self.bomb_loaded:
            if bomb:
                self.bomb_loaded = True
                self.trigger('bomb_loaded')

    trigger_bomb_loaded = lambda self: self.trigger('bomb_loaded')

    def valve_turns(self, valve_coords, turns):
        print valve_coords, turns
        y, x = valve_coords
        if turns == 0:
            return
        #if turns > self.turns_needed:
            #[self.device1, self.device2][y].reset_counter(x)
            #return
        if turns >= self.turns_needed:
            [self.device1, self.device2][y].reset_counter(x)
        self.trigger('{0}_turned'.format(self.valves_map[y][x]), 1)

    def get_all_valves_turns(self):
        all_turns = [None]*8
        all_turns[0] = self.device1.read_regs('stat.line0_counter_tozero').get()[0]
        all_turns[1] = self.device1.read_regs('stat.line1_counter_tozero').get()[0]
        all_turns[2] = self.device1.read_regs('stat.line2_counter_tozero').get()[0]
        all_turns[3] = self.device1.read_regs('stat.line3_counter_tozero').get()[0]
        all_turns[4] = self.device2.read_regs('stat.line0_counter_tozero').get()[0]
        all_turns[5] = self.device2.read_regs('stat.line1_counter_tozero').get()[0]
        all_turns[6] = self.device2.read_regs('stat.line2_counter_tozero').get()[0]
        all_turns[7] = self.device2.read_regs('stat.line3_counter_tozero').get()[0]
        return all_turns

    def deactivate(self):
        for line in range(0, 4):
            self.device1.unsubscribe('stat.line{0}_counter_tozero'.format(line))
        for line in range(0, 4):
            self.device2.unsubscribe('stat.line{0}_counter_tozero'.format(line))
        for valve in itertools.chain(self.valves_map[0], self.valves_map[1]):
            self.unsubscribe('{0}_turned'.format(valve))
        self.device1.unsubscribe('stat.line5_counter_tozero')
        self.unsubscribe('bomb_loaded')

    def stopall(self):
        self.device1.stop()
        self.device2.stop()


class Okulyar(Puzzle):
    glossary_name = u"Окуляры"
    glossary_code = u"G2R3O2"
    #device_addr = 19

    def __init__(self, device_addr, slot=None, **kwargs):
        super(Okulyar, self).__init__(**kwargs)
        self.device_addr = device_addr

        self.hide = False

        self.route1 = [
         (0,0),(0,1),(1,1),
         (2,1),(2,2),(2,3),
         (3,3),(4,3),
         (4,4),(5,4),(6,4),(7,4),(None,None),
         (7,3),(7,2),(7,1),(7,0),
         (6,0),(5,0),(4,0),(3,0),(2,0),(1,0),
        ]
        self.route2 = [
         (8,4),(9,4),(10,4),(11,4),(12,4),(13,4),(14,4),
         (14,3),(14,2),
         (13,2),(12,2),
         (12,1),(12,0),
         (11,0),(10,0),(9,0),
         (9,1),(9,2),(9,3),
         (8,3),(7,3),(6,3),(5,3),
         (5,4),(6,4),(7,4)
        ]
        self.need_to_change_route = False
        self.route_generator = self.gen_route(self.route1)

    def on_start(self):
        self.device = OkulyarDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def gen_route(self, route):
        while True:
            for x, y in route:
                yield x, y

    def init(self):
        self.device.init().get()

    def setup(self):
        pass
        #self.device.write_regs('conf.coords', [90])

    def reset(self):
        self.device.send_cmd('reset')
        self.device.write_regs('conf.coords', [90])

    def activate(self):
        #schedule coords change
        pass

    def disable(self):
        self.device.write_regs('conf.coords', [90])

    def hide_lodka(self):
        self.hide = True

    def unhide_lodka(self):
        self.hide = False

    def coords(self, x, y):
        return 18*y + x

    def move(self, x, y):
        coords = self.coords(x, y)
        self.device.write_regs('conf.coords', [coords])

    def change_route(self):
        self.need_to_change_route = True

    def move_by_route(self, ugol=None):
        if self.hide:
            x, y = 0, 5 #90
        else:
            x, y = next(self.route_generator)
            if x is None and y is None:
                if self.need_to_change_route:
                    self.route_generator = self.gen_route(self.route2)
                x, y = next(self.route_generator)
        self.move(x,y)
        if ugol:
            coords = self.coords(x,y)
            ugol.set_coords(coords)

    def deactivate(self):
        pass

    def stopall(self):
        self.device.stop()


class Konfigurator(Puzzle):
    glossary_name = u"Конфигуратор"
    glossary_code = u"G2R1O0"
    #device_addr = 14

    def __init__(self, device_addr, slot=None, **kwargs):
        super(Konfigurator, self).__init__(**kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = ScriptDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        pass

    def go_to_standby(self):
        self.device.go_to_standby()

    def go_to_game(self):
        self.device.go_to_game()

    def go_to_solved(self):
        self.trigger('solved')
        self.device.go_to_solved()

    def reset(self):
        self.device.send_cmd('reset')

    def activate(self):
        self.device.subscribe(
            'stat.script_status',
            lambda data: self.proxy().status_event(data[0]),
            2000
        ).get()

    def status_event(self, event):
        if event == 2:
            self.trigger('solved')

    def deactivate(self):
        self.device.unsubscribe('stat.script_status')
        self.unsubscribe('solved')

    def stopall(self):
        self.device.stop()


