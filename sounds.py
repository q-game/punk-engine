from utils import Resources

class Sounds(object):
    def __init__(self, debug=False):
        self.debug = debug
        if not debug:
            self.sound = Resources().require('SOUND')
        else:
            self.sound = None
        self.sinks = {
            'z1': [1,2],
            'z2': [7,8],
            'z3': [5],
            'pch': [3,4],
            'trl': [6],
        }

    def setup(self, sound_name):
        if self.debug:
            print "Sounds.setup: {0}".format(sound_name)
            return
        snds = self
        def snd_setup(*args, **kwargs):
            print args, kwargs
            snds.sound.play(sound_name, *args, paused=True, **kwargs)
        def sinks(sinks_string):
            channels = []
            for sink in sinks_string.split():
                channels += self.sinks[sink]
            return channels

        {'achivka': lambda: snd_setup([([1,2], sinks('z3'))], volume=1) ,
         'ochered_1': lambda: snd_setup([([1,2], sinks('trl'))], volume=1) ,
         'ochered_2': lambda: snd_setup([([1,2], sinks('trl'))], volume=1) ,
         'ochered_3': lambda: snd_setup([([1,2], sinks('trl'))], volume=1) ,
         'ochered_4': lambda: snd_setup([([1,2], sinks('trl'))], volume=1) ,
         'osechka_1': lambda: snd_setup([([1,2], sinks('trl'))], volume=1) ,
         'osechka_2': lambda: snd_setup([([1,2], sinks('trl'))], volume=1) ,
         'osechka_3': lambda: snd_setup([([1,2], sinks('trl'))], volume=1) ,
         'osechka_4': lambda: snd_setup([([1,2], sinks('trl'))], volume=1) ,
         'sbros_bombi_1': lambda: snd_setup([([1,2], sinks('z3'))], volume=0.4) ,
         'sbros_bombi_2': lambda: snd_setup([([1,2], sinks('z3'))], volume=0.4) ,
         'sbros_bombi_3': lambda: snd_setup([([1,2], sinks('z3'))], volume=0.4) ,
         'sbros_bombi_4': lambda: snd_setup([([1,2], sinks('z3'))], volume=0.4) ,
         'v_nas_popali_1': lambda: snd_setup([([1,2], sinks('z3 trl'))], volume=0.4) ,
         'v_nas_popali_2': lambda: snd_setup([([1,2], sinks('z3 trl'))], volume=0.4) ,
         'v_nas_popali_3': lambda: snd_setup([([1,2], sinks('z3 trl'))], volume=0.4) ,
         'v_nas_popali_4': lambda: snd_setup([([1,2], sinks('z3 trl'))], volume=0.4) ,
         'perezaryadka': lambda: snd_setup([([1,2], sinks('trl'))], loop=True) ,
         'pervoe_popadanie': lambda: snd_setup([([1,2], sinks('z1'))]) ,
         'promah': lambda: snd_setup([([1,2], sinks('z3'))]) ,
         'sbros_bombi': lambda: snd_setup([([1,2], sinks('z3'))]) ,
         #'tick': lambda: snd_setup([([1,2], sinks('z1'))]) ,
         'v_nas_popali': lambda: snd_setup([([1,2], sinks('z3'))]) ,
         'vtoroe_popadanie': lambda: snd_setup([([1,2], sinks('z3'))]) ,
         }.get(sound_name)()

    def resume(self, sound_name):
        if self.debug:
            print "Sounds.resume: {0}".format(sound_name)
            return
        snds = self
        def snd_resume(*args, **kwargs):
            snds.sound.resume(sound_name)

        {'achivka': lambda: snd_resume(),
         'ochered_1': lambda: snd_resume(),
         'ochered_2': lambda: snd_resume(),
         'ochered_3': lambda: snd_resume(),
         'ochered_4': lambda: snd_resume(),
         'osechka_1': lambda: snd_resume(),
         'osechka_2': lambda: snd_resume(),
         'osechka_3': lambda: snd_resume(),
         'osechka_4': lambda: snd_resume(),
         'sbros_bombi_1': lambda: snd_resume() ,
         'sbros_bombi_2': lambda: snd_resume() ,
         'sbros_bombi_3': lambda: snd_resume() ,
         'sbros_bombi_4': lambda: snd_resume() ,
         'v_nas_popali_1': lambda: snd_resume() ,
         'v_nas_popali_2': lambda: snd_resume() ,
         'v_nas_popali_3': lambda: snd_resume() ,
         'v_nas_popali_4': lambda: snd_resume() ,
         'perezaryadka': lambda: snd_resume(),
         'pervoe_popadanie': lambda: snd_resume(),
         'promah': lambda: snd_resume(),
         'sbros_bombi': lambda: snd_resume(),
         #'tick': lambda: snd_resume(),
         'v_nas_popali': lambda: snd_resume(),
         'vtoroe_popadanie': lambda: snd_resume(),
         }.get(sound_name)()

    def pause(self, sound_name):
        if self.debug:
            print "Sounds.pause: {0}".format(sound_name)
            return
        snds = self
        def snd_pause(*args, **kwargs):
            snds.sound.pause(sound_name)
        {'perezaryadka': lambda: snd_pause(),
         }.get(sound_name)()

    def play(self, sound_name):
        if self.debug:
            print "Sounds.play: {0}".format(sound_name)
            return
        snds = self
        def snd_play(*args, **kwargs):
            snds.sound.play(sound_name, *args, **kwargs)
        def sinks(sinks_string):
            channels = []
            for sink in sinks_string.split():
                channels += self.sinks[sink]
            return channels

        {
         'achivka': lambda: snd_play([([1,2], sinks('z1 z2 z3'))], volume=1) ,
         'nepravilnoe_poleno': lambda:  snd_play([([1,2], sinks('pch'))], volume=1) ,
         'pravilnoe_poleno': lambda: snd_play([([1,2], sinks('pch'))], volume=1) ,
         'topka_reshena': lambda: snd_play([([1,2], sinks('pch'))], loop=True, volume=1) ,
         'boevaya_tema_v1': lambda: snd_play([([1,2], sinks('z3'))], loop=True, volume=0.3),
         'bolshaya_visota': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'bolshaya_visota_eng': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'cel_ne_ustanovlena': lambda: snd_play([([1,2], sinks('z3'))], volume=0.4),
         'cel_ne_ustanovlena_eng': lambda: snd_play([([1,2], sinks('z3'))], volume=0.4),
         'dont_push_30s': lambda: snd_play([([1,2], sinks('z1 z2 z3'))], volume=0.4),
         'dont_push_30s_eng': lambda: snd_play([([1,2], sinks('z1 z2 z3'))], volume=0.4),
         'final_crash_3min': lambda: snd_play([([1,2], sinks('z2 z3 '))], loop=True, volume=0.4),
         'final_crash_3min_eng': lambda: snd_play([([1,2], sinks('z2 z3'))], loop=True, volume=0.4),
         'final_crash_posadka': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'final_crash_posadka_eng': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'moyka': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'moyka_eng': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'final_crash_vals': lambda: snd_play([([1,2], sinks('z1'))], loop=True, volume=0.4),
         'glavnaya_tema_v1': lambda: snd_play([([1,2], sinks('z1'))], loop=True, volume=0.015),
         'help1': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'help1_eng': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'help2': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'help2_eng': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'help3': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'help3_eng': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'help4': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'help4_eng': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'malaya_visota': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'malaya_visota_eng': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'manual1': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'manual1_eng': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'manual2': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'manual2_eng': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'goal_reminder': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'goal_reminder_eng': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'manual3': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'manual3_eng': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'naberite_visote_eng': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'naberite_visotu': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'ne_pochinen_fuzelyazh': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'ne_pochinen_fuzelyazh_eng': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'nedostatochnaya_visota_dlya_mayaka': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'nedostatochnaya_visota_dlya_mayaka_eng': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'net_mayaka': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'net_mayaka_eng': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'pochinite_fuzelyazh': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'pochinite_fuzelyazh_eng': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'pochinite_fuzelyazh_i_naberite_visotu': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'pochinite_fuzelyazh_i_naberite_visotu_eng': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'polet': lambda: snd_play([([1,2], sinks('z1'))], loop=True),
         'proshel_chas': lambda: snd_play([([1,2], sinks('z1 z2 z3'))], volume=0.4),
         'proshel_chas_eng': lambda: snd_play([([1,2], sinks('z1 z2 z3'))], volume=0.4),
         'pervoe_popadanie': lambda: snd_play([([1,2], sinks('z3'))], volume=0.4) ,
         'vtoroe_popadanie': lambda: snd_play([([1,2], sinks('z3'))], volume=0.4) ,
         'promah': lambda: snd_play([([1,2], sinks('z3'))]) ,
         'replika1sh': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'replika1sh_eng': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'replika2sh': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'replika2sh_eng': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'replika3sh': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'replika3sh_eng': lambda: snd_play([([1,2], sinks('z1 z3'))], volume=0.4),
         'sirena': lambda: snd_play([([1,2], sinks('z1 z3'))], loop=True, volume=0.4),
         'sbros_bombi': lambda: snd_play([([1,2], sinks('z3'))]) ,
         'v_nas_popali': lambda: snd_play([([1,2], sinks('z3'))], volume=1) ,
         'shum_machinnogo_zala_2min': lambda: snd_play([([1,2], sinks('z2'))], loop=True),
         'shum_machinnogo_zala_start': lambda: snd_play([([1,2], sinks('z2'))]),
         'strong_wind': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'strong_wind_eng': lambda: snd_play([([1,2], sinks('z1'))], volume=0.4),
         'tresk_v_topke_2_min': lambda: snd_play([([1,2], sinks('pch'))], loop=True, volume=1),
         'vseh_razbili_na_severo_vostoke_sh': lambda: snd_play([([1,2], sinks('z1'))]),
         'vzlet': lambda: snd_play([([1,2], sinks('z1'))]),
         }.get(sound_name)()

    def stop_play(self, sound_name):
        if self.debug:
            print "Sounds.stop_play: {0}".format(sound_name)
            return
        self.sound.stop_play(sound_name)

    def snd_ochered(self):
        def next_snd(n):
            return 1 if n==4 else n+1

        if getattr(self, 'ochered_snd_n', None) is None:
            self.ochered_snd_n = 1
        self.resume('ochered_{0}'.format(self.ochered_snd_n))
        self.setup('ochered_{0}'.format(next_snd(self.ochered_snd_n)))
        self.setup('ochered_{0}'.format(next_snd(next_snd(self.ochered_snd_n))))
        self.ochered_snd_n = next_snd(self.ochered_snd_n)

    def snd_osechka(self):
        print 'snd_osechka'
        def next_snd(n):
            return 1 if n==4 else n+1

        if getattr(self, 'osechka_snd_n', None) is None:
            self.osechka_snd_n = 1
        self.resume('osechka_{0}'.format(self.osechka_snd_n))
        self.setup('osechka_{0}'.format(next_snd(self.osechka_snd_n)))
        self.setup('osechka_{0}'.format(next_snd(next_snd(self.osechka_snd_n))))
        self.osechka_snd_n = next_snd(self.osechka_snd_n)

    def snd_sbros_bombi(self):
        print 'snd_sbros_bombi'
        def next_snd(n):
            return 1 if n==4 else n+1

        if getattr(self, 'sbros_bombi_snd_n', None) is None:
            self.sbros_bombi_snd_n = 1
        self.resume('sbros_bombi_{0}'.format(self.sbros_bombi_snd_n))
        self.setup('sbros_bombi_{0}'.format(next_snd(self.sbros_bombi_snd_n)))
        self.setup('sbros_bombi_{0}'.format(next_snd(next_snd(self.sbros_bombi_snd_n))))
        self.sbros_bombi_snd_n = next_snd(self.sbros_bombi_snd_n)

    def snd_v_nas_popali(self):
        print 'snd_v_nas_popali'
        def next_snd(n):
            return 1 if n==4 else n+1

        if getattr(self, 'v_nas_popali_snd_n', None) is None:
            self.v_nas_popali_snd_n = 1
        self.resume('v_nas_popali_{0}'.format(self.v_nas_popali_snd_n))
        self.setup('v_nas_popali_{0}'.format(next_snd(self.v_nas_popali_snd_n)))
        self.setup('v_nas_popali_{0}'.format(next_snd(next_snd(self.v_nas_popali_snd_n))))
        self.v_nas_popali_snd_n = next_snd(self.v_nas_popali_snd_n)


