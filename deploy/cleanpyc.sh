#!/bin/sh -e

cd `dirname "$0"`;
cd ../;
find . -name '*.pyc' -delete;
find . -name '*.py~' -delete;
find . -name '*~' -delete;
