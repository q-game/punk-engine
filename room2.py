# encoding: utf-8
"""
В общем печь - обычные скриптовые регистры + флаг ачивки
struct script_common_status_struct{
    uint16_t status; // Статус скрипта. 0 - не запущен, 1 - выполняется, 2 - завершен
    uint16_t step; // Номер текущего шага
} common;
uint32_t achivka; // Флаг ачивки

Статус - при записи нуля сбрасывает все в ноль,
в т.ч. покзаометр и переходит автоматом в статус 1 - тоесть запускет загадку.

step равен 1, если вставлено полено (любое), иначе 0.
Это нужно для того, чтобы ты запускал звук неправильного полена

achivka по дефолту в нуле, если вставили полено собранное по конфигурации ачивки,
то туда пишется 1 до того как загадка не будет сброшена
"""
from functools import partial
import itertools

from engine import Resources, shex
from engine import Puzzle
from engine import PechDevice, ScriptDevice, LoadSDevice, SerialDevice
from engine import MindDevice, BalansDevice


class Pech(Puzzle):
    glossary_name = u"Печь(топка)"
    glossary_code = u"G2R2O2"
    #device_addr = 11

    def __init__(self, device_addr, slot=None, **kwargs):
        super(Pech, self).__init__(**kwargs)
        self.device_addr = device_addr

        self.got_achivka = False

    def on_start(self):
        self.device = PechDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        pass

    def go_to_game(self):
        self.device.go_to_game()

    def go_to_solved(self):
        self.trigger('solved')
        self.device.go_to_solved()

    def reset(self):
        self.device.send_cmd('reset')

    def activate(self):
        self.device.subscribe(
            'stat.script_step',
            lambda data: self.proxy().check_poleno(data[0]),
            1500
        ).get()

    def check_poleno(self, poleno):
        if poleno:
            if not self.got_achivka:
                achivka = self.device.read_regs('stat.achivka').get()[0]
                if achivka:
                    self.got_achivka = True
                    self.trigger('achivka')

            status = self.device.read_regs('stat.script_status').get()[0]
            if status == 2: #solved
                self.trigger('solved')
            elif status == 1: #wrong poleno
                self.trigger('wrong_poleno')

    def smoke_enable(self):
        self.device.write_regs('conf.smoke',[1])

    def smoke_disable(self):
        self.device.write_regs('conf.smoke',[0])

    def compressor_enable(self):
        self.device.write_regs('conf.compressor',[1])

    def compressor_disable(self):
        self.device.write_regs('conf.compressor',[0])


    def deactivate(self):
        self.device.unsubscribe('stat.script_step')
        self.unsubscribe('achivka')
        self.unsubscribe('solved')
        self.unsubscribe('wrong_poleno')

    def stopall(self):
        self.device.stop()


class Balans(Puzzle):
    glossary_name = u"Балансировка"
    glossary_code = u"G2R1O1"
    #device_addr = 9

    def __init__(self, device_addr, slot=None, **kwargs):
        super(Balans, self).__init__(**kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = BalansDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        pass

    def go_to_game(self):
        self.device.go_to_game()

    def go_to_solved(self):
        self.trigger('solved')
        self.device.go_to_solved()

    def reset(self):
        self.device.send_cmd('reset')

    def activate(self):
        self.device.subscribe(
            'stat.script_status',
            lambda data: self.proxy().status_event(data[0]),
            1500
        ).get()
        self.device.subscribe(
            'stat.ach_left',
            lambda data: self.proxy().achivka(data, 'energy'),
            1000
        ).get()
        self.device.subscribe(
            'stat.ach_right',
            lambda data: self.proxy().achivka(data, 'fire'),
            1000
        ).get()

    def achivka(self, event, ach):
        if event:
            self.trigger('ach_'+ach)

    def status_event(self, event):
        if event == 2:
            self.trigger('solved')

    def deactivate(self):
        self.device.unsubscribe('stat.script_status')
        self.device.unsubscribe('stat.ach_left')
        self.device.unsubscribe('stat.ach_right')
        self.unsubscribe('solved')

    def stopall(self):
        self.device.stop()


class Mastermind(Puzzle):
    glossary_name = u"Мастермайнд"
    glossary_code = u"G2R1O3"

    def __init__(self, device_addr, slot=None, **kwargs):
        super(Mastermind, self).__init__(**kwargs)
        self.device_addr = device_addr

        self.suppress_achivki = False

    def on_start(self):
        self.device = MindDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        pass

    def go_to_game(self):
        self.device.go_to_game()

    def go_to_solved(self):
        self.trigger('solved', 20)
        self.suppress_achivki = True
        self.device.go_to_solved()

    def reset(self):
        self.device.send_cmd('reset')

    def activate(self):
        self.device.subscribe(
            'stat.script_status',
            lambda data: self.proxy().status_event(data[0]),
            2000
        ).get()

    def status_event(self, event):
        print 'mastermind status_event: {0}'.format(event)
        if not self.suppress_achivki:
            if event == 2:
                retries = self.device.read_regs('stat.retries').get()[0]
                self.trigger('solved', retries)

    def deactivate(self):
        self.device.unsubscribe('stat.script_status')
        self.unsubscribe('solved')

    def stopall(self):
        self.device.stop()

class Shluz(Puzzle):
    glossary_name = u"Шлюз в боевую зону"
    glossary_code = u"G2R1O4"

    def __init__(self, device_addr, slot, *args, **kwargs):
        super(Shluz, self).__init__(*args, **kwargs)
        self.device_addr = device_addr
        self.slot = slot

    def on_start(self):
        self.device = LoadSDevice.start(self.device_addr, self.slot).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.setup_line(0,value=1)
        self.device.setup_line(1)

    def reset(self):
        self.device.send_cmd('reset')

    def open_shluz(self):
        self.device.line_value(0, 0)

    def close_shluz(self):
        self.device.line_value(0, 1)

    def plate_on(self):
        self.device.line_value(1, 1)

    def plate_off(self):
        self.device.line_value(1, 0)


class Mahovik(Puzzle):
    glossary_name = u"Маховик"
    glossary_code = u"G2R1O1"

    def __init__(self, device_addr, slot, *args, **kwargs):
        super(Mahovik, self).__init__(*args, **kwargs)
        self.device_addr = device_addr
        self.slot = slot
        self.lines_map = [0, 0, 0, 0]

    def on_start(self):
        self.device = SerialDevice.start(self.device_addr, self.slot).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        pass

    def reset(self):
        self.device.send_cmd('reset')
        #self.lines_map = [0, 0, 0, 0]
        #self.device.set_lines(0, self.lines_map)

    def on(self):
        self.lines_map[0] = 10
        self.device.set_lines(0, self.lines_map)

    def off(self):
        self.lines_map[0] = 0
        self.device.set_lines(0, self.lines_map)

    def sidushki_svet_on(self):
        self.lines_map[1] = 10
        self.device.set_lines(0, self.lines_map)

    def sidushki_svet_off(self):
        self.lines_map[1] = 0
        self.device.set_lines(0, self.lines_map)

    def klapan_on(self):
        self.lines_map[2] = 10
        self.device.set_lines(0, self.lines_map)

    def klapan_off(self):
        self.lines_map[2] = 0
        self.device.set_lines(0, self.lines_map)

    def deactivate(self):
        pass

    def stopall(self):
        self.device.stop()


