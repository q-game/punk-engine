# encoding: utf-8
"""
Думаю общий принцип тогда будет такой:
1)Ты в окуляры и в угол наводчика раз в секунду(примерно) пишешь новую координату 0..89.
    На окулярах она просто отображается светящейся лампой и все, забываем про них.
    В угле наводчика факт записи новой координаты означает "+1 шаг"
2)также в угол наводчика ты пишешь высоту 0..5000.
    Каждая тысяча увеличивает длительность полета бомбы на один шаг.
3)При нажатии кнопки сброса бомбы угол наводчика запоминает текущую высоту(например 3000)
    и начинает считать 3 шага(или сколько нужно). После трех шагов он проверяет - совпадает
    ли выбранная с помощью кнопки координата с той, которая была записана на этом шаге.
    Если нет, значит промах, если да - значит попали
    В этот момент в какой-нибудь регистр выставляется статус - миио или попали
4)После падения бомбы начинается перезарядка.
    По окончанию перезарядки статус попадания снимается и мы возвращаемся в начало.

Значения, которые тебе надо писать:
    1)Текущая высота
    2)Текущаяя координата
    3)Скорострельность(время перезарядки)

Значения которые тебе надо читать:
    1)Факт нажатия кнопки(для запуска видео/звука)
    2)факт попадания/не попадания бомбы(для видео/звука/сценария)


А, да, еще там же будут кнопки "Помощь" и "Не нажимать".
Кстати, они подключены к SERIAL-INPUT, так что придется тебе с ним все-таки работать. ну или если хочешь, могу просто спец регистры в скрипте под них сделать - как тебе проще

// Статус. Единственное отличие статуса 0 и 1 - в статусе 0 не срабатывает нажатие кнопки "Стрельба", в статусе 1 - срабатывает

struct script_status_struct{
struct script_common_status_struct{
uint16_t status; // Статус скрипта. 0 - не запущен, 1 - выполняется, 2 - завершен
uint16_t step; // Номер текущего шага
} common;
uint16_t bomb_status; // 0-Ожидание сброса бомбы, 1 - бомба летит, 2 - бомба попала, 3 - бомба не попала, 4 - перезарядка
uint16_t fire; // 0 - нажата кнопка "Выстрел", сигнал удерживается 2 секунды, 1 - не нажата
uint16_t donotpress; // 0 - нажата кнопка "не нажимать", сигнал удерживается 2 секунды, 1 - не нажата
uint16_t help; // 0 - нажата кнопка "помощь", сигнал удерживается 2 секунды, 1 - не нажата
};

struct script_control_struct{
struct script_common_control_struct{
uint16_t command; // Командный регистр для скрипта
uint16_t reserve; // 
} common;
uint16_t coordinate; // Сюда движок пишет новую координату
uint16_t height; // Сюда пишется высота в метрах
uint16_t recharge_speed; // Тут задается скорость перезарядки в секундах
uint16_t fail; // Сюда пишется флаг аварии. 0 - нет аварии, 1 - есть авария, при аварии загорается лампа и отключается кнопка сброса бомбы
};
"""
from functools import partial
import itertools

from engine import Resources, shex
from engine import Puzzle
from engine import UgolDevice, ScriptDevice


class Ugol(Puzzle):
    glossary_name = u"Угол наводчика"
    glossary_code = u"G2R4O1"
    #device_addr = 16

    def __init__(self, device_addr,  slot=None, **kwargs):
        super(Ugol, self).__init__(**kwargs)
        self.device_addr = device_addr

        self.mute_fire = False

    def on_start(self):
        self.device = UgolDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self, recharge_speed=5):
        self.device.go_to_standby()
        self.device.write_regs('conf.recharge_speed', [recharge_speed])

    def reset(self):
        self.device.send_cmd('reset')

    def go_to_standby(self):
        self.device.go_to_standby()

    def go_to_game(self):
        self.device.go_to_game()

    def go_to_solved(self):
        self.device.go_to_solved()

    def activate_utils(self):
        self.device.subscribe(
            'stat.donotpress',
            lambda data: self.proxy().donotpress_event(data[0]),
            2000
        ).get()
        self.device.subscribe(
            'stat.help',
            lambda data: self.proxy().help_event(data[0]),
            2000
        ).get()

    def activate_fight(self):
        self.device.subscribe(
            'stat.bomb_status',
            lambda data: self.proxy().bomb_status_event(data[0]),
            1000
        ).get()
        #self.device.subscribe(
            #'stat.fire',
            #lambda data: self.proxy().fire_event(data[0]),
            #400
        #).get()
        #self.device.subscribe(
            #'stat.script_status',
            #lambda data: self.proxy().status_event(data[0]),
            #1500
        #).get()

    def status_event(self, event):
        if event == 2:
            self.trigger('solved')

    def fire_event(self, event):
        if event == 0:
            if not self.mute_fire:
                self.trigger('bomb_launch')

    def trigger_bomb_hit(self):
        #next launch will hit lokda
        self.device.write_regs('conf.bomb_override', [1])

    def bomb_status_event(self, status):
        if status == 0:
            self.mute_fire = False
        elif status == 1:
            self.mute_fire = True
            self.trigger('bomb_launch')
        elif status == 2:
            self.mute_fire = True
            self.trigger('bomb_hit')
        elif status == 3:
            self.mute_fire = True
            self.trigger('bomb_miss')
        elif status == 4:
            self.mute_fire = True
            self.trigger('bomb_recharge')
        print 'bomb_status_event', status
        print 'mute_fire', self.mute_fire

    def donotpress_event(self, event):
        if event == 0:
            self.trigger('donotpress')

    def help_event(self, event):
        if event == 0:
            self.trigger('help')

    def set_coords(self, coords):
        self.device.write_regs('conf.coordinate', [coords])

    def set_height(self, height):
        self.device.write_regs('conf.height', [height])

    def set_emergency(self, emergency=True):
        self.device.write_regs('conf.fail', [int(emergency)])
        if emergency:
            self.mute_fire = True
        else:
            self.mute_fire = False

    def deactivate(self):
        self.device.unsubscribe('stat.bomb_status')
        self.device.unsubscribe('stat.donotpress')
        self.device.unsubscribe('stat.help')
        self.device.unsubscribe('stat.script_status')
        self.device.unsubscribe('stat.fire')
        self.unsubscribe('solved')
        self.device.go_to_standby()

    def stopall(self):
        self.device.stop()


class Komposter(Puzzle):
    glossary_name = u"Компостер"
    glossary_code = u"G2R1O0"
    #device_addr = 17

    def __init__(self, device_addr, slot=None, **kwargs):
        super(Komposter, self).__init__(**kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = ScriptDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        pass

    def reset(self):
        self.device.send_cmd('reset')

    def activate(self):
        pass

    def deactivate(self):
        pass

    def stopall(self):
        self.device.stop()



