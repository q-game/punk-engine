# encoding: utf-8
from functools import partial
import itertools

from engine import Resources, shex
from engine import Puzzle
from engine import TurelDevice


class Turel(Puzzle):
    glossary_name = u"Турель"
    glossary_code = u"G2R5O1"
    #device_addr = 18

    def __init__(self, device_addr,  slot=None, **kwargs):
        super(Turel, self).__init__(**kwargs)
        self.device_addr = device_addr

        self.current_holes = 0
        self.needed_fixes = 3
        self.holes = [0]*8
        self.have_arm = True
        self.disable_rockets = False

    def on_start(self):
        self.device = TurelDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self, uron=25, rockets=0, recharge=200):
        if self.disable_rockets:
            rockets=0
        self.device.write_regs('conf.uron', [uron, rockets, recharge])
        #self.device.write_regs('conf.rockets', [rockets])
        #self.device.write_regs('conf.recharge', [recharge])

    def more_rockets(self):
        self.device.write_regs('conf.rockets', [1])

    def stop_rockets(self):
        self.disable_rockets = True
        self.device.write_regs('conf.rockets', [0])

    def start_rockets(self):
        self.disable_rockets = False
        self.device.write_regs('conf.rockets', [7])

    def reset(self):
        self.device.send_cmd('reset')

    def go_to_standby(self):
        self.device.go_to_standby()

    def go_to_game(self):
        self.device.go_to_game()

    def go_to_solved(self):
        self.device.go_to_solved()


    def activate_interaction(self):
        self.device.subscribe(
            'stat.shot',
            lambda data: self.proxy().shot_event(data[0]),
            500
        ).get()
        self.device.subscribe(
            'stat.arm',
            lambda data: self.proxy().arm_event(data[0]),
            1000
        ).get()
        self.device.subscribe(
            'stat.recharge',
            lambda data: self.proxy().recharge_event(data[0]),
            1000
        ).get()

    def achivka_event(self, event):
        if event:
            self.trigger('achivka')


    def activate_fight(self):
        self.device.subscribe(
            'stat.hole_count',
            lambda data: self.proxy().holes_count(data[0]),
            1000
        ).get()
        self.device.subscribe(
            'stat.health',
            lambda data: self.proxy().health_change(data[0]),
            3000
        ).get()
        self.device.subscribe(
            'stat.achivka',
            lambda data: self.proxy().achivka_event(data[0]),
            2000
        ).get()

    def holes_count(self, holes):
        print 'holes count: {0}'.format(holes)
        if holes <= self.current_holes:
            pass
        else:
            self.current_holes = holes
            self.trigger('hit_by_rocket')

    def arm_event(self, arm):
        #print "arm_event: {0}".format(arm)
        if arm:
            self.have_arm = True
        else:
            self.have_arm = False

    def shot_event(self, shot):
        #print "shot_event: {0}".format(shot)
        if shot:
            if self.have_arm:
                self.trigger('shot')
            else:
                self.trigger('noarm')


    def recharge_event(self, recharge):
        if recharge:
            self.trigger('recharge_start')
        else:
            self.trigger('recharge_stop')


    def health_change(self, health):
        print 'health change: {0}'.format(health)
        if health == 0:
            self.trigger('breakage')
        elif health == 100:
            self.trigger('repaired')

    def check_holes(self):
        self.holes = self.device.read_regs('conf.hole0', 8).get()
        #for i, hole in enumerate(self.holes):
            #if hole > 0:
                #self.trigger('hole{0}'.format(i), hole)

    def fix_hole(self, hole, fix):
        """
        Логика такая - есть 8 регистров отвечающих за пробоины
        когда в нас попадают - моя прошивка записывает в этот регистр число 3.
        потом ты туда пишешь число 2, 1, 0.
        """

        print 'fix_hole: hole={0}, fixes={1}'.format(hole, fix)
        print self.holes
        value = self.holes[hole]
        if value < 1:
            return
        self.device.write_regs('conf.hole{0}'.format(hole), [value-fix])

    def selfhealing(self):
        self.device.write_regs('conf.hole0', [0,0,0,0,0,0,0,0])

    def full_destroy(self):
        self.device.write_regs('conf.hole0', [3,3,3,3,3,3,3,3])

    def deactivate(self):
        self.device.unsubscribe('stat.health')
        self.device.unsubscribe('stat.hole_count')
        self.device.unsubscribe('stat.shot')
        self.device.unsubscribe('stat.recharge')
        self.device.unsubscribe('stat.achivka')
        self.unsubscribe('hit_by_rocket')
        self.unsubscribe('shot')
        self.unsubscribe('recharge_start')
        self.unsubscribe('recharge_stop')
        self.unsubscribe('breakage')
        self.unsubscribe('repaired')
        self.unsubscribe('achivka')

    def stopall(self):
        self.device.stop()



