#! ../env/bin/python
# encoding: utf-8
import time
from datetime import datetime, timedelta
from pprint import pprint
import gevent
from utils import Scheduler

from pykka.gevent import GeventActor as Actor

from engine import Events, Resources, run_game
from utils import shex

from utils import load_device_addrs

def init(puzzle, *args, **kwargs):
    proxy = puzzle.start(*args, **kwargs).proxy()
    proxy.init().get()
    return proxy


dv_addrs = load_device_addrs()['steampunk']


from room0 import Achivki, Vhod, ConfigPanel
class Room0(object):
    def __init__(self):
        dv = dv_addrs['room0']
        self.inited = False
        self.puzzles = []
        self.achivki = Achivki.start(*dv['achivki']).proxy()
        self.vhod = Vhod.start(*dv['vhod']).proxy()
        self.config_panel = ConfigPanel.start(*dv['config']).proxy()
        self.puzzles = [
            self.achivki, self.vhod, self.config_panel,
        ]

    def init(self):
        #ACHTUNG: в этом методе только инициализировать загадки и девайсы
        # настроку проводить в методе setup_logic
        if self.inited:
            pass
        self.inited = True

        for puzzle in self.puzzles:
            puzzle.init().get()

    def stop(self):
        for puzzle in self.puzzles:
            puzzle.stop()



from room1 import Gotovnost, Karta, Vagonetki, Telefon, Yakor, Sidushki, PultRadio, Dver
class Room1(object):
    def __init__(self):
        dv = dv_addrs['room1']
        self.inited = False
        self.puzzles = []
        self.gotovnost = Gotovnost.start(*dv['gotovnost']).proxy()
        self.karta = Karta.start(*dv['karta']).proxy()
        self.vagonetki = Vagonetki.start(*dv['vagonetki']).proxy()
        self.telefon = Telefon.start(*dv['telefon']).proxy()
        self.yakor = Yakor.start(*dv['yakor']).proxy()
        self.sidushki = Sidushki.start(*dv['sidushki']).proxy()
        self.pultradio = PultRadio.start(*dv['pultradio']).proxy()
        self.dver = Dver.start(*dv['dver']).proxy()
        self.puzzles = [
            self.gotovnost, self.karta, self.vagonetki,
            self.telefon,
            self.yakor,
            self.sidushki,
            self.pultradio,
            self.dver
        ]

    def init(self):
        #ACHTUNG: в этом методе только инициализировать загадки и девайсы
        # настроку проводить в методе setup_logic
        if self.inited:
            pass
        self.inited = True

        for puzzle in self.puzzles:
            puzzle.init().get()

    def stop(self):
        for puzzle in self.puzzles:
            puzzle.stop()


from room2 import Pech, Balans, Mastermind, Shluz, Mahovik
class Room2(object):
    def __init__(self):
        #import pudb; pu.db
        dv = dv_addrs['room2']

        self.inited = False
        self.puzzles = []

        self.pech = Pech.start(*dv['pech']).proxy()
        self.balans = Balans.start(*dv['balans']).proxy()
        self.mastermind = Mastermind.start(*dv['mastermind']).proxy()
        self.shluz = Shluz.start(*dv['shluz']).proxy()
        self.mahovik = Mahovik.start(*dv['mahovik']).proxy()

        self.puzzles = [
            self.pech, self.balans, self.mastermind,
            self.shluz,
            self.mahovik
        ]

    def init(self):
        #ACHTUNG: в этом методе только инициализировать загадки и девайсы
        # настроку проводить в методе setup_logic
        if self.inited:
            pass
        self.inited = True

        for puzzle in self.puzzles:
            puzzle.init().get()


    def stop(self):
        for puzzle in self.puzzles:
            puzzle.stop()


from room3 import Pochinka, Okulyar, Konfigurator
class Room3(object):
    def __init__(self):
        dv = dv_addrs['room3']

        self.inited = False
        self.puzzles = []

        self.pochinka = Pochinka.start(*dv['pochinka']).proxy()
        self.okulyar = Okulyar.start(*dv['okulyar']).proxy()
        self.konfigurator = Konfigurator.start(*dv['konfigurator']).proxy()

        self.puzzles = [
            self.pochinka, self.okulyar,
            self.konfigurator
        ]


    def init(self):
        #ACHTUNG: в этом методе только инициализировать загадки и девайсы
        # настроку проводить в методе setup_logic
        if self.inited:
            pass
        self.inited = True

        for puzzle in self.puzzles:
            puzzle.init().get()


    def stop(self):
        for puzzle in self.puzzles:
            puzzle.stop()


from room4 import Ugol, Komposter
class Room4(object):
    def __init__(self):
        dv = dv_addrs['room4']

        self.inited = False
        self.puzzles = []

        self.ugol = Ugol.start(*dv['ugol']).proxy()
        self.komposter = Komposter.start(*dv['komposter']).proxy()

        self.puzzles = [
            self.ugol, self.komposter
        ]


    def init(self):
        #ACHTUNG: в этом методе только инициализировать загадки и девайсы
        # настроку проводить в методе setup_logic
        if self.inited:
            pass
        self.inited = True

        for puzzle in self.puzzles:
            puzzle.init().get()


    def stop(self):
        for puzzle in self.puzzles:
            puzzle.stop()


from room5 import Turel
class Room5(object):
    def __init__(self):
        dv = dv_addrs['room5']

        self.inited = False
        self.puzzles = []

        self.turel = Turel.start(*dv['turel']).proxy()

        self.puzzles = [
            self.turel
        ]


    def init(self):
        #ACHTUNG: в этом методе только инициализировать загадки и девайсы
        # настроку проводить в методе setup_logic
        if self.inited:
            pass
        self.inited = True

        for puzzle in self.puzzles:
            puzzle.init().get()


    def stop(self):
        for puzzle in self.puzzles:
            puzzle.stop()


class StepBase(Actor, Events):
    def __init__(self):
        #import pudb; pu.db
        super(StepBase, self).__init__()
        Events.__init__(self)
        self.failed = None
        self.solved = None
        self.PUB = Resources().require('PUB')
        self.snds = Resources().require('SNDS')
        self.video = Resources().require('VIDEO')
        self.scheduler = Resources().require('scheduler')
        self.solved_clbk = lambda: self.proxy().step_solved()

    def get_time(self):
        GAME = Resources().require('game')
        start_time = GAME._actor.start_time
        current_time = datetime.now()
        return u"{0}".format(strfdelta(current_time - start_time))

    def ach_set(self, name):
        GAME = Resources().require('game')
        if GAME._actor.ach_set(self.room0.achivki, self.snds, name):
            self.PUB.log(u"Ачивка: {0}".format(name))

    def ach_unset(self, name):
        GAME = Resources().require('game')
        GAME._actor.ach_unset(name)

    def proxy(self):
        return self.actor_ref.proxy()

    def init(self):
        raise NotImplemented()

    def start_step(self):
        #if not self.failed:
        try:
            self.setup_logic()
        except:
            self.trigger('fail')
            raise

    def setup_logic(self):
        raise NotImplemented()

    def log_that_failed(self):
        pass
        #self.PUB.log(u"""
#Кажется на этом шаге({0}), что-то может не работать, но на всякий случай пробуем его запустить.
#Если ничего не будет происходить, попробуйте нажать кнопку 'Повторить шаг', а если это не поможет - кнопку 'След. шаг'.
#""".format(self.name))

    def fail(self, pzl=None, resp=None):
        if not self.failed:
            #pass
            #self.PUB.log(u"Шаг провалился: {0}".format(self.name))
            #self.PUB.log(u"""
#Кажется на этом шаге({0}), что-то может не работать, но на всякий случай в процессе игры попробуем его запустить.
#Если ничего не будет происходить, попробуйте нажать кнопку 'Повторить шаг', а если это не поможет - кнопку 'След. шаг'.
#""".format(self.name))
            #self.PUB.step_event({'id': self.step, 'status': 'failed'})
            #self.failed = True
            self.trigger('fail')

    def step_solved(self):
        #if self.failed:
            #pass

        if not self.solved:
            #self.PUB.log(u"Шаг решен: {0}".format(self.name))
            self.PUB.step_event({'id': self.step, 'status': 'solved'})
            self.solved = True
            self.trigger('solved')

    def finishing(self):
        self.deactivate()

    def repeat(self):
        pass

    def deactivate(self):
        pass

    def _set_fail_handler(self, pzl):
        pzl.subscribe('fail', lambda data: self.proxy().fail(*data))


class Step(StepBase):
    pass


class StepTest(Step):
    name = u'Тестовый шаг'
    def __init__(self, step, room0):
        super(Step, self).__init__()
        self.step = step

    def on_start(self):
        for puzzle in [
            #self.room1.dama
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        #self.room1.dama.init().get()
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.PUB.log(u"""
Если выше не вывелось никаких ошибок(красные записи), то можно переходить к следующему этапу 'Подготовка игры'.

Если ошибки были, то нужно связаться с кем-нибудь из электроннщиков или программистом.
""", log_level='tip')

        self.PUB.manual_controls('Тест управления объектом', [
            {'method': 'room0.vhod.open_vhod', 'descr': u'Открыть вход'},
            {'method': 'room0.vhod.close_vhod', 'descr': u'Закрыть вход'},
        ])
        self.PUB.controls('Тест управления объектом', [
            {'method': 'room0.vhod.open_vhod', 'descr': u'Открыть вход'},
            {'method': 'room0.vhod.close_vhod', 'descr': u'Закрыть вход'},
        ])
        self.PUB.controls('Тест управления игрой', [
            {'method': 'set_game_config_players_2', 'descr': u'2 игрока'},
            {'method': 'set_game_config_players_3', 'descr': u'3 игрока'},
            {'method': 'set_game_config_players_4', 'descr': u'4 игрока'},
        ])
        self.PUB.controls('Дирижабль(движение)', [
            {'method': 'room1.pultradio.manual_speed_back', 'descr': u'Назад'},
            {'method': 'room1.pultradio.manual_speed_stop', 'descr': u'Стоп'},
            {'method': 'room1.pultradio.manual_speed_forward', 'descr': u'Вперед'},
            {'method': 'room1.pultradio.manual_speed_disable', 'descr': u'Вернуть управление игрокам'},
        ])
        self.PUB.controls('Дирижабль(высота)', [
            {'method': 'room1.pultradio.manual_height_down', 'descr': u'Вниз'},
            {'method': 'room1.pultradio.manual_height_stop', 'descr': u'Стоп'},
            {'method': 'room1.pultradio.manual_height_up', 'descr': u'Вверх'},
            {'method': 'room1.pultradio.manual_height_disable', 'descr': u'Вернуть управление игрокам'},
        ])
        self.PUB.controls('Дирижабль(принудительная высота) Нажимать только если Пульт совсем накрылся!', [
            {'method': 'room1.pultradio.set_override_height', 'descr': u'Сделать вид, что дирижабль на высоте 3400'},
            {'method': 'room1.pultradio.unset_override_height', 'descr': u'Отмена'},
        ])
        self.PUB.event({'enable': 'prepare_game'})

    def setup_logic(self):
        from random import random
        self.scheduler.schedule(lambda: self.PUB.height(int(random()*5000)), 1000)
        #self.proxy().step_solved()


class StepInit(Step):
    name = u'Стартуем девайсы'
    def __init__(self, step, room0, room1, room2, room3, room4, room5):
        super(Step, self).__init__()
        self.step = step
        self.room0 = room0
        self.room1 = room1
        self.room2 = room2
        self.room3 = room3
        self.room4 = room4
        self.room5 = room5

    def on_start(self):
        for room in [self.room0, self.room1, self.room2, self.room3, self.room4, self.room5]:
            for puzzle in room.puzzles:
                self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room0.init()
        self.room1.init()
        self.room2.init()
        self.room3.init()
        self.room4.init()
        self.room5.init()
        self.PUB.log(u"""
Если выше не вывелось никаких ошибок(красные записи), то можно переходить к следующему этапу 'Подготовка игры'.

Если ошибки были, то нужно связаться с кем-нибудь из электроннщиков или программистом.
""", log_level='tip')

        #TODO: manual controls for operators
        #self.PUB.manual_controls('Управление Витражем 1', [
            #{'method': 'room2.vitrag1.setup', 'descr': u'Активировать'},
            #{'method': 'room2.vitrag1.main_light', 'descr': u'(вкл) Подсветка'},
            #{'method': 'room2.vitrag1.main_off', 'descr': u'(выкл) Подсветка'},
            #{'method': 'room2.vitrag1.akkord', 'descr': u'(вкл) Часть'},
            #{'method': 'room2.vitrag1.part_off', 'descr': u'(выкл) Часть'},
        #])
        self.PUB.manual_controls('Кнопка старта квеста', [
            {'method': 'room0.config_panel.trigger_start', 'descr': u'Нажать'},
        ])
        self.PUB.manual_controls('Вход в квест', [
            {'method': 'room0.vhod.open_vhod', 'descr': u'Открыть вход'},
            {'method': 'room0.vhod.close_vhod', 'descr': u'Закрыть вход'},
        ])
        self.PUB.manual_controls('Табло готовности', [
            {'method': 'room1.gotovnost.gotov_engine', 'descr': u'Зажечь "Двигатель"'},
            {'method': 'room1.gotovnost.gotov_toplivo', 'descr': u'Зажечь "Топливо"'},
            {'method': 'room1.gotovnost.gotov_ballons', 'descr': u'Зажечь "Баллоны"'},
            {'method': 'room1.gotovnost.gotov_generator', 'descr': u'Зажечь "Генератор"'},
            {'method': 'room1.gotovnost.gotov_yakor', 'descr': u'Зажечь "Якорь"'},
        ])
        self.PUB.manual_controls('Вагонетки', [
            {'method': 'room1.vagonetki.go_to_solved', 'descr': u'Решить'},
            {'method': 'room1.vagonetki.go_to_game', 'descr': u'Вернуть к игре'},
        ])
        self.PUB.manual_controls('Якорь', [
            {'method': 'room1.yakor.go_to_solved', 'descr': u'Решить'},
        ])
        self.PUB.manual_controls('Пульт', [
            {'method': 'room1.pultradio.switch_tumbler', 'descr': u'Переключить тумблер'},
            {'method': 'room1.pultradio.go_to_solved', 'descr': u'Решить'},
            {'method': 'room1.pultradio.go_to_game', 'descr': u'Вернуть к игре'},
        ])
        self.PUB.manual_controls('Карта', [
            {'method': 'room1.karta.go_to_solved', 'descr': u'Решить'},
            {'method': 'room1.karta.go_to_game', 'descr': u'Вернуть к игре'},
        ])
        self.PUB.manual_controls('Дверь в машинный зал', [
            {'method': 'room1.dver.open_dver', 'descr': u'Открыть'},
            {'method': 'room1.dver.close_dver', 'descr': u'Закрыть'},
        ])
        self.PUB.manual_controls('Мастермайнд', [
            {'method': 'room2.mastermind.go_to_solved', 'descr': u'Решить'},
            {'method': 'room2.mastermind.go_to_game', 'descr': u'Вернуть к игре'},
        ])
        self.PUB.manual_controls('Печь', [
            {'method': 'room2.pech.go_to_solved', 'descr': u'Решить'},
            {'method': 'room2.pech.go_to_game', 'descr': u'Вернуть к игре'},
        ])
        self.PUB.manual_controls('Печь(дыммышина)', [
            {'method': 'room2.pech.smoke_enable', 'descr': u'Активна во время игры'},
            {'method': 'room2.pech.smoke_disable', 'descr': u'Неактивна во время игры'},
        ])
        self.PUB.manual_controls('Компрессор', [
            {'method': 'room2.pech.compressor_enable', 'descr': u'Поддать питание'},
            {'method': 'room2.pech.compressor_disable', 'descr': u'Снять питание'},
        ])
        self.PUB.manual_controls('Балансировка', [
            {'method': 'room2.balans.go_to_solved', 'descr': u'Решить'},
            {'method': 'room2.balans.go_to_game', 'descr': u'Вернуть к игре'},
        ])
        self.PUB.manual_controls('Маховик', [
            {'method': 'room2.mahovik.on', 'descr': u'Включить'},
            {'method': 'room2.mahovik.off', 'descr': u'Выключить'},
        ])
        self.PUB.manual_controls('Шлюз в арсенал', [
            {'method': 'room2.shluz.open_shluz', 'descr': u'Открыть'},
            {'method': 'room2.shluz.close_shluz', 'descr': u'Закрыть'},
        ])
        self.PUB.manual_controls('Конфигуратор', [
            {'method': 'room3.konfigurator.go_to_solved', 'descr': u'Решить'},
            {'method': 'room3.konfigurator.go_to_game', 'descr': u'Вернуть к игре'},
        ])
        self.PUB.manual_controls('Ракеты', [
            {'method': 'room5.turel.more_rockets', 'descr': u'Не оставить шансов игрокам'},
            {'method': 'room5.turel.stop_rockets', 'descr': u'Остановить запуск ракет'},
            {'method': 'room5.turel.start_rockets', 'descr': u'Продолжить запуск ракет'},
        ])
        self.PUB.manual_controls('Дирижабль', [
            {'method': 'room5.turel.selfhealing', 'descr': u'Починить дирижабль'},
            {'method': 'room5.turel.full_destroy', 'descr': u'Уничтожить дирижабль'},
        ])
        self.PUB.manual_controls('Изменение количества игроков', [
            {'method': 'set_game_config_players_2', 'descr': u'2 игрока'},
            {'method': 'set_game_config_players_3', 'descr': u'3 игрока'},
            {'method': 'set_game_config_players_4', 'descr': u'4 игрока'},
        ])
        self.PUB.manual_controls('Изменение типа дирижабля', [
            {'method': 'set_game_config_airship_tank', 'descr': u'Броненосный'},
            {'method': 'set_game_config_airship_shooter', 'descr': u'Скорострельный'},
            {'method': 'set_game_config_airship_fast', 'descr': u'Быстрый'},
        ])
        self.PUB.manual_controls('Изменение языка', [
            {'method': 'set_game_config_lang_rus', 'descr': u'Русский'},
            {'method': 'set_game_config_lang_eng', 'descr': u'Английский'},
        ])
        self.PUB.manual_controls('Завершение игры', [
            {'method': 'force_game_end', 'descr': u'Перейти к шагу преждевременного завершения'},
        ])
        self.PUB.event({'enable': 'prepare_game'})

    def setup_logic(self):
        self.proxy().step_solved()


class StepIdle(Step):
    name = u'Режим ожидания'
    def __init__(self, step, room0, room2):
        super(Step, self).__init__()
        self.step = step

        self.room0 = room0
        self.room2 = room2

    def on_start(self):
        for puzzle in [
            self.room0.achivki,
            self.room0.vhod,
            self.room0.config_panel,
            self.room2.pech,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room0.achivki.init()
        self.room0.vhod.init()
        self.room0.config_panel.init()

    def setup_logic(self):
        self.snds.play('glavnaya_tema_v1')
        self.snds.play('tresk_v_topke_2_min')
        self.room2.pech.smoke_enable()
        self.room2.pech.compressor_disable()
        self.room0.achivki.setup()
        self.room0.vhod.setup()
        self.room0.vhod.open_vhod()
        self.room0.config_panel.setup()
        self.PUB.log(u"""
Комната находится в режиме ожидания. Чтобы начать игру выставьте нужные настройки с помощью тумблеров в панели рядом с дверью и нажмите кнопку "Старт" рядом с дверью.
""", log_level='legend')
        self.PUB.controls('Кнопка старта квеста', [
            {'method': 'room0.config_panel.trigger_start', 'descr': u'Нажать'},
        ])

        self.room0.config_panel.subscribe('need_config',[
            lambda data: pprint('need_config: {0}'.format(data)),
            lambda data: self.proxy().config_not_defined(data)
       ])
        self.room0.config_panel.subscribe('start_game', lambda config: self.proxy().config_and_start(config)) 
        self.room0.config_panel.activate()

    def config_not_defined(self, config_name):
        if config_name == 'players':
            self.PUB.log(u"""
Не было выставлено количество игроков.
Использовано стандартное значение: 4 игрока.
""", log_level='tip')
        elif config_name == 'airship':
            self.PUB.log(u"""
Не был выставлен тип дирижабля.
Использовано стандартное значение: скорострельный.
""", log_level='tip')


    def config_and_start(self, config):
        GAME = Resources().require('game')
        GAME.set_game_config(config)
        self.room0.vhod.close_vhod()
        self.ach_set(u"Поехали!")
        self.PUB.controls('Вход в квест', [
            {'method': 'room0.vhod.open_vhod', 'descr': u'Открыть вход'},
            {'method': 'room0.vhod.close_vhod', 'descr': u'Закрыть вход'},
        ])
        self.PUB.controls('Завершение игры', [
            {'method': 'force_game_end', 'descr': u'Перейти к шагу преждевременного завершения'},
        ])
        self.PUB.controls('Изменение количества игроков', [
            {'method': 'set_game_config_players_2', 'descr': u'2 игрока'},
            {'method': 'set_game_config_players_3', 'descr': u'3 игрока'},
            {'method': 'set_game_config_players_4', 'descr': u'4 игрока'},
        ])
        self.PUB.controls('Изменение типа дирижабля', [
            {'method': 'set_game_config_airship_tank', 'descr': u'Броненосный'},
            {'method': 'set_game_config_airship_shooter', 'descr': u'Скорострельный'},
            {'method': 'set_game_config_airship_fast', 'descr': u'Быстрый'},
        ])
        self.PUB.controls('Изменение языка', [
            {'method': 'set_game_config_lang_rus', 'descr': u'Русский'},
            {'method': 'set_game_config_lang_eng', 'descr': u'Английский'},
        ])
        self.PUB.controls('Печь(дыммышина)', [
            {'method': 'room2.pech.smoke_enable', 'descr': u'Активна во время игры'},
            {'method': 'room2.pech.smoke_disable', 'descr': u'Неактивна во время игры'},
        ])
        self.solved_clbk()

    def deactivate(self):
        self.room0.config_panel.deactivate()

    def repeat(self):
        pass


def strfdelta(tdelta, fmt="{h:02}:{m:02}:{s:02}"):
    d = {}
    d["h"], rem = divmod(tdelta.seconds, 3600)
    d["m"], d["s"] = divmod(rem, 60)
    return fmt.format(**d)

def time_now():
    return datetime.now().strftime("%H:%M:%S")

class StepStartTimer(Step):
    name = u'Старт таймера'
    def __init__(self, step):
        super(Step, self).__init__()
        self.step = step

    def init(self):
        self.start_time = datetime.now()
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})

    def setup_logic(self):
        self.PUB.log(u"""
Таймер начал отсчет. Через 75 минут, если игроки не успеют выйти из комнаты, вам будет предложено закончить игру и открыть вход.

Т.е. игра автоматически не завершится! Это должны сделать вы, если видно, что игроки точно не успеют выбраться за время чуть большее чем 75 минут.

Через полчаса в интерфейс начнут выводится подсказки, сколько времени осталось.
""", log_level='legend')
        self.PUB.log(u"""

Время начала игры: <{0}>

""".format(self.start_time.strftime("%H:%M:%S")), log_level='legend')
        GAME = Resources().require('game')
        GAME.set_start_time(self.start_time)

        def timer_minutes(t):
            self.scheduler.schedule(lambda: self.PUB.log("""
<{0}> До конца игры осталось {1} минут.
""".format(time_now(), 75 - t)), t * 60 * 1000, once=True)

        timer_minutes(30)
        timer_minutes(45)
        timer_minutes(60)
        timer_minutes(70)

        def timer_seconds(t):
            self.scheduler.schedule(lambda: self.PUB.log("""
<{0}> До конца игры осталось {1} секунд.
""".format(time_now(), 60 - t)), (59 + 15) * 60 * 1000 + t * 1000, once=True)

        timer_seconds(0)
        timer_seconds(30)

        GAME = Resources().require('game')
        self.scheduler.schedule(lambda: GAME.game_over(), 75 * 60 * 1000, once=True)
        self.scheduler.schedule(self.solved_clbk, 1000, once=True)

    def deactivate(self):
        pass

    def repeat(self):
        pass

class StepYakorVagonetki(Step):
    name = u'Якорь и вагонетки'
    def __init__(self, step, room0, room1):
        super(Step, self).__init__()
        self.step = step
        self.room0 = room0
        self.room1 = room1

        self.fast_start = True

    def on_start(self):
        for puzzle in [
            self.room0.achivki,
            self.room1.gotovnost,
            self.room1.vagonetki,
            self.room1.yakor,
            self.room1.pultradio,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room1.gotovnost.init()
        self.room1.vagonetki.init()
        self.room1.yakor.init()
        self.room1.pultradio.init()

    def setup_logic(self):
        self.PUB.log(u"""
[{0}]
Что сейчас должны сделать игроки:
    - переключить на Пульте(приборной панели) тумблер - это включит две следующие загадки;
    - решить Вагонетки - одной нужно ловить огоньки, другой - уклоняться;
    - решить Якорь - просто поднять его, используя ручку из Пульта(приборной панели)
""".format(self.get_time()), log_level='legend')
        self.PUB.controls('Пульт', [
            {'method': 'room1.pultradio.switch_tumbler', 'descr': u'Переключить тумблер'},
        ])
        self.PUB.controls('Вагонетки', [
            {'method': 'room1.vagonetki.go_to_solved', 'descr': u'Решить'},
            {'method': 'room1.vagonetki.go_to_game', 'descr': u'Запустить'},
        ])
        self.PUB.controls('Якорь', [
            {'method': 'room1.yakor.go_to_solved', 'descr': u'Решить'},
        ])
        self.PUB.controls('Табло готовности', [
            {'method': 'room1.gotovnost.gotov_ballons', 'descr': u'Зажечь "Баллоны"'},
            {'method': 'room1.gotovnost.gotov_yakor', 'descr': u'Зажечь "Якорь"'},
        ])
        self.vagonetki_solved = False
        self.yakor_solved = False

        self.scheduler.schedule(
            lambda: self.proxy().disable_fast_start()
        , 1000*60, once=True)

        self.room1.gotovnost.setup()
        self.room1.vagonetki.setup()
        self.room1.yakor.setup()
        self.room1.pultradio.setup(get_config('airship_speed'))

        self.room1.pultradio.subscribe('tumbler_on',
            lambda: self.proxy().start_vagonetki()
        )
        self.room1.pultradio.subscribe('help',
            lambda: self.snds.play('help1'+get_config('lang'))
        )
        self.room1.pultradio.activate_tumbler()
        self.room1.pultradio.activate_help()

        self.room1.vagonetki.subscribe('solved',
            lambda: self.proxy().check_if_solved(vagonetki=True)
        )

        self.room1.yakor.subscribe('solved',
            lambda: self.proxy().check_if_solved(yakor=True)
        )
        self.room1.yakor.activate()
        self.room1.vagonetki.activate()

    def disable_fast_start(self):
        self.fast_start = False

    def start_vagonetki(self):
        print 'start_vagonetki'
        self.PUB.log(u"[{0}] Вагонетки активированы.".format(self.get_time()))
        if self.fast_start:
            self.ach_set(u"Быстрый старт")
        self.room1.vagonetki.go_to_game()

    def check_if_solved(self, vagonetki=False, yakor=False):
        if vagonetki:
            self.PUB.log(u"[{0}] Вагонетки решены.".format(self.get_time()))
            self.vagonetki_solved = True
            self.room1.gotovnost.gotov('ballons')
        if yakor:
            self.PUB.log(u"[{0}] Якорь решен.".format(self.get_time()))
            self.yakor_solved = True
            self.room1.gotovnost.gotov('yakor')
        if self.vagonetki_solved and self.yakor_solved:
            self.solved_clbk()

    def deactivate(self):
        self.room1.pultradio.deactivate()
        self.room1.vagonetki.deactivate()
        self.room1.yakor.deactivate()

    def repeat(self):
        pass
        #self.scheduler.unschedule(self.solved_clbk)
        #self.deactivate()
        #self.setup_logic()


class StepPechMastermind(Step):
    name = u'Печь и мастермайнд'
    def __init__(self, step, room0, room1, room2):
        super(Step, self).__init__()
        self.step = step
        self.room0 = room0
        self.room1 = room1
        self.room2 = room2

        self.plate_open = True
        self.plate_open_clbk = lambda: self.proxy().dim_plate_open()

    def on_start(self):
        for puzzle in [
            self.room0.achivki,
            self.room1.gotovnost,
            self.room1.pultradio,
            self.room1.dver,
            self.room2.pech,
            self.room2.mastermind,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room1.gotovnost.init()
        self.room1.pultradio.init()
        self.room1.dver.init()
        self.room2.pech.init()
        self.room2.mastermind.init()

    def setup_logic(self):
        self.PUB.log(u"""
[{0}]
Что сейчас должны сделать игроки:
    - решить Мастермайнд(штука с батарейками);
    - решить Печь - нужно собрать Палено и вставить его в топку до конца.

Есть два варианта решения Печи:
    - ачивка "Фанат"( + решает саму загадку)
    - правильное решение
""".format(self.get_time()), log_level='legend')
        self.PUB.controls('Дверь в машинный зал', [
            {'method': 'room1.dver.open_dver', 'descr': u'Открыть'},
            {'method': 'room1.dver.close_dver', 'descr': u'Закрыть'},
        ])
        self.PUB.controls('Табло готовности', [
            {'method': 'room1.gotovnost.gotov_toplivo', 'descr': u'Зажечь "Топливо"'},
            {'method': 'room1.gotovnost.gotov_generator', 'descr': u'Зажечь "Генератор"'},
        ])
        self.PUB.controls('Мастермайнд', [
            {'method': 'room2.mastermind.go_to_solved', 'descr': u'Решить'},
            {'method': 'room2.mastermind.go_to_game', 'descr': u'Вернуть к игре'},
        ])
        self.PUB.controls('Печь', [
            {'method': 'room2.pech.go_to_solved', 'descr': u'Решить'},
            {'method': 'room2.pech.go_to_game', 'descr': u'Вернуть к игре'},
        ])
        self.pech_solved = False
        self.mastermind_solved = False
        self.room1.pultradio.unsubscribe('help')
        self.room1.pultradio.set_track('novost1'+get_config('lang'))
        self.scheduler.schedule(self.help_subs, 1000*45, once=True)

        self.room1.dver.setup()
        self.room1.dver.open_dver()
        self.scheduler.schedule(self.plate_open_clbk, 1000)
        self.room2.pech.setup()
        self.room2.mastermind.setup()
        self.room2.pech.go_to_game()
        self.room2.mastermind.go_to_game()

        self.room2.pech.subscribe('solved',[
            lambda: self.PUB.log(u"[{0}] Печь решена.".format(self.get_time())),
            lambda: self.proxy().check_if_solved(pech=True)
        ])
        self.room2.mastermind.subscribe('solved',[
            lambda retries: self.PUB.log(u"[{0}] Мастермайнд решен.".format(self.get_time())),
            lambda retries: self.proxy().mastermind_achivki(retries),
            lambda retries: self.proxy().check_if_solved(mastermind=True),
        ])

        self.room2.pech.subscribe('wrong_poleno',
            lambda: self.snds.play('nepravilnoe_poleno')
        )
        self.room2.pech.subscribe('achivka',[
            lambda: self.ach_set(u"Фанат")
        ])

        self.room1.pultradio.activate_help()
        self.room2.pech.activate()
        self.room2.mastermind.activate()

    def help_subs(self):
        self.room1.pultradio.subscribe('help',
            lambda: self.snds.play('help1'+get_config('lang'))
        )

    def dim_plate_open(self):
        if self.plate_open:
            self.room1.dver.plate_off()
            self.plate_open = False
        else:
            self.room1.dver.plate_on()
            self.plate_open = True

    def mastermind_achivki(self, retries):
        if retries <= 9:
            self.ach_set(u"Энерджайзер")

        if retries <= 1:
            self.ach_set(u"1 на 360")

    def check_if_solved(self, pech=False, mastermind=False):
        if pech:
            print 'pech solved'
            self.pech_solved = True
            self.snds.play('pravilnoe_poleno')
            self.snds.play('topka_reshena')
            self.room1.gotovnost.gotov('toplivo')
        if mastermind:
            print 'mastermind solved'
            self.mastermind_solved = True
            self.room1.gotovnost.gotov('generator')
        if self.pech_solved and self.mastermind_solved:
            self.room1.dver.plate_on()
            self.solved_clbk()

    def deactivate(self):
        self.room1.pultradio.deactivate()
        self.room2.pech.deactivate()
        self.room2.mastermind.deactivate()
        self.scheduler.unschedule(self.plate_open_clbk)

    def repeat(self):
        pass
        #self.scheduler.unschedule(self.solved_clbk)
        #self.deactivate()
        #self.setup_logic()


class StepBalansirovka(Step):
    name = u'Балансировка(запуск двигателя)'
    def __init__(self, step, room0, room1, room2):
        super(Step, self).__init__()
        self.step = step
        self.room0 = room0
        self.room1 = room1
        self.room2 = room2

    def on_start(self):
        for puzzle in [
            self.room0.achivki,
            self.room1.gotovnost,
            self.room1.pultradio,
            self.room2.balans,
            self.room2.mahovik,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room1.gotovnost.init()
        self.room1.pultradio.init()
        self.room2.balans.init()
        self.room2.mahovik.init()

    def setup_logic(self):
        self.PUB.log(u"""
[{0}]
Что сейчас должны сделать игроки:
    - выставить на Балансировке значения, которые показывают Мастермайнд и Печь.

""".format(self.get_time()), log_level='legend')
        self.PUB.controls('Табло готовности', [
            {'method': 'room1.gotovnost.gotov_engine', 'descr': u'Зажечь "Двигатель"'},
        ])
        self.PUB.controls('Балансировка', [
            {'method': 'room2.balans.go_to_solved', 'descr': u'Решить'},
            {'method': 'room2.balans.go_to_game', 'descr': u'Вернуть к игре'},
        ])
        self.PUB.controls('Маховик', [
            {'method': 'room2.mahovik.on', 'descr': u'Включить'},
            {'method': 'room2.mahovik.off', 'descr': u'Выключить'},
        ])
        self.room2.mahovik.setup()
        self.room1.pultradio.unsubscribe('help')
        self.snds.stop_play('help1'+get_config('lang'))
        self.room1.pultradio.set_track('novost2'+get_config('lang'))
        self.scheduler.schedule(self.help_subs, 1000*29, once=True)
        self.room2.balans.setup()
        self.room2.balans.go_to_game()

        self.room2.balans.subscribe('solved',[
            lambda: self.PUB.log(u'[{0}] Балансировка решена.'.format(self.get_time())),
            lambda: self.proxy().actions_on_solved()
        ])
        self.room2.balans.subscribe('ach_energy',[
            lambda: self.ach_set(u"Подгоревший")
        ])
        self.room2.balans.subscribe('ach_fire',[
            lambda: self.ach_set(u"Чистая энергия")
        ])
        self.room1.pultradio.activate_help()
        self.room2.balans.activate()

    def help_subs(self):
        self.room1.pultradio.subscribe('help',
            lambda: self.snds.play('help1'+get_config('lang'))
        )

    def actions_on_solved(self):
        self.room1.gotovnost.gotov('engine')
        self.room2.mahovik.on()
        self.snds.play('shum_machinnogo_zala_start')
        self.scheduler.schedule(
            lambda: self.snds.play('shum_machinnogo_zala_2min')
        , 1000*90, once=True)

        self.solved_clbk()

    def deactivate(self):
        self.room1.pultradio.deactivate()
        self.room2.balans.deactivate()

    def repeat(self):
        pass
        #self.scheduler.unschedule(self.solved_clbk)
        #self.deactivate()
        #self.setup_logic()


class StepVzlet(Step):
    name = u'Взлет'
    def __init__(self, step, room0, room1):
        super(Step, self).__init__()
        self.step = step
        self.room0 = room0
        self.room1 = room1

        self.shdld_snds = lambda: self.proxy().scheduled_sounds()
        self.vzletaem = True

    def on_start(self):
        for puzzle in [
            self.room0.achivki,
            self.room1.pultradio,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room1.pultradio.init()

    def novost3(self):
        self.room1.pultradio.set_track('novost3'+get_config('lang'))

    def replika1(self):
        self.snds.play('replika1sh'+get_config('lang'))

    def setup_logic(self):
        self.PUB.log(u"""
[{0}]
Что сейчас должны сделать игроки:
    - используя ручки Телеграфа(штука рядом с Пультом) подняться на высоту более 3000м, чтобы можно было связаться с маякам

""".format(self.get_time()), log_level='legend')
        self.PUB.controls('Дирижабль(движение)', [
            {'method': 'room1.pultradio.manual_speed_back', 'descr': u'Назад'},
            {'method': 'room1.pultradio.manual_speed_stop', 'descr': u'Стоп'},
            {'method': 'room1.pultradio.manual_speed_forward', 'descr': u'Вперед'},
            {'method': 'room1.pultradio.manual_speed_disable', 'descr': u'Вернуть управление игрокам'},
        ])
        self.PUB.controls('Дирижабль(высота)', [
            {'method': 'room1.pultradio.manual_height_down', 'descr': u'Вниз'},
            {'method': 'room1.pultradio.manual_height_stop', 'descr': u'Стоп'},
            {'method': 'room1.pultradio.manual_height_up', 'descr': u'Вверх'},
            {'method': 'room1.pultradio.manual_height_disable', 'descr': u'Вернуть управление игрокам'},
        ])
        self.PUB.controls('Дирижабль(принудительная высота) Нажимать только если Пульт совсем накрылся!', [
            {'method': 'room1.pultradio.set_override_height', 'descr': u'Сделать вид, что дирижабль на высоте 3400'},
            {'method': 'room1.pultradio.unset_override_height', 'descr': u'Отмена'},
        ])
        self.snds.stop_play('help1'+get_config('lang'))
        self.snds.play('polet')
        self.snds.play('vzlet')
        self.room1.pultradio.unsubscribe('help')
        self.scheduler.schedule(
            lambda: self.proxy().novost3()
        , 1000*5, once=True)
        self.room1.pultradio.setup(get_config('airship_speed'))
        self.room1.pultradio.go_to_game()
        self.room1.pultradio.cant_reach_lodka()
        self.room1.pultradio.enable_handles()
        self.scheduler.schedule(
            lambda: self.proxy().replika1()
        , 1000*(5+29), once=True)
        self.scheduler.schedule(self.help_subs, 1000*(5+29+49), once=True)

        self.room1.pultradio.subscribe('uletel',
            lambda: self.ach_set(u"На краю света")
        )
        self.room1.pultradio.subscribe('height',[
            lambda height: self.proxy().set_current_height(height),
        ])
        self.scheduler.schedule(
            lambda: self.proxy().set_current_height(self.room1.pultradio.get_height().get())
        , 1000, once=True)

        self.room1.pultradio.subscribe('wind',
            lambda: self.snds.play('strong_wind'+get_config('lang'))
        )
        self.room1.pultradio.subscribe('mayaki_height',[
            lambda: self.PUB.log(u'[{0}] Игроки поднялись на нужную высоту.'.format(self.get_time())),
            lambda: self.proxy().actions_on_solved()
        ])
        self.room1.pultradio.activate_height()
        self.room1.pultradio.activate_wind()
        self.room1.pultradio.activate_mayaki_height()
        self.room1.pultradio.activate_help()

        self.scheduler.schedule(
            self.shdld_snds,
            1000*20
        )

    def help_subs(self):
        self.room1.pultradio.subscribe('help',
            lambda: self.snds.play('help2'+get_config('lang'))
        )

    def set_current_height(self, height):
        self.current_height = height
        self.PUB.height(height)
        if height >= 4950:
            self.ach_set(u"Икар")

        if self.vzletaem:
            if self.current_height > 500:
                self.vzletaem = False

    def scheduled_sounds(self):
        height = getattr(self, 'current_height', None)
        if height is None:
            return

        if height < 500:
            if not self.vzletaem:
                self.snds.play('malaya_visota'+get_config('lang'))
                self.scheduler.schedule(
                    lambda: self.snds.play('naberite_visotu'+get_config('lang')),
                    1000*3.5,
                    once=True
                )
        elif height > 4500:
            self.snds.play('bolshaya_visota'+get_config('lang'))


    def actions_on_solved(self):
        self.solved_clbk()

    def deactivate(self):
        self.room1.pultradio.deactivate()
        self.scheduler.unschedule(self.shdld_snds)

    def repeat(self):
        pass
        #self.scheduler.unschedule(self.solved_clbk)
        #self.deactivate()
        #self.setup_logic()


class StepKartaMayaki(Step):
    name = u'Карта и маяки'
    def __init__(self, step, room0, room1):
        super(Step, self).__init__()
        self.step = step
        self.room0 = room0
        self.room1 = room1

        self.shdld_snds = lambda: self.proxy().scheduled_sounds()
        self.current_height = 3000

    def on_start(self):
        for puzzle in [
            self.room0.achivki,
            self.room1.pultradio,
            self.room1.karta,
            self.room1.telefon,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room1.pultradio.init()
        self.room1.karta.init()
        self.room1.telefon.init()

    def setup_logic(self):
        self.PUB.log(u"""
[{0}]
Что сейчас должны сделать игроки:
    - обзвонить маяки, чтобы узнать где было сражение(в трех будет полезная информация о направление, в остальных - всякая чушь)
    - выставить эти направления на Карте используя ручки под ней
    - после выставления направлений с трех правильных маяков Карта решится
""".format(self.get_time()), log_level='legend')
        self.PUB.controls('Дирижабль(движение)', [
            {'method': 'room1.pultradio.manual_speed_back', 'descr': u'Назад'},
            {'method': 'room1.pultradio.manual_speed_stop', 'descr': u'Стоп'},
            {'method': 'room1.pultradio.manual_speed_forward', 'descr': u'Вперед'},
            {'method': 'room1.pultradio.manual_speed_disable', 'descr': u'Вернуть управление игрокам'},
        ])
        self.PUB.controls('Дирижабль(высота)', [
            {'method': 'room1.pultradio.manual_height_down', 'descr': u'Вниз'},
            {'method': 'room1.pultradio.manual_height_stop', 'descr': u'Стоп'},
            {'method': 'room1.pultradio.manual_height_up', 'descr': u'Вверх'},
            {'method': 'room1.pultradio.manual_height_disable', 'descr': u'Вернуть управление игрокам'},
        ])
        self.PUB.controls('Дирижабль(принудительная высота) Нажимать только если Пульт совсем накрылся!', [
            {'method': 'room1.pultradio.set_override_height', 'descr': u'Сделать вид, что дирижабль на высоте 3400'},
            {'method': 'room1.pultradio.unset_override_height', 'descr': u'Отмена'},
        ])
        self.PUB.controls('Карта', [
            {'method': 'room1.karta.go_to_solved', 'descr': u'Решить'},
            #{'method': 'room1.karta.go_to_game', 'descr': u'Вернуть к игре'},
        ])
        self.snds.play('polet')
        self.room1.pultradio.setup(get_config('airship_speed'))
        self.room1.pultradio.go_to_game()
        self.room1.pultradio.cant_reach_lodka()
        self.room1.pultradio.enable_handles()
        self.room1.karta.setup()
        self.room1.karta.go_to_game()
        self.room1.telefon.setup(get_config('lang'))
        self.room1.telefon.go_to_standby()
        self.room1.telefon.go_to_game()

        self.room1.telefon.subscribe('mayak',
            lambda mayak: self.proxy().ring_to_mayak(mayak)
        )
        self.room1.pultradio.subscribe('wind',
            lambda: self.snds.play('strong_wind'+get_config('lang'))
        )
        self.room1.pultradio.subscribe('uletel',
            lambda: self.ach_set(u"На краю света")
        )
        self.snds.stop_play('help2'+get_config('lang'))
        self.room1.pultradio.subscribe('help',
            lambda: self.snds.play('help3'+get_config('lang'))
        )
        self.room1.pultradio.subscribe('height',[
            lambda height: self.proxy().set_current_height(height),
        ])
        self.scheduler.schedule(
            lambda: self.proxy().set_current_height(self.room1.pultradio.get_height().get())
        , 1000, once=True)


        #for tests
        #for i in range(0,8):
            #self.room1.karta.set_mayak(i)


        self.room1.karta.subscribe('solved',[
            lambda: self.PUB.log(u'[{0}] Карта решена.'.format(self.get_time())),
            lambda: self.proxy().actions_on_solved()
        ])
        self.room1.karta.subscribe('achivka',
            lambda: self.ach_set(u"Настоящий друг")
        )
        self.room1.karta.activate()
        self.room1.telefon.activate()
        self.room1.pultradio.activate_height()
        self.room1.pultradio.activate_wind()
        self.room1.pultradio.activate_help()

        self.scheduler.schedule(
            self.shdld_snds,
            1000*20
        )

    def set_current_height(self, height):
        self.current_height = height
        self.PUB.height(height)
        if height >= 4950:
            self.ach_set(u"Икар")

        if height < 3000 or height > 4500:
            self.room1.telefon.go_to_standby()
        else:
            self.room1.telefon.go_to_game()

    def scheduled_sounds(self):
        height = getattr(self, 'current_height', None)
        if height is None:
            return

        if height < 500:
            self.snds.play('malaya_visota'+get_config('lang'))
            self.scheduler.schedule(
                lambda: self.snds.play('naberite_visotu'+get_config('lang')),
                1000*3.5,
                once=True
            )
        elif height > 4500:
            self.snds.play('bolshaya_visota')

    def ring_to_mayak(self, mayak=None):
        if self.current_height < 3000:
            print 'ring_to_mayak: sound: nedostatochnaya_visota_dlya_mayaka'
            self.snds.play('nedostatochnaya_visota_dlya_mayaka'+get_config('lang'))
            return

        if mayak == -1:
            self.snds.play('net_mayaka'+get_config('lang'))
            return

        self.room1.karta.set_mayak(mayak)
        mayaks = self.room1.karta.get_mayaks().get()
        if all(mayaks):
            self.ach_set(u"Никто не забыт")

    def actions_on_solved(self):
        self.solved_clbk()

    def deactivate(self):
        self.room1.pultradio.deactivate()
        self.room1.telefon.deactivate()
        self.room1.karta.deactivate()
        self.scheduler.unschedule(self.shdld_snds)

    def repeat(self):
        pass
        #self.scheduler.unschedule(self.solved_clbk)
        #self.deactivate()
        #self.setup_logic()

class StepFlight(Step):
    name = u'Полет к лодке'
    def __init__(self, step, room0, room1, room2):
        super(Step, self).__init__()
        self.step = step
        self.room0 = room0
        self.room1 = room1
        self.room2 = room2

        self.shdld_snds = lambda: self.proxy().scheduled_sounds()

    def on_start(self):
        for puzzle in [
            self.room0.achivki,
            self.room1.telefon,
            self.room1.karta,
            self.room1.pultradio,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room1.pultradio.init()
        self.room1.telefon.init()
        self.room1.karta.init()

    def setup_logic(self):
        self.PUB.log(u"""
[{0}]
Что сейчас должны сделать игроки:
    - используя Телеграф и Пульт долететь до точки, которая подсвечивается на Карте;
""".format(self.get_time()), log_level='legend')
        self.PUB.controls('Пульт', [
            {'method': 'room1.pultradio.go_to_solved', 'descr': u'Решить'},
            {'method': 'room1.pultradio.go_to_game', 'descr': u'Вернуть к игре'},
        ])
        self.PUB.controls('Дирижабль(движение)', [
            {'method': 'room1.pultradio.manual_speed_back', 'descr': u'Назад'},
            {'method': 'room1.pultradio.manual_speed_stop', 'descr': u'Стоп'},
            {'method': 'room1.pultradio.manual_speed_forward', 'descr': u'Вперед'},
            {'method': 'room1.pultradio.manual_speed_disable', 'descr': u'Вернуть управление игрокам'},
        ])
        self.PUB.controls('Дирижабль(высота)', [
            {'method': 'room1.pultradio.manual_height_down', 'descr': u'Вниз'},
            {'method': 'room1.pultradio.manual_height_stop', 'descr': u'Стоп'},
            {'method': 'room1.pultradio.manual_height_up', 'descr': u'Вверх'},
            {'method': 'room1.pultradio.manual_height_disable', 'descr': u'Вернуть управление игрокам'},
        ])
        self.PUB.controls('Дирижабль(принудительная высота) Нажимать только если Пульт совсем накрылся!', [
            {'method': 'room1.pultradio.set_override_height', 'descr': u'Сделать вид, что дирижабль на высоте 3400'},
            {'method': 'room1.pultradio.unset_override_height', 'descr': u'Отмена'},
        ])
        self.room1.pultradio.setup(get_config('airship_speed'))
        self.room1.pultradio.go_to_game()
        self.room1.pultradio.can_reach_lodka()

        self.room1.pultradio.subscribe('height',[
            lambda height: self.proxy().set_current_height(height),
        ])
        self.scheduler.schedule(
            lambda: self.proxy().set_current_height(self.room1.pultradio.get_height().get())
        , 1000, once=True)

        self.snds.stop_play('help3'+get_config('lang'))
        self.room1.pultradio.subscribe('help',
            lambda: self.snds.play('help4'+get_config('lang'))
        )
        self.room1.pultradio.subscribe('uletel',
            lambda: self.ach_set(u"На краю света")
        )
        self.room1.pultradio.subscribe('wind',[
            lambda: self.PUB.log(u'[{0}] Включился ветер.'.format(self.get_time())),
            lambda: self.snds.play('strong_wind'+get_config('lang'))
        ])
        self.room1.pultradio.subscribe('solved',[
            lambda: self.PUB.log(u'[{0}] Игроки подлетели к точке. Дальше они не смогут летать, только менять высоту'.format(self.get_time())),
            lambda: self.proxy().actions_on_solved(),
        ])
        self.room1.pultradio.activate_height()
        self.room1.pultradio.activate_wind()
        self.room1.pultradio.activate_help()
        self.room1.pultradio.activate_script()

        self.scheduler.schedule(
            self.shdld_snds,
            1000*20
        )

        self.room1.telefon.setup(get_config('lang'))
        self.room1.telefon.go_to_standby()
        self.room1.telefon.go_to_game()
        self.room1.telefon.subscribe('mayak',
            lambda mayak: self.proxy().ring_to_mayak(mayak)
        )
        self.room1.telefon.activate()

        self.room1.karta.subscribe('achivka',
            lambda: self.ach_set(u"Настоящий друг")
        )
        self.room1.karta.activate()

    def set_current_height(self, height):
        self.current_height = height
        self.PUB.height(height)

        if height >= 4950:
            self.ach_set(u"Икар")

        if height < 3000 or height > 4500:
            self.room1.telefon.go_to_standby()
        else:
            self.room1.telefon.go_to_game()

    def ring_to_mayak(self, mayak=None):
        if self.current_height < 3000:
            print 'ring_to_mayak: sound: nedostatochnaya_visota_dlya_mayaka'
            self.snds.play('nedostatochnaya_visota_dlya_mayaka'+get_config('lang'))
            return

        if mayak == -1:
            self.snds.play('net_mayaka'+get_config('lang'))
            return

        self.room1.karta.set_mayak(mayak)
        mayaks = self.room1.karta.get_mayaks().get()
        if all(mayaks):
            self.ach_set(u"Никто не забыт")


    def scheduled_sounds(self):
        height = getattr(self, 'current_height', None)
        if height is None:
            return

        if height < 500:
            self.snds.play('malaya_visota'+get_config('lang'))
            self.scheduler.schedule(
                lambda: self.snds.play('naberite_visotu'+get_config('lang')),
                1000*3.5,
                once=True
            )
        elif height > 4500:
            self.snds.play('bolshaya_visota')

    def actions_on_solved(self):
        self.room1.pultradio.unsubscribe('help')
        self.snds.stop_play('help4'+get_config('lang'))
        self.snds.play('replika2sh'+get_config('lang'))
        self.scheduler.schedule(
            lambda: self.proxy().scheduled_actions_on_solved(),
            1000*25,
            once=True
        )

    def scheduled_actions_on_solved(self):
        self.solved_clbk()

    def deactivate(self):
        self.room1.pultradio.deactivate()
        self.room1.karta.deactivate()
        self.room1.telefon.deactivate()
        self.scheduler.unschedule(self.shdld_snds)

    def repeat(self):
        pass
        #self.scheduler.unschedule(self.solved_clbk)
        #self.deactivate()
        #self.setup_logic()


class StepBombConfig(Step):
    name = u'Арсенал. Подготовка бомбы'
    def __init__(self, step, room0, room1, room2, room3, room4, room5):
        super(Step, self).__init__()
        self.step = step
        self.room0 = room0
        self.room1 = room1
        self.room2 = room2
        self.room3 = room3
        self.room4 = room4
        self.room5 = room5

        self.shdld_snds = lambda: self.proxy().scheduled_sounds()

        self.plate_open = True
        self.plate_open_clbk = lambda: self.proxy().dim_plate_open()

    def on_start(self):
        for puzzle in [
            self.room0.achivki,
            self.room1.pultradio,
            self.room1.karta,
            self.room2.shluz,
            self.room3.konfigurator,
            self.room4.ugol,
            self.room5.turel,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room1.pultradio.init()
        self.room2.shluz.init()
        self.room3.konfigurator.init()
        self.room4.ugol.init()
        self.room5.turel.init()

    def setup_logic(self):
        self.PUB.log(u"""
[{0}]
Что сейчас должны сделать игроки:
    - посмотреть в Люк(огромный телек в полу) как выглядит лодка
    - выставить очертания лодки на Компостере, дернуть ручку сбоку от Компостера и получить код лодки
    - по коду лодки в Картотеке найти очертания нужной бомбы
    - выставить очертания бомбы на Конфигураторе
    - достать бомбу и положить в Бомбоприемник
""".format(self.get_time()), log_level='legend')
        self.PUB.controls('Шлюз в арсенал', [
            {'method': 'room2.shluz.open_shluz', 'descr': u'Открыть'},
            {'method': 'room2.shluz.close_shluz', 'descr': u'Закрыть'},
        ])
        self.PUB.controls('Конфигуратор', [
            {'method': 'room3.konfigurator.go_to_solved', 'descr': u'Решить'},
            {'method': 'room3.konfigurator.go_to_game', 'descr': u'Вернуть к игре'},
        ])
        self.PUB.controls('Дирижабль(высота)', [
            {'method': 'room1.pultradio.manual_height_down', 'descr': u'Вниз'},
            {'method': 'room1.pultradio.manual_height_stop', 'descr': u'Стоп'},
            {'method': 'room1.pultradio.manual_height_up', 'descr': u'Вверх'},
            {'method': 'room1.pultradio.manual_height_disable', 'descr': u'Вернуть управление игрокам'},
        ])
        self.PUB.controls('Дирижабль(принудительная высота) Нажимать только если Пульт совсем накрылся!', [
            {'method': 'room1.pultradio.set_override_height', 'descr': u'Сделать вид, что дирижабль на высоте 3400'},
            {'method': 'room1.pultradio.unset_override_height', 'descr': u'Отмена'},
        ])

        self.room2.shluz.setup()
        self.room2.shluz.open_shluz()
        self.scheduler.schedule(self.plate_open_clbk, 1000)

        self.snds.play('manual1'+get_config('lang'))
        #TODO
        self.scheduler.schedule(self.help_subs, 1000*(20), once=True)

        self.scheduler.schedule(
            lambda: self.video.queue(['luk', 'vspliv'])
        , 1000*25, once=True)
        self.room1.pultradio.setup(get_config('airship_speed'))
        self.room1.pultradio.go_to_solved()
        self.room1.pultradio.enable_handles()
        self.room3.konfigurator.setup()
        self.room4.ugol.setup()
        self.room4.ugol.go_to_standby()
        self.room5.turel.setup(recharge=get_config('turel_recharge_speed'))
        self.room5.turel.go_to_game()

        self.room1.pultradio.subscribe('height',[
            lambda height: self.proxy().set_current_height(height),
        ])
        self.scheduler.schedule(
            lambda: self.proxy().set_current_height(self.room1.pultradio.get_height().get())
        , 1000, once=True)

        #инициализируем высоту в углу
        self.scheduler.schedule(
            lambda: self.room4.ugol.set_height(self.room1.pultradio.get_height().get())
        , 1000, once=True)
        #синхронизация высоты
        self.room1.pultradio.subscribe('height',
            lambda height: self.room4.ugol.set_height(height)
        )
        self.room4.ugol.subscribe('donotpress',[
            lambda: self.proxy().dont_press(),
            lambda: pprint("event: donotpress"),
        ])

        self.snds.setup('ochered_1')
        self.snds.setup('osechka_1')
        self.snds.setup('perezaryadka')
        self.room5.turel.subscribe('shot',
            lambda: self.snds.snd_ochered()
        )
        self.room5.turel.subscribe('noarm',[
            lambda: self.ach_unset(u"Прагматик"),
            lambda: self.snds.snd_osechka()
        ])
        self.room5.turel.subscribe('recharge_start',
            lambda: self.snds.resume('perezaryadka')
        )
        self.room5.turel.subscribe('recharge_stop',
            lambda: self.snds.pause('perezaryadka')
        )

        self.room3.konfigurator.subscribe('solved',[
            lambda: self.PUB.log(u'[{0}] Бомба сконфигурирована.'.format(self.get_time())),
            lambda: self.proxy().actions_on_solved()
        ])
        self.room3.konfigurator.activate()
        self.room3.konfigurator.go_to_game()

        self.room4.ugol.activate_utils()
        self.room5.turel.activate_interaction()
        self.room1.pultradio.activate_height()
        self.room1.pultradio.activate_help()

        self.scheduler.schedule(
            self.shdld_snds,
            1000*20
        )


    def help_subs(self):
        self.room1.pultradio.subscribe('help',
            lambda: self.snds.play('manual1'+get_config('lang'))
        )
        self.room4.ugol.subscribe('help',
            lambda: self.snds.play('manual1'+get_config('lang'))
        )


    def dim_plate_open(self):
        if self.plate_open:
            self.room2.shluz.plate_off()
            self.plate_open = False
        else:
            self.room2.shluz.plate_on()
            self.plate_open = True


    def set_current_height(self, height):
        self.current_height = height
        self.PUB.height(height)

        if height >= 4950:
            self.ach_set(u"Икар")


    def scheduled_sounds(self):
        height = getattr(self, 'current_height', None)
        if height is None:
            return

        if height < 500:
            self.snds.play('malaya_visota'+get_config('lang'))
            self.scheduler.schedule(
                lambda: self.snds.play('naberite_visotu'+get_config('lang')),
                1000*3.5,
                once=True
            )
        elif height > 4500:
            self.snds.play('bolshaya_visota')

    def dont_press(self):
        self.ach_unset(u"Сила воли")
        self.room1.karta.light_dontpress()
        self.snds.pause('perezaryadka')
        self.snds.play('dont_push_30s'+get_config('lang')),
        self.room1.pultradio.disable_handles()
        self.room3.konfigurator.go_to_standby()
        self.room5.turel.go_to_standby()
        self.scheduler.schedule(
            lambda: self.proxy().dont_press_end(),
            1000*32,
            once=True
        )
        self.room1.pultradio.unsubscribe('help')
        self.room4.ugol.unsubscribe('help')
        self.snds.stop_play('manual1'+get_config('lang'))

    def dont_press_end(self):
        self.room1.karta.light_on()
        self.room1.pultradio.enable_handles()
        self.room3.konfigurator.go_to_game()
        self.room5.turel.go_to_game()
        self.help_subs()

    def actions_on_solved(self):
        print 'konfigurator solved'
        self.room2.shluz.plate_on()
        self.solved_clbk()

    def deactivate(self):
        self.room3.konfigurator.deactivate()
        self.room1.pultradio.deactivate()
        self.room4.ugol.deactivate()
        self.room5.turel.deactivate()
        self.scheduler.unschedule(self.shdld_snds)
        self.scheduler.unschedule(self.plate_open_clbk)

    def repeat(self):
        pass
        #self.scheduler.unschedule(self.solved_clbk)
        #self.deactivate()
        #self.setup_logic()


class StepFight(Step):
    name = u'Бой с подлодкой'
    def __init__(self, step, room0, room1, room3, room4, room5):
        super(Step, self).__init__()
        self.step = step
        self.room0 = room0
        self.room1 = room1
        self.room3 = room3
        self.room4 = room4
        self.room5 = room5

        self.shdld_snds = lambda: self.proxy().scheduled_sounds()
        self.shdld_selfhealing = lambda: self.proxy().selfhealing()
        self.goal_rem_clbk = lambda: self.snds.play('goal_reminder'+get_config('lang'))
        self.more_rockets_clbk = lambda: self.room5.turel.more_rockets()
        self.breaked = False
        self.smoke_count = 1

        okl = self.room3.okulyar
        ugl = self.room4.ugol
        self.move_tick = lambda: [func() for func in [
            lambda: okl.move_by_route(ugl),
        ]]

        trl = self.room5.turel
        self.check_holes = lambda: [func() for func in [
            lambda: trl.check_holes(),
        ]]

        self.step_stage = 'start'

        self.bomb_count = 0

    def on_start(self):
        for puzzle in [
            self.room0.achivki,
            self.room1.pultradio,
            self.room1.karta,
            self.room3.pochinka,
            self.room4.ugol,
            self.room5.turel,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room1.pultradio.init()
        self.room3.pochinka.init()
        self.room3.okulyar.init()
        self.room4.ugol.init()
        self.room5.turel.init()

    def setup_logic(self):
        self.PUB.log(u"""
[{0}]
Что сейчас должны сделать игроки:
    - используя Окуляры запомнить траекторию лодки
    - используя Угол наводчика, сбросить бомбу с упреждением(бомбе нужно время, чтобы долететь до цели)
    - после первого попадания лодка начнет отстреливаться
    - нужно сбивать ракеты, которые летят в дирижабль, используя турель
    - при попадании ракет в дирижабль ракет, его нужно чинить с помощью Починки(штука с кучей вентилей)
    - после второго попадания в лодку, она будет уничтожена и квест перейдет к завершающему шагу
""".format(self.get_time()), log_level='legend')
        self.PUB.controls('Бомбоприемник', [
            {'method': 'room3.pochinka.trigger_bomb_loaded', 'descr': u'Сделать вид, что бомба загружена'},
        ])
        self.PUB.controls('Ракеты', [
            {'method': 'room5.turel.more_rockets', 'descr': u'Не оставить шансов игрокам'},
            {'method': 'room5.turel.stop_rockets', 'descr': u'Остановить запуск ракет'},
            {'method': 'room5.turel.start_rockets', 'descr': u'Продолжить запуск ракет'},
        ])
        self.PUB.controls('Дирижабль(высота)', [
            {'method': 'room1.pultradio.manual_height_down', 'descr': u'Вниз'},
            {'method': 'room1.pultradio.manual_height_stop', 'descr': u'Стоп'},
            {'method': 'room1.pultradio.manual_height_up', 'descr': u'Вверх'},
            {'method': 'room1.pultradio.manual_height_disable', 'descr': u'Вернуть управление игрокам'},
        ])
        self.PUB.controls('Дирижабль(принудительная высота) Нажимать только если Пульт совсем накрылся!', [
            {'method': 'room1.pultradio.set_override_height', 'descr': u'Сделать вид, что дирижабль на высоте 3400'},
            {'method': 'room1.pultradio.unset_override_height', 'descr': u'Отмена'},
        ])

        self.hit_sub_count = 0

        self.room1.pultradio.setup(get_config('airship_speed'))
        self.room1.pultradio.go_to_solved()
        self.room1.pultradio.enable_handles()
        self.room3.pochinka.setup()
        self.room3.okulyar.setup()
        self.room4.ugol.setup()
        self.room4.ugol.go_to_game()
        self.room5.turel.setup(recharge=get_config('turel_recharge_speed'))
        self.room5.turel.go_to_game()

        self.room1.pultradio.subscribe('height',[
            lambda height: self.room4.ugol.set_height(height),
            lambda height: self.proxy().set_current_height(height),
        ])
        self.scheduler.schedule(
            lambda: self.proxy().set_current_height(self.room1.pultradio.get_height().get())
        , 1000, once=True)

        self.room1.pultradio.activate_height()
        self.room1.pultradio.activate_help()
        self.scheduler.schedule(
            lambda: self.room4.ugol.set_height(self.room1.pultradio.get_height().get())
        , 1000, once=True)

        self.room5.turel.subscribe('shot',
            lambda: self.snds.snd_ochered()
        )
        self.room5.turel.subscribe('noarm',[
            lambda: self.ach_unset(u"Прагматик"),
            lambda: self.snds.snd_osechka()
        ])
        self.room5.turel.subscribe('recharge_start',
            lambda: self.snds.resume('perezaryadka')
        )
        self.room5.turel.subscribe('recharge_stop',
            lambda: self.snds.pause('perezaryadka')
        )

        self.room5.turel.subscribe('hit_by_rocket',[
            lambda: self.snds.snd_v_nas_popali(),
            #lambda: self.proxy().hit_by_rocket(),
        ])
        self.room5.turel.subscribe('breakage',[
            lambda: pprint('event: breakage'),
            lambda: self.proxy().got_breakage(),
        ])
        self.room5.turel.subscribe('repaired',[
            lambda: pprint('event: repaired'),
            lambda: self.proxy().got_repaired(),
        ])
        self.room5.turel.activate_interaction()

        trl = self.room5.turel
        self.scheduler.schedule(self.check_holes, 1000)


        self.room3.pochinka.subscribe('bomb_loaded',[
            lambda: self.PUB.log(u'[{0}] Бомба загружена в бомбоприемник.'.format(self.get_time())),
            lambda: self.proxy().bomb_loaded()
        ])
        self.room3.pochinka.activate_bombpriem()

        pch = self.room3.pochinka
        pch.activate()
        pch.subscribe('valve00_turned', lambda turns: trl.fix_hole(0, turns))
        pch.subscribe('valve01_turned', lambda turns: trl.fix_hole(1, turns))
        pch.subscribe('valve02_turned', lambda turns: trl.fix_hole(2, turns))
        pch.subscribe('valve03_turned', lambda turns: trl.fix_hole(3, turns))
        pch.subscribe('valve10_turned', lambda turns: trl.fix_hole(4, turns))
        pch.subscribe('valve11_turned', lambda turns: trl.fix_hole(5, turns))
        pch.subscribe('valve12_turned', lambda turns: trl.fix_hole(6, turns))
        pch.subscribe('valve13_turned', lambda turns: trl.fix_hole(7, turns))

        okl = self.room3.okulyar
        ugl = self.room4.ugol
        ugl.subscribe('bomb_launch',[
            lambda: self.bomb_count_inc(),
            lambda: self.snds.snd_sbros_bombi(),
            lambda: pprint("event: bomb_launch")
        ])
        ugl.subscribe('bomb_hit', [
            lambda: pprint("event: bomb_hit"),
            lambda: self.proxy().hit_in_sub()
        ])
        ugl.subscribe('bomb_miss',[
            lambda: pprint("event: bomb_miss"),
            lambda: self.snds.play('promah'),
            #lambda: self.video.queue(['FLUSH', 'mimo', 'otvet'])
        ])
        ugl.subscribe('bomb_recharge',
            lambda: pprint("event: bomb_recharge")
        )
        ugl.subscribe('donotpress', [
            lambda: self.proxy().dont_press(),
            lambda: pprint("event: donotpress"),
        ])
        ugl.activate_utils()
        ugl.activate_fight()

        #for tests
        #ugl.set_height(1000)

        self.scheduler.schedule(self.move_tick, 1000)

        #self.room4.ugol.subscribe('solved',
            #lambda: self.proxy().actions_on_solved()
        #)

        self.snds.setup('ochered_1')
        self.snds.setup('osechka_1')
        self.snds.setup('sbros_bombi_1')
        self.snds.setup('v_nas_popali_1')
        self.snds.setup('perezaryadka')

        self.scheduler.schedule(
            self.shdld_snds,
            1000*20
        )

        self.help_subs_man1()

    def help_subs_man1(self):
        self.room4.ugol.subscribe('help',
            lambda: self.snds.play('manual1'+get_config('lang'))
        )
        self.room1.pultradio.subscribe('help',
            lambda: self.snds.play('manual1'+get_config('lang'))
        )

    def help_subs_man2(self):
        self.room4.ugol.subscribe('help',
            lambda: self.snds.play('manual2'+get_config('lang'))
        )
        self.room1.pultradio.subscribe('help',
            lambda: self.snds.play('manual2'+get_config('lang'))
        )


    def bomb_count_inc(self):
        print "bomb_count_inc before", self.bomb_count
        self.bomb_count += 1
        print "bomb_count_inc after", self.bomb_count

    def set_current_height(self, height):
        self.current_height = height
        self.PUB.height(height)

        if height >= 4950:
            self.ach_set(u"Икар")

        self.proxy().check_current_height(height)

    def scheduled_sounds(self):
        height = getattr(self, 'current_height', None)
        if height is None:
            return

        if self.breaked and height < 500:
            self.snds.play('pochinite_fuzelyazh_i_naberite_visotu'+get_config('lang'))

        elif self.breaked:
            self.snds.play('pochinite_fuzelyazh'+get_config('lang'))

        elif height < 500:
            self.snds.play('malaya_visota'+get_config('lang'))
            self.scheduler.schedule(
                lambda: self.snds.play('naberite_visotu'+get_config('lang')),
                1000*3.5,
                once=True
            )
        elif height > 4500:
            self.snds.play('bolshaya_visota')

    def bomb_launch_events(self):
        self.room4.ugol.unsubscribe('bomb_launch')
        self.room4.ugol.subscribe('bomb_launch',[
            lambda: self.snds.snd_sbros_bombi(),
            lambda: self.bomb_count_inc(),
            lambda: pprint("event: bomb_launch")
        ])

    def check_current_height(self, height):
        if height < 500:
            self.room4.ugol.go_to_standby()
            self.room4.ugol.unsubscribe('bomb_launch')
        else:
            self.room4.ugol.unsubscribe('bomb_launch')
            self.room4.ugol.subscribe('bomb_launch',[
                lambda: self.snds.snd_sbros_bombi(),
                lambda: self.bomb_count_inc(),
                lambda: pprint("event: bomb_launch")
            ])
            if not self.breaked:
                self.room4.ugol.go_to_game()
                self.room4.ugol.set_emergency(False)

    def dont_press(self):
        self.ach_unset(u"Сила воли")
        self.room1.karta.light_dontpress()
        self.snds.pause('perezaryadka')
        self.snds.play('dont_push_30s'+get_config('lang')),
        self.room1.pultradio.disable_handles()
        self.room4.ugol.go_to_standby()
        self.room5.turel.go_to_standby()
        self.room4.ugol.unsubscribe('bomb_launch')
        self.scheduler.schedule(
            lambda: self.proxy().dont_press_end(),
            1000*32,
            once=True
        )
        self.room1.pultradio.unsubscribe('help')
        self.room4.ugol.unsubscribe('help')
        if self.step_stage == 'start':
            self.snds.stop_play('manual1'+get_config('lang'))
        elif self.step_stage == 'bomb_loaded':
            self.snds.stop_play('manual2'+get_config('lang'))

    def dont_press_end(self):
        self.room1.karta.light_on()
        self.room1.pultradio.enable_handles()
        self.room4.ugol.go_to_game()
        self.room5.turel.go_to_game()
        self.room4.ugol.unsubscribe('bomb_launch')
        self.room4.ugol.subscribe('bomb_launch',[
            lambda: self.snds.snd_sbros_bombi(),
            lambda: self.bomb_count_inc(),
            lambda: pprint("event: bomb_launch")
        ])
        if self.step_stage == 'start':
            self.help_subs_man1()
        elif self.step_stage == 'bomb_loaded':
            self.help_subs_man2()


    def bomb_loaded(self):
        self.snds.play('manual2'+get_config('lang'))
        self.snds.play('boevaya_tema_v1')
        self.room1.pultradio.unsubscribe('help')
        self.room4.ugol.unsubscribe('help')
        self.help_subs_man2()
        self.step_stage == 'bomb_loaded'

    def hit_by_rocket(self):
        if self.smoke_count:
            self.smoke_count -= 1
            self.room3.pochinka.smoke()

    def selfhealing(self):
        self.ach_unset(u"Механик")
        self.snds.stop_play('sirena')
        self.snds.play('ne_pochinen_fuzelyazh'+get_config('lang'))
        self.scheduler.schedule(
            lambda: self.proxy().got_repaired()
        , 1000*35, once=True)
        self.scheduler.schedule(
            lambda: self.room5.turel.selfhealing()
        , 1000*35, once=True)


    def got_breakage(self):
        self.PUB.log(u'[{0}] Дирижабль был подбит и начал снижение.'.format(self.get_time()))
        self.scheduler.unschedule(self.goal_rem_clbk)
        self.scheduler.unschedule(self.more_rockets_clbk)
        self.room1.karta.light_crash()
        self.snds.play('sirena')
        self.room5.turel.setup(
            uron=get_config('rocket_damage'),
            rockets=0,
            recharge=get_config('turel_recharge_speed'),
        )
        self.room4.ugol.set_emergency(True)
        self.room4.ugol.go_to_standby()
        self.room1.pultradio.breakage()
        self.hit_by_rocket()

        self.breaked = True
        self.scheduler.schedule(
            self.shdld_selfhealing
        , 1000*60*2, once=True)

    def got_repaired(self):
        self.goal_rem_clbk()
        self.scheduler.unschedule(self.shdld_selfhealing)
        self.room3.okulyar.unhide_lodka()
        self.room1.karta.light_on()
        self.snds.stop_play('sirena')
        self.room4.ugol.set_emergency(False)
        self.room4.ugol.go_to_game()
        self.room1.pultradio.repaired()
        self.room1.pultradio.enable_handles()
        if not get_config('disable_turel_after_hit'):
            self.video.queue(['vspliv'])
            self.room5.turel.setup(
                uron=get_config('rocket_damage'),
                rockets=get_config('rockets_count'),
                recharge=get_config('turel_recharge_speed'),
            )
        self.breaked = False

    def start_fight(self):
        self.PUB.log(u'[{0}] Лодка начала вести ответный огонь.'.format(self.get_time()))
        self.PUB.controls('Угол наводчика', [
            {'method': 'room4.ugol.trigger_bomb_hit', 'descr': u'Сделать вид, что следующий выстрел - попадание'},
        ])
        self.PUB.controls('Дирижабль', [
            {'method': 'room5.turel.selfhealing', 'descr': u'Починить дирижабль'},
            {'method': 'room5.turel.full_destroy', 'descr': u'Уничтожить дирижабль'},
        ])
        self.room5.turel.setup(
            uron=get_config('rocket_damage'),
            rockets=get_config('rockets_count'),
            recharge=get_config('turel_recharge_speed'),
        )
        self.room5.turel.subscribe('achivka',
            lambda: self.ach_set(u"Снайпер")
        )
        self.room5.turel.activate_fight()
        self.room4.ugol.go_to_game()
        self.room4.ugol.unsubscribe('help')
        self.room1.pultradio.unsubscribe('help')
        self.help_subs_man2()

        self.room4.ugol.unsubscribe('bomb_launch')
        self.scheduler.schedule(
            self.more_rockets_clbk
        , 1000*60*1.5, once=True)
        self.scheduler.schedule(
            lambda: self.room3.okulyar.unhide_lodka()
        , 1000*60*2, once=True)
        self.scheduler.schedule(
            lambda: self.proxy().bomb_launch_events()
        , 1000*60*2.1, once=True)
        self.scheduler.schedule(
            self.goal_rem_clbk
        , 1000*60*2.5, once=True)

    def hit_in_sub(self):
        if self.bomb_count <= 1:
            print "hit_in_sub bomb_count", self.bomb_count
            self.ach_set(u"Подрывник")
        if self.current_height <= 1500:
            self.ach_set(u"Ласточка")

        self.hit_sub_count += 1
        if self.hit_sub_count == 1:
            self.video.queue(['FLUSH', 'popal', 'otvet'])
            self.snds.play('pervoe_popadanie'),
            self.room1.pultradio.unsubscribe('help')
            self.room4.ugol.unsubscribe('help')
            self.snds.stop_play('manual2'+get_config('lang'))
            self.scheduler.schedule(
                lambda: self.snds.play('replika3sh'+get_config('lang'))
            , 1000*7, once=True)
            self.scheduler.schedule(
                lambda: self.snds.play('manual3'+get_config('lang'))
            , 1000*(45+7), once=True)
            self.scheduler.schedule(
                lambda: self.proxy().start_fight()
            , 1000*(45+7+57), once=True)
            self.room3.okulyar.change_route()
            self.room3.okulyar.hide_lodka()
        elif self.hit_sub_count == 2:
            self.video.queue(['FLUSH', 'win'])
            self.snds.play('vtoroe_popadanie'),
            self.actions_on_solved()

    def actions_on_solved(self):
        self.solved_clbk()

    def deactivate(self):
        self.room1.pultradio.deactivate()
        self.room3.pochinka.deactivate()
        self.room4.ugol.deactivate()
        self.room5.turel.deactivate()
        self.scheduler.unschedule(self.shdld_snds)
        self.scheduler.unschedule(self.move_tick)
        self.scheduler.unschedule(self.check_holes)

    def repeat(self):
        pass
        #self.scheduler.unschedule(self.solved_clbk)
        #self.deactivate()
        #self.setup_logic()

class StepEnd(Step):
    name = u'Конец. Завершение игры'
    def __init__(self, step, room0, room1, room2, room3, room4, room5):
        super(Step, self).__init__()
        self.step = step
        self.room0 = room0
        self.room1 = room1
        self.room2 = room2
        self.room3 = room3
        self.room4 = room4
        self.room5 = room5

        self.players_count_clbk = lambda: self.proxy().get_players_count()
        self.dim_svet_clbk = lambda: self.proxy().dim_svet()

        self.svet_sidushki = True

    def on_start(self):
        for puzzle in [
            self.room0.achivki,
            self.room0.vhod,
            self.room1.yakor,
            self.room1.sidushki,
            self.room1.pultradio,
            self.room1.karta,
            self.room1.telefon,
            self.room2.mahovik,
            self.room3.pochinka,
            self.room3.okulyar,
            self.room4.ugol,
            self.room5.turel,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room0.vhod.init()
        self.room1.yakor.init()
        self.room1.sidushki.init()
        self.room1.pultradio.init()
        self.room1.telefon.init()
        self.room2.mahovik.init()
        self.room3.pochinka.init()
        self.room3.okulyar.init()
        self.room4.ugol.init()
        self.room5.turel.init()

    def setup_logic(self):
        self.PUB.log(u"""
[{0}]
Что сейчас должны сделать игроки:
    - занять места для сидения в первой комнате
""".format(self.get_time()), log_level='legend')
        self.PUB.controls('Вход в квест', [
            {'method': 'room0.vhod.open_vhod', 'descr': u'Открыть вход'},
            {'method': 'room0.vhod.close_vhod', 'descr': u'Закрыть вход'},
        ])
        self.PUB.controls('Сидушки', [
            {'method': 'room1.yakor.force_all_players_sat', 'descr': u'Сделать вид, что все игроки сели'},
        ])
        self.snds.stop_play('perezaryadka')
        self.snds.stop_play('manual2'+get_config('lang'))
        self.snds.stop_play('glavnaya_tema_v1')
        self.snds.stop_play('glavnaya_tema_v1')
        self.snds.stop_play('boevaya_tema_v1')
        self.snds.play('final_crash_vals')
        self.snds.play('final_crash_3min'+get_config('lang'))

        self.room1.karta.light_crash()
        self.room0.vhod.setup()

        self.room1.sidushki.setup()

        self.room1.yakor.setup()
        self.scheduler.schedule(self.players_count_clbk, 1000)

        self.room2.mahovik.setup()
        self.room2.mahovik.off()
        self.scheduler.schedule(self.dim_svet_clbk, 1000)

        self.room1.pultradio.setup(get_config('airship_speed'))
        self.room1.pultradio.go_to_solved()
        self.room1.pultradio.breakage()

        self.room1.telefon.setup(get_config('lang'))
        self.room1.telefon.go_to_standby()

        self.room3.pochinka.setup()
        self.room3.pochinka.smoke()

        self.room3.okulyar.setup()
        self.room3.okulyar.disable()

        self.room4.ugol.setup()
        self.room4.ugol.set_emergency(True)

        self.room5.turel.setup()
        self.room5.turel.full_destroy()
        self.room5.turel.go_to_standby()

    def dim_svet(self):
        if self.svet_sidushki:
            self.room2.mahovik.sidushki_svet_off()
            self.svet_sidushki = False
        else:
            self.room2.mahovik.sidushki_svet_on()
            self.svet_sidushki = True

    def get_players_count(self):
        players_count = self.room1.yakor.get_players_count().get()
        if players_count >= get_config('players'):
            self.scheduler.unschedule(self.dim_svet_clbk)
            self.scheduler.unschedule(self.players_count_clbk)
            self.proxy().all_sat_down()

    def all_sat_down(self):
        self.room2.mahovik.sidushki_svet_off()
        self.room1.vagonetki.go_to_standby()
        self.snds.stop_play('final_crash_3min'+get_config('lang'))
        self.snds.play('final_crash_posadka'+get_config('lang'))
        self.scheduler.schedule(
            lambda: self.snds.stop_play('final_crash_vals')
            , 2000,
            once=True
        )
        self.scheduler.schedule(
            lambda: self.proxy().epic_end()
            , 1000 * 25,
            once=True
        )


    def epic_end(self):
        self.room2.mahovik.klapan_on()
        self.room1.sidushki.vibro_on()
        self.room1.karta.light_off()
        self.scheduler.schedule(
            lambda: self.proxy().actions_on_solved()
            , 1000 * 10,
            once=True
        )


    def actions_on_solved(self):
        self.snds.stop_play('final_crash_posadka'+get_config('lang'))
        self.scheduler.schedule(
            lambda: self.snds.play('moyka'+get_config('lang'))
            , 1000 * 5,
            once=True
        )
        self.room1.sidushki.vibro_off()
        self.room1.vagonetki.go_to_standby()
        self.room2.mahovik.klapan_off()
        self.room1.karta.light_on()
        self.room2.mahovik.sidushki_svet_off()
        self.room0.vhod.open_vhod()
        self.room0.vhod.plate_on()

        end_time = datetime.now()
        GAME = Resources().require('game')
        start_time = GAME._actor.start_time
        if end_time - start_time > timedelta(minutes=55):
            self.ach_unset(u"Крепкий орешек")

        achivki = GAME._actor.game_achivki
        for ach, yes in achivki.items():
            if yes:
                self.room0.achivki.achieve(ach)

        ach_count = len(filter(None, [val for key, val in achivki.items()]))
        rank = ach_count/3
        self.room0.achivki.rank(rank)

        GAME = Resources().require('game')
        GAME.game_win()
        self.solved_clbk()

    def deactivate(self):
        self.room1.pultradio.deactivate()
        self.room4.ugol.deactivate()
        self.room5.turel.deactivate()

    def repeat(self):
        pass
        #self.scheduler.unschedule(self.solved_clbk)
        #self.deactivate()
        #self.setup_logic()


class StepForceEnd(Step):
    name = u'Принудительное завершение игры'
    def __init__(self, step, room0, room1, room2, room3, room4, room5):
        super(Step, self).__init__()
        self.step = step
        self.room0 = room0
        self.room1 = room1
        self.room2 = room2
        self.room3 = room3
        self.room4 = room4
        self.room5 = room5

        self.players_count_clbk = lambda: self.proxy().get_players_count()
        self.dim_svet_clbk = lambda: self.proxy().dim_svet()

        self.svet_sidushki = True

    def on_start(self):
        for puzzle in [
            self.room0.achivki,
            self.room0.vhod,
            self.room1.yakor,
            self.room1.sidushki,
            self.room1.pultradio,
            self.room1.telefon,
            self.room2.mahovik,
            self.room3.pochinka,
            self.room3.okulyar,
            self.room4.ugol,
            self.room5.turel,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room0.vhod.init()
        self.room1.yakor.init()
        self.room1.sidushki.init()
        self.room1.pultradio.init()
        self.room1.telefon.init()
        self.room2.mahovik.init()
        self.room3.pochinka.init()
        self.room3.okulyar.init()
        self.room4.ugol.init()
        self.room5.turel.init()

    def setup_logic(self):
        self.PUB.log(u"""
[{0}]
Что сейчас должны сделать игроки:
    - занять места для сидения в первой комнате
""".format(self.get_time()), log_level='legend')
        self.PUB.controls('Вход в квест', [
            {'method': 'room0.vhod.open_vhod', 'descr': u'Открыть вход'},
            {'method': 'room0.vhod.close_vhod', 'descr': u'Закрыть вход'},
        ])
        self.PUB.controls('Сидушки', [
            {'method': 'room1.yakor.force_all_players_sat', 'descr': u'Сделать вид, что все игроки сели'},
        ])
        self.snds.stop_play('perezaryadka')
        self.snds.stop_play('manual2'+get_config('lang'))
        self.snds.stop_play('glavnaya_tema_v1')
        self.snds.stop_play('boevaya_tema_v1')
        self.snds.stop_play('bolshaya_visota')
        self.snds.stop_play('bolshaya_visota_eng')
        self.snds.stop_play('cel_ne_ustanovlena')
        self.snds.stop_play('cel_ne_ustanovlena_eng')
        self.snds.stop_play('dont_push_30s')
        self.snds.stop_play('dont_push_30s_eng')
        self.snds.stop_play('final_crash_3min')
        self.snds.stop_play('final_crash_3min_eng')
        self.snds.stop_play('final_crash_posadka')
        self.snds.stop_play('final_crash_posadka_eng')
        self.snds.stop_play('moyka')
        self.snds.stop_play('final_crash_vals')
        self.snds.stop_play('glavnaya_tema_v1')
        self.snds.stop_play('help1')
        self.snds.stop_play('help1_eng')
        self.snds.stop_play('help2')
        self.snds.stop_play('help2_eng')
        self.snds.stop_play('help3')
        self.snds.stop_play('help3_eng')
        self.snds.stop_play('help4')
        self.snds.stop_play('help4_eng')
        self.snds.stop_play('malaya_visota')
        self.snds.stop_play('malaya_visota_eng')
        self.snds.stop_play('manual1')
        self.snds.stop_play('manual1_eng')
        self.snds.stop_play('manual2')
        self.snds.stop_play('manual2_eng')
        self.snds.stop_play('manual3')
        self.snds.stop_play('manual3_eng')
        self.snds.stop_play('naberite_visote_eng')
        self.snds.stop_play('naberite_visotu')
        self.snds.stop_play('ne_pochinen_fuzelyazh')
        self.snds.stop_play('ne_pochinen_fuzelyazh_eng')
        self.snds.stop_play('nedostatochnaya_visota_dlya_mayaka')
        self.snds.stop_play('nedostatochnaya_visota_dlya_mayaka_eng')
        self.snds.stop_play('net_mayaka')
        self.snds.stop_play('net_mayaka_eng')
        self.snds.stop_play('pochinite_fuzelyazh')
        self.snds.stop_play('pochinite_fuzelyazh_eng')
        self.snds.stop_play('pochinite_fuzelyazh_i_naberite_visotu')
        self.snds.stop_play('pochinite_fuzelyazh_i_naberite_visotu_eng')
        self.snds.stop_play('polet')
        self.snds.stop_play('proshel_chas')
        self.snds.stop_play('proshel_chas_eng')
        self.snds.stop_play('pervoe_popadanie')
        self.snds.stop_play('vtoroe_popadanie')
        self.snds.stop_play('promah')
        self.snds.stop_play('replika1sh')
        self.snds.stop_play('replika1sh_eng')
        self.snds.stop_play('replika2sh')
        self.snds.stop_play('replika2sh_eng')
        self.snds.stop_play('replika3sh')
        self.snds.stop_play('replika3sh_eng')
        self.snds.stop_play('sirena')
        self.snds.stop_play('sbros_bombi')
        self.snds.stop_play('v_nas_popali')
        self.snds.stop_play('shum_machinnogo_zala_2min')
        self.snds.stop_play('shum_machinnogo_zala_start')
        self.snds.stop_play('tresk_v_topke_2_min')
        self.snds.stop_play('vzlet')

        self.snds.play('final_crash_vals')
        self.snds.play('final_crash_3min'+get_config('lang'))
        self.snds.play('sirena')

        self.room1.karta.light_crash()
        self.room0.vhod.setup()

        self.room1.sidushki.setup()

        self.room1.yakor.setup()
        self.scheduler.schedule(self.players_count_clbk, 1000)

        self.room2.mahovik.setup()
        self.room2.mahovik.off()
        self.scheduler.schedule(self.dim_svet_clbk, 1000)

        self.room1.pultradio.setup(get_config('airship_speed'))
        self.room1.pultradio.go_to_solved()
        self.room1.pultradio.breakage()

        self.room1.telefon.setup(get_config('lang'))
        self.room1.telefon.go_to_standby()

        self.room3.pochinka.setup()
        self.room3.pochinka.smoke()

        self.room3.okulyar.setup()
        self.room3.okulyar.disable()

        self.room4.ugol.setup()
        self.room4.ugol.set_emergency(True)

        self.room5.turel.setup()
        self.room5.turel.full_destroy()
        self.room5.turel.go_to_standby()


    def dim_svet(self):
        if self.svet_sidushki:
            self.room2.mahovik.sidushki_svet_off()
            self.svet_sidushki = False
        else:
            self.room2.mahovik.sidushki_svet_off()
            self.svet_sidushki = True

    def get_players_count(self):
        players_count = self.room1.yakor.get_players_count().get()
        if players_count >= get_config('players'):
            self.scheduler.unschedule(self.dim_svet_clbk)
            self.scheduler.unschedule(self.players_count_clbk)
            self.proxy().all_sat_down()


    def all_sat_down(self):
        self.room2.mahovik.sidushki_svet_off()
        self.room1.vagonetki.go_to_standby()
        self.scheduler.schedule(
            lambda: self.snds.stop_play('final_crash_vals')
            , 2000,
            once=True
        )
        self.scheduler.schedule(
            lambda: self.proxy().epic_end()
            , 1000 * 3,
            once=True
        )


    def epic_end(self):
        self.room2.mahovik.klapan_on()
        self.room1.sidushki.vibro_on()
        self.room1.karta.light_off()
        self.scheduler.schedule(
            lambda: self.proxy().actions_on_solved()
            , 1000 * 10,
            once=True
        )


    def actions_on_solved(self):
        self.snds.stop_play('sirena')
        self.snds.stop_play('final_crash_vals')
        self.snds.stop_play('final_crash_3min'+get_config('lang'))
        self.room1.sidushki.vibro_off()
        self.room1.vagonetki.go_to_standby()
        self.room2.mahovik.klapan_off()
        self.room1.karta.light_on()
        self.room2.mahovik.sidushki_svet_off()
        self.room0.vhod.open_vhod()
        self.room0.vhod.plate_on()

        self.ach_unset(u'Крепкий орешек')
        self.ach_unset(u'Прагматик')
        self.ach_unset(u'Механик')
        self.ach_unset(u'Сила воли')

        end_time = datetime.now()
        GAME = Resources().require('game')
        start_time = GAME._actor.start_time

        achivki = GAME._actor.game_achivki
        for ach, yes in achivki.items():
            if yes:
                self.room0.achivki.achieve(ach)

        ach_count = len(filter(None, [val for key, val in achivki.items()]))
        rank = ach_count/3
        self.room0.achivki.rank(rank)

        GAME = Resources().require('game')
        GAME.game_win()
        self.solved_clbk()


    def deactivate(self):
        self.room1.pultradio.deactivate()
        self.room4.ugol.deactivate()
        self.room5.turel.deactivate()

    def repeat(self):
        pass
        #self.scheduler.unschedule(self.solved_clbk)
        #self.deactivate()
        #self.setup_logic()


class StepPrepareHuman(Step):
    name = u'Готовим комнату к новой игре(ручной этап)'
    def __init__(self, step, room0, room1, room2, room3, room4, room5):
        super(Step, self).__init__()
        self.step = step
        self.room0 = room0
        self.room1 = room1
        self.room2 = room2
        self.room3 = room3
        self.room4 = room4
        self.room5 = room5

    def on_start(self):
        pass

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.PUB.log(u"""
Что нужно сейчас сделать:
    - убрать фигурки Маяков в Секретер и закрыть его
    - опустить Якорь и вставить ручку обратно в Пульт
    - вынуть Палено из Печи и разобрать его
    - вынуть Батарейки из Мастермайнда и вставить их в слоты зарядки
    - вынуть Касету с бомбами из Бомбоприемника, засунуть ее в Конфигуратор и закрыть дверцу Конфигуратора (ДАЖЕ ЕСЛИ ИГРОКИ НЕ ДОШЛИ ДО ЭТОЙ ЗАГАДКИ, НУЖНО ЗАЙТИ В АРСЕНАЛ И ЗАКРЫТЬ ДВЕРЦУ КОНФИГУРАТОРА)
    - провернуть элементы Бомбы в Кофигураторе случайным образом
    - проверить, что телевзор в полу включен, если нет - включить
    - засунуть обратно все карточки в Картотеке
    - закрыть Шлюз(дверь в арсенал, комнату с огромным телеком в полу)
    - закрыть Дверь(дверь в машинный зал, команту где Печь)

Если все выше перечисленное выполнено, можно переходить с следующему этапу (кнопка 'Страт игры', она станет доступна примерно через 10 секунд после начала этого шага)
""", log_level='tip')

    def setup_logic(self):

        self.room0.achivki.init()
        self.room0.achivki.reset()

        self.room0.vhod.init()
        self.room0.vhod.reset()

        self.room0.config_panel.init()
        self.room0.config_panel.reset()

        self.room1.gotovnost.init()
        self.room1.gotovnost.reset()

        self.room1.vagonetki.init()
        self.room1.vagonetki.reset()

        self.room1.yakor.init()
        self.room1.yakor.reset()

        self.room1.sidushki.init()
        self.room1.sidushki.reset()

        self.room1.karta.init()
        self.room1.karta.reset()

        self.room1.telefon.init()
        self.room1.telefon.reset()

        self.room1.pultradio.init()
        self.room1.pultradio.reset()

        self.room1.dver.init()
        self.room1.dver.reset()

        self.room2.mahovik.init()
        self.room2.mahovik.reset()

        self.room2.shluz.init()
        self.room2.shluz.reset()

        self.room2.pech.init()
        self.room2.pech.reset()

        self.room2.balans.init()
        self.room2.balans.reset()

        self.room2.mastermind.init()
        self.room2.mastermind.reset()

        self.room3.konfigurator.init()
        self.room3.konfigurator.reset()

        self.room3.pochinka.init()
        self.room3.pochinka.reset()

        self.room3.okulyar.init()
        self.room3.okulyar.reset()

        self.room4.ugol.init()
        self.room4.ugol.reset()

        self.room4.komposter.init()
        self.room4.komposter.reset()

        self.room5.turel.init()
        self.room5.turel.reset()

        self.scheduler.schedule(lambda: self.proxy().setup_devices(), 5000, once=True)

    def setup_devices(self):
        self.room0.achivki.setup()
        self.room0.vhod.setup()
        self.room0.vhod.open_vhod()
        self.room0.config_panel.setup()
        self.room1.gotovnost.setup()
        self.room1.vagonetki.setup()
        self.room1.yakor.setup()
        self.room1.sidushki.setup()
        self.room1.karta.setup()
        self.room1.telefon.setup()
        self.room1.pultradio.setup()
        self.room1.dver.setup()
        self.room1.dver.open_dver()
        self.scheduler.schedule(
            lambda: self.room1.dver.close_dver(),
        1000, once=True)
        self.room2.mahovik.setup()
        self.room2.mahovik.sidushki_svet_on()
        self.room2.shluz.setup()
        self.room2.shluz.open_shluz()
        self.scheduler.schedule(
            lambda: self.room2.shluz.close_shluz(),
        1000, once=True)
        self.room2.pech.setup()
        self.room2.pech.compressor_enable()
        self.room2.balans.setup()
        self.room2.mastermind.setup()
        self.room3.konfigurator.setup()
        self.room3.konfigurator.go_to_solved()
        self.scheduler.schedule(
            lambda: self.room3.konfigurator.go_to_game(),
        1000, once=True)
        self.room3.pochinka.setup()
        self.room3.okulyar.setup()
        self.room3.okulyar.disable()
        self.room4.ugol.setup()
        self.room4.komposter.setup()
        self.room5.turel.setup()
        self.scheduler.schedule(self.solved_clbk, 1000, once=True)


        self.scheduler.schedule(lambda: self.PUB.event({'enable': 'start_game'}), 5000, once=True)

    def deactivate(self):
        pass

    def repeat(self):
        pass
        #self.deactivate()
        #self.setup_logic()


def test_step():
    room = TestRoom_1()
    step = Step_test.start(room).proxy()
    step.init().get()
    step.start_step().get()


# игра - содержить в себе всю логику
# инициализирует все загадки и определяет
# взаимоотношения между ними
class Game(Actor):

    def __init__(self):
        super(Game, self).__init__()
        self.steps = None
        self.active_step = None
        self.solved_steps = []
        self.started = None
        self.PUB = Resources().require('PUB')
        self.vhod_opened = None
        self.start_time = None
        self.last_next_step = None

        self.game_config = {
            'airship': 'fast',
            'players': 4,
            'lang': 'rus',
        }
        self.game_achivki = {
            u'Поехали!': False ,
            u'Быстрый старт': False ,
            u'Фанат': False ,
            u'1 на 360': False ,
            u'Энерджайзер': False ,
            u'Подгоревший': False ,
            u'Чистая энергия': False ,
            u'Икар': False ,
            u'Никто не забыт': False ,
            u'Настоящий друг': False ,
            u'На краю света': False ,
            u'Подрывник': False ,
            u'Снайпер': False ,
            u'Ласточка': False ,

            u'Прагматик': True ,
            u'Механик': True ,
            u'Сила воли': True ,
            u'Крепкий орешек': True ,
        }

    def proxy(self):
        return self.actor_ref.proxy()

    def init(self):
        #import pudb; pu.db
        self.started = True
        self.room0 = Room0()
        self.room1 = Room1()
        self.room2 = Room2()
        self.room3 = Room3()
        self.room4 = Room4()
        self.room5 = Room5()
        self.steps = []
        self.init_steps = [
            StepInit.start(0, self.room0, self.room1, self.room2,
                           self.room3, self.room4, self.room5).proxy(),
            #StepTest.start(0, self.room0).proxy(),
        ]

        self.prepare_steps = [
            StepPrepareHuman.start(50, self.room0, self.room1, self.room2,
                                   self.room3, self.room4, self.room5).proxy(),
        ]

        self.game_steps = [
            StepIdle.start(1, self.room0, self.room2).proxy(),
            StepStartTimer.start(2).proxy(),
            StepYakorVagonetki.start(3, self.room0, self.room1).proxy(),
            StepPechMastermind.start(4, self.room0, self.room1, self.room2).proxy(),
            StepBalansirovka.start(5, self.room0, self.room1, self.room2).proxy(),
            StepVzlet.start(6, self.room0, self.room1).proxy(),
            StepKartaMayaki.start(7, self.room0, self.room1).proxy(),
            StepFlight.start(8, self.room0, self.room1, self.room2).proxy(),
            StepBombConfig.start(9, self.room0, self.room1,self.room2, self.room3,
                                    self.room4, self.room5).proxy(),
            StepFight.start(10, self.room0, self.room1, self.room3,
                               self.room4, self.room5).proxy(),

            StepEnd.start(11, self.room0, self.room1, self.room2, self.room3,
                              self.room4, self.room5).proxy(),
        ]
        self.force_end_steps = [
            StepForceEnd.start(20, self.room0, self.room1, self.room2,
                                   self.room3, self.room4, self.room5).proxy(),
        ]

    def game_init(self):
        self.init()
        self.steps = self.init_steps
        self.proxy().next_step()
        return self.steps[:]

    def prepare_new_game(self):
        if self.active_step:
            self.active_step.unsubscribe('fail')
            self.active_step.unsubscribe('solved')
            #if not self.active_step.failed.get():
            self.active_step.step_solved()
            self.solved_steps.append(self.active_step)

        self.steps = self.prepare_steps
        self.proxy().next_step()
        return self.steps[:]

    def force_game_end(self):
        if self.active_step:
            self.active_step.unsubscribe('fail')
            self.active_step.unsubscribe('solved')
            #if not self.active_step.failed.get():
            self.active_step.step_solved()
            self.solved_steps.append(self.active_step)

        self.steps = self.force_end_steps
        self.proxy().next_step()
        return self.steps[:]


    def game_start(self):
        self.steps = self.game_steps
        self.proxy().next_step()
        self.sound_eng = Resources().require('SOUND')
        self.scheduler = Scheduler()
        self.scheduler.schedule(lambda: self.sound_eng.check_cpu(), 10000)
        return self.steps[:]

    def game_over(self):
        self.PUB.log(u"""
Прошло 75 минут игры. Игроки не успели выбраться за отведенное время.
Но если им осталось совсем чуть-чуть до выхода, можно не завершать игру и немного подождать.
Чтобы не портить людям удовольствие.

Если все совсем плохо и они точно не успевают, кликните на кнопку 'Перейти к шагу преждевременного завершения'. На этом шаге дирижабль как-бы подбивают и игра заканчивается, когда игроки садятся на сидушки.
""", log_level='legend')
        self.PUB.controls('Завершение игры', [
            {'method': 'force_game_end', 'descr': u'Перейти к шагу преждевременного завершения'},
        ])

    def game_win(self):
        self.end_time = datetime.now()
        self.PUB.log(u"""
Успех. Игроки успели выбраться из команты.
""", log_level='legend')
        if self.start_time:
            self.PUB.log(u"""
Игра длилась: [{0}].
""".format(strfdelta(self.end_time - self.start_time)), log_level='legend')


    def next_step(self, debounce=False):
        if debounce:
            if self.last_next_step:
                if ( (datetime.now() - self.last_next_step) < timedelta(seconds=1) ):
                    return

        self.last_next_step = datetime.now()

        self.PUB.event({'temp_disable': 'next_step'})
        if self.active_step:
            #self.active_step.unsubscribe('fail')
            self.active_step.unsubscribe('solved')
            #if not self.active_step.failed.get():
            self.active_step.step_solved()
            self.active_step.finishing()
            self.solved_steps.append(self.active_step)

        if not len(self.steps):
            return

        self.active_step = self.steps.pop(0)
        #self.PUB.log(u'Переход к следующем шагу: {0}'.format(self.active_step.name.get()))
        if self.active_step.failed.get():
            self.active_step.log_that_failed()

        #self.active_step.subscribe('fail', lambda: self.proxy().next_step())
        self.active_step.subscribe('solved', lambda: self.proxy().next_step())
        self.active_step.init().get()
        self.active_step.start_step().get()

    def repeat_step(self):
        self.active_step.repeat()

    def open_close_vhod(self):
        if not self.vhod_opened:
            self.room0.vhod.open_vhod()
            self.vhod_opened = True
            self.PUB.log(u"""
Входная дверь открыта.
""", log_level='legend')
        else:
            self.room0.vhod.close_vhod()
            self.vhod_opened = False
            self.PUB.log(u"""
Входная дверь закрыта.
""", log_level='legend')

    def control(self, data):
        control = data['control']
        splited = control.split('.')
        if len(splited) == 3:
            room, puzzle, method = splited
            puzzle = getattr(getattr(self, room), puzzle)
            getattr(puzzle, method)()
        elif len(splited) == 1:
            method = splited[0]
            getattr(self, method)()

    def open_all(self):
        self.room1.dver.open_dver()
        self.room0.vhod.open_vhod()
        self.room2.shluz.open_shluz()
        self.PUB.log(u"""
Все двери открыты. Чтобы закрыть их воспользуйтесь кнопками управления загадками(объектами) или начните все заново. На этапе 'Подготовка игры', все закроется.
""", log_level='legend')

    #def emerge_stop(self):
        #self.PUB.log(u"""
#Все движение остановлено. Чтобы продолжить игру нажмите кнопку 'Повторить шаг'.
#""", log_level='legend')

    def stop_game(self):
        pass

    def set_start_time(self, start_time):
        self.start_time = start_time

    def get_start_time(self):
        return self.start_time

    def set_game_config(self, config):
        self.game_config = config

    def set_game_config_players_2(self):
        self.game_config['players'] = 2
        self.PUB.log(u"Настройка изменена. Игроков: 2")

    def set_game_config_players_3(self):
        self.game_config['players'] = 3
        self.PUB.log(u"Настройка изменена. Игроков: 3")

    def set_game_config_players_4(self):
        self.game_config['players'] = 4
        self.PUB.log(u"Настройка изменена. Игроков: 4")

    def set_game_config_airship_tank(self):
        self.game_config['airship'] = 'tank'
        self.PUB.log(u"Настройка изменена. Дирижабль: Броненосный")

    def set_game_config_airship_fast(self):
        self.game_config['airship'] = 'fast'
        self.PUB.log(u"Настройка изменена. Дирижабль: Быстрый")

    def set_game_config_airship_shooter(self):
        self.game_config['airship'] = 'shooter'
        self.PUB.log(u"Настройка изменена. Дирижабль: Скорострельный")

    def set_game_config_lang_rus(self):
        self.game_config['lang'] = 'rus'
        self.PUB.log(u"Настройка изменена. Язык: Русский")

    def set_game_config_lang_eng(self):
        self.game_config['lang'] = 'eng'
        self.PUB.log(u"Настройка изменена. Язык: Английский")

    def get_game_config(self):
        return getattr(self, 'game_config', {})

    def ach_set(self, achpzl, snds, name):
        achpzl.achieve(name)
        if self.game_achivki[name]:
            return False
        else:
            self.game_achivki[name] = True
            snds.play('achivka')
            return True

    def ach_unset(self, name):
        self.game_achivki[name] = False

def get_config(name):
    GAME = Resources().require('game')
    config = GAME._actor.get_game_config()
    airship_speed = 1 if config.get('airship') == 'fast' else 0
    turel_recharge_speed = 100 if config.get('airship') == 'shooter' else 200
    rocket_damage = 25 if config.get('airship') == 'tank' else 50
    if config.get('players') == 2:
        rockets_count = 10
        disable_turel_after_hit = True
    elif config.get('players') == 3:
        rockets_count = 7
        disable_turel_after_hit = False
    elif config.get('players') == 4:
        rockets_count = 5
        disable_turel_after_hit = False

    lang = '' if config['lang'] == 'rus' else '_eng'
    players_count = config.get('players')

    config = {
        'airship_speed': airship_speed,
        'turel_recharge_speed': turel_recharge_speed,
        'rocket_damage': rocket_damage,
        'rockets_count': rockets_count,
        'disable_turel_after_hit': disable_turel_after_hit,
        'players': players_count,
        'lang': lang,
    }
    return config.get(name)



if __name__ == '__main__':
    run_game(Game)
