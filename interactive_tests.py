#! ../env/bin/python
# encoding: utf-8
import itertools
from pprint import pprint
from gevent import sleep

from utils import Resources, Scheduler, shex, load_device_addrs
from quest import Game
#from sounds import Sounds
import sounds
from eng_settings import ADRS



class DummyPUB(object):
    def log(self, msg, log_level=None):
        print msg

    def event(self, msg):
        print msg

    def step_event(self, msg):
        print msg


Resources().register('PUB', DummyPUB())

from engine import Sound
def test_sounds():
    #address = "tcp://{ip}:{port}".format(**ADRS['SND'])
    #sound = Resources().register('SOUND', Sound.start(address).proxy())

    sounds = Sounds()
    Resources().register('SNDS', sounds)
    return sounds

def reload_sounds():
    reload(sounds)
    return sounds.Sounds()


def test_game():
    Resources().register('PUB', DummyPUB())
    game = Game.start().proxy()
    Resources().register('game', game)
    sounds = Sounds()
    Resources().register('SNDS', sounds)
    game.game_init().get()
    return game, sounds

def get_rooms(game):
    rooms = []
    rooms.append(game.room0.get())
    rooms.append(game.room1.get())
    rooms.append(game.room2.get())
    rooms.append(game.room3.get())
    rooms.append(game.room4.get())
    rooms.append(game.room5.get())
    return rooms

from engine import run_game
def test_game_with_cli():
    run_game(Game)
    return lambda: Resources().require('game')

from net import Video
def test_video():
    address = "tcp://{ip}:{port}".format(**ADRS['VIDEO_PUB'])
    vid = Video.start(address).proxy()
    return vid


from engine import shex
from engine import ModbusDevice, MainDevice, RelayDevice, SensorDevice
from engine import LoadSDevice, SerialDevice, TestScriptDevice

def test_device():
    dv = ModbusDevice.start(1,0).proxy()
    dv.init().get()
    return dv

def test_dv_main():
    dv = MainDevice.start(1,0).proxy()
    dv.init().get()
    return dv

def test_dv_script():
    dv = TestScriptDevice.start(1).proxy()
    dv.init().get()
    dv.subscribe(
        'stat.script_status',
        lambda data: pprint(bin(data[0]))
    )
    dv.subscribe(
        'stat.script_step',
        lambda data: pprint(bin(data[0]))
    )
    return dv

def test_dv_relay():
    dv = RelayDevice.start(1,1).proxy()
    dv.init().get()
    return dv

def test_dv_loads():
    dv = LoadSDevice.start(1,2).proxy()
    dv.init().get()
    return dv

def test_dv_sensor(addr, slot):
    dv = SensorDevice.start(addr,slot).proxy()
    dv.init().get()
    return dv

def test_dv_serial():
    dv = SerialDevice.start(1,4).proxy()
    dv.init().get()
    return dv


def test_dummy():
    dv = DummyDevice.start(shex(25)).proxy()
    dv.init().get()

    dv.set_port(7, [
            shex(2)
        ])
    dv.set_port(6, [
            shex(2)
        ])
    dv.set_port(5, [
            shex(2)
        ])
    dv.set_port(4, [
            shex(2)
        ])
    handler = dv.subscribe(
        'stat.inputs',
        lambda data: pprint(bin(data[0]))
    )


from room0 import Achivki
def test_achivki():
    pzl = Achivki.start(1,3).proxy()
    pzl.init().get()
    pzl.reset()
    return pzl

from room0 import Vhod
def test_vhod(scheduler):
    pzl = Vhod.start(1,4).proxy()
    pzl.init().get()
    pzl.reset()
    def logic():
        pzl.setup()
    scheduler.schedule(logic, 2000, once=True)
    return pzl

from room1 import Dver
def test_dver(scheduler):
    pzl = Dver.start(7,1).proxy()
    pzl.init().get()
    pzl.reset()
    def logic():
        pzl.setup()
    scheduler.schedule(logic, 2000, once=True)
    return pzl

from room2 import Shluz
def test_shluz(scheduler):
    pzl = Shluz.start(12,1).proxy()
    pzl.init().get()
    pzl.reset()
    def logic():
        pzl.setup()
    scheduler.schedule(logic, 2000, once=True)
    return pzl


from room0 import ConfigPanel
def test_config(scheduler):
    pzl = ConfigPanel.start(1,1).proxy()
    pzl.init().get()
    pzl.reset()
    pzl.subscribe('start_game', lambda config: pprint("start_game: {0}".format(config)))
    pzl.subscribe('need_config', lambda data: pprint("need_config: {0}".format(data)))
    def logic():
        pzl.setup()
        pzl.activate()
    scheduler.schedule(logic, 2000, once=True)
    return pzl

from room3 import Pochinka
def test_pochinka(scheduler):
    pzl = Pochinka.start(14, 1, 14, 2).proxy()
    pzl.init().get()
    pzl.reset()
    pzl.subscribe('valve00_turned', lambda turns: pprint(turns))
    pzl.subscribe('bomb_loaded', lambda: pprint("bomb_loaded"))
    def logic():
        pzl.setup()
        pzl.activate()
        pzl.activate_bombpriem()
    scheduler.schedule(logic, 2000, once=True)
    return pzl

from room5 import Turel
def test_turel(scheduler):
    pzl = Turel.start(16).proxy()
    pzl.init().get()
    pzl.reset()
    def logic():
        pzl.setup()
        pzl.activate()
        pzl.subscribe('hole0', lambda holes: pprint("hole0: {0}".format(holes)))
        pzl.subscribe('hole1', lambda holes: pprint("hole1: {0}".format(holes)))
        pzl.subscribe('hole2', lambda holes: pprint("hole2: {0}".format(holes)))
        pzl.subscribe('hole3', lambda holes: pprint("hole3: {0}".format(holes)))
        pzl.subscribe('hole4', lambda holes: pprint("hole4: {0}".format(holes)))
        pzl.subscribe('hole5', lambda holes: pprint("hole5: {0}".format(holes)))
        pzl.subscribe('hole6', lambda holes: pprint("hole6: {0}".format(holes)))
        pzl.subscribe('hole7', lambda holes: pprint("hole7: {0}".format(holes)))
    scheduler.schedule(logic, 2000, once=True)
    return pzl

def test_turel_pochinka(scheduler):
    trl = Turel.start(18).proxy()
    trl.init().get()
    trl.reset()
    trl.activate()
    #trl.subscribe('hole0', lambda holes: pprint("hole0: {0}".format(holes)))
    #trl.subscribe('hole1', lambda holes: pprint("hole1: {0}".format(holes)))
    #trl.subscribe('hole2', lambda holes: pprint("hole2: {0}".format(holes)))
    #trl.subscribe('hole3', lambda holes: pprint("hole3: {0}".format(holes)))
    #trl.subscribe('hole4', lambda holes: pprint("hole4: {0}".format(holes)))
    #trl.subscribe('hole5', lambda holes: pprint("hole5: {0}".format(holes)))
    #trl.subscribe('hole6', lambda holes: pprint("hole6: {0}".format(holes)))
    #trl.subscribe('hole7', lambda holes: pprint("hole7: {0}".format(holes)))
    trl.subscribe('shot', lambda: pprint("shot event"))
    trl.subscribe('recharge', lambda: pprint("recharge event}"))
    check_holes = lambda: [func() for func in [
        lambda: trl.check_holes(),
    ]]
    scheduler.schedule(check_holes, 1000)

    pch = Pochinka.start(15, 1, 15, 2).proxy()
    pch.init().get()
    pch.reset()
    pch.activate()
    pch.subscribe('valve00_turned', lambda turns: trl.fix_hole(0, turns))
    pch.subscribe('valve01_turned', lambda turns: trl.fix_hole(1, turns))
    pch.subscribe('valve02_turned', lambda turns: trl.fix_hole(2, turns))
    pch.subscribe('valve03_turned', lambda turns: trl.fix_hole(3, turns))
    pch.subscribe('valve10_turned', lambda turns: trl.fix_hole(4, turns))
    pch.subscribe('valve11_turned', lambda turns: trl.fix_hole(5, turns))
    pch.subscribe('valve12_turned', lambda turns: trl.fix_hole(6, turns))
    pch.subscribe('valve13_turned', lambda turns: trl.fix_hole(7, turns))
    return trl, pch



from room1 import PultRadio
def test_pult(scheduler):
    pzl = PultRadio.start(6).proxy()
    pzl.init().get()
    pzl.reset()
    pzl.subscribe('tumbler_on', lambda: pprint("event: tumbler_on"))
    pzl.subscribe('crit_low', lambda: pprint("event: crit_low"))
    pzl.subscribe('crit_high', lambda: pprint("event: crit_height"))
    pzl.subscribe('height', lambda height: pprint("event: height={0}".format(height)))
    pzl.subscribe('help', lambda: pprint("event: help"))
    pzl.subscribe('mayaki_height', lambda: pprint("event: mayaki_height"))
    pzl.subscribe('solved', lambda: pprint("event: solved"))
    def logic():
        pzl.setup()
        pzl.activate()
    scheduler.schedule(logic, 2000, once=True)
    return pzl

from room1 import Karta
def test_karta(scheduler):
    pzl = Karta.start(3).proxy()
    pzl.init().get()
    pzl.reset()
    pzl.subscribe('solved', lambda: pprint("event: solved"))
    def logic():
        pzl.setup()
        pzl.activate()
        pzl.go_to_game()
    scheduler.schedule(logic, 2000, once=True)
    return pzl

from room1 import Telefon
def test_telefon(scheduler):
    pzl = Telefon.start(2).proxy()
    pzl.init().get()
    pzl.reset()
    pzl.subscribe('trubka', lambda: pprint("event: trubka"))
    pzl.subscribe('mayak', lambda data: pprint("event: mayak {0}".format(data)))
    def logic():
        pzl.setup('_eng')
        pzl.activate()
        pzl.go_to_game()
    scheduler.schedule(logic, 2000, once=True)
    return pzl

from room1 import Yakor
def test_yakor(scheduler):
    pzl = Yakor.start(2, 1).proxy()
    pzl.init().get()
    pzl.reset()
    pzl.subscribe('solved', lambda: pprint("event: yakor_raised"))

    def logic():
        pzl.setup()
        pzl.activate()
    scheduler.schedule(logic, 2000, once=True)
    return pzl

from room2 import Mahovik
def test_mahovik(scheduler):
    pzl = Mahovik.start(9, 4).proxy()
    pzl.init().get()
    pzl.reset()

    def logic():
        pzl.setup()
    scheduler.schedule(logic, 2000, once=True)
    return pzl



from room2 import Pech
def test_pech(scheduler):
    pzl = Pech.start(10).proxy()
    pzl.init().get()
    pzl.reset()
    def logic():
        pzl.activate()
        pzl.subscribe('achivka', lambda: pprint("event: achivka"))
        pzl.subscribe('wrong_poleno', lambda: pprint("event: wrong_poleno"))
        pzl.subscribe('solved', lambda: pprint("event: solved"))
        pzl.go_to_game()
    scheduler.schedule(logic, 2000, once=True)
    return pzl

from room2 import Mastermind
def test_mastermind():
    pzl = Mastermind.start(11).proxy()
    pzl.init().get()
    pzl.reset()
    pzl.activate()
    pzl.subscribe('solved', lambda: pprint("event: solved"))
    return pzl


from room1 import Gotovnost
def test_gotovnost():
    pzl = Gotovnost.start(4, 1).proxy()
    pzl.init().get()
    pzl.reset()
    return pzl

from room3 import Okulyar
def test_okulyar(scheduler):
    pzl = Okulyar.start(19).proxy()
    pzl.init().get()
    pzl.reset()
    pzl.setup()
    move_tick = lambda: [func() for func in [
        lambda: pzl.move_by_route(),
        lambda: pprint('move_tick')
    ]]
    scheduler.schedule(move_tick, 1000)
    return pzl

from room4 import Ugol
def test_okl_ugl(scheduler):
    okl = Okulyar.start(15).proxy()
    okl.init().get()
    okl.reset()
    okl.setup()

    ugl = Ugol.start(17).proxy()
    ugl.init().get()
    ugl.reset()
    def logic():
        ugl.setup()
        ugl.activate_utils()
        ugl.activate_fight()
        ugl.go_to_game()

        ugl.subscribe('bomb_launch', lambda: pprint("event: bomb_launch"))
        ugl.subscribe('bomb_hit', lambda: pprint("event: bomb_hit"))
        ugl.subscribe('bomb_miss', lambda: pprint("event: bomb_miss"))
        ugl.subscribe('bomb_recharge', lambda: pprint("event: bomb_recharge"))
        ugl.subscribe('donotpress', lambda: pprint("event: donotpress"))
        ugl.subscribe('help', lambda: pprint("event: help"))

        move_tick = lambda: [func() for func in [
            lambda: okl.move_by_route(ugl),
        ]]
        scheduler.schedule(move_tick, 1000)
    scheduler.schedule(logic, 3000, once=True)
    return okl, ugl

def test_ugol(scheduler):
    ugl = Ugol.start(16).proxy()
    ugl.init().get()
    ugl.reset()
    ugl.setup()
    ugl.activate()
    ugl.subscribe('bomb_launch', lambda: pprint("event: bomb_launch"))
    ugl.subscribe('bomb_hit', lambda: pprint("event: bomb_hit"))
    ugl.subscribe('bomb_miss', lambda: pprint("event: bomb_miss"))
    ugl.subscribe('bomb_recharge', lambda: pprint("event: bomb_recharge"))
    ugl.subscribe('donotpress', lambda: pprint("event: donotpress"))
    ugl.subscribe('help', lambda: pprint("event: help"))

    coords = [0,1,2,3,4,5]
    coords_gen = itertools.cycle(coords)

    move_tick = lambda: [func() for func in [
        lambda: ugl.set_coords(next(coords_gen)),
    ]]
    scheduler.schedule(move_tick, 1000)
    return ugl
