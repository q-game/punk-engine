# encoding: utf-8
import socket
#host = socket.gethostbyname
def host(*args):
    return '127.0.0.1'

ADRS = {
        'SND': {
            'ip': host('qg_punk_snd'),
            #'ip': socket.gethostbyname('sp-oper.dlink'),
            'port': '50000'
        },
        'VHAL': {
            'ip': host('qg_hal'),
            'port': '20000'
        },
        'ENG': {
            'ip': host('qg_eng'),
        },
        'CLI_SRV': {
            'ip': host('qg_eng'),
            'port': '30000'
        },
        'CLI_PUB': {
            'ip': host('qg_eng'),
            'port': '40000'
        },
        'VIDEO_PUB': {
            'ip': '0.0.0.0',
            'port': '25555'
        },
        'LOGS': {
            'ip': host('qg_logger'),
            'port': '25001'
        }
}
