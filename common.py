# encoding: utf-8

def reverse_profile(profile):
    profile = profile[:]
    reversed_profile = []
    while len(profile):
        segment = [profile.pop(-3), profile.pop(), profile.pop()]
        reversed_profile += segment
    return reversed_profile

