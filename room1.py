# encoding: utf-8
from functools import partial
from pprint import pprint

from utils import Scheduler

from engine import Resources, shex
from engine import Puzzle
from engine import SerialDevice, KartaDevice, ScriptDevice, PultDevice
from engine import TelefonDevice, SensorDevice, LoadSDevice


class Gotovnost(Puzzle):
    glossary_name = u"Табло готовности(пере капитанским мостиком)"
    glossary_code = u"G2R1O2"

    def __init__(self, device_addr, slot, *args, **kwargs):
        super(Gotovnost, self).__init__(*args, **kwargs)
        self.device_addr = device_addr
        self.slot = slot
        self.gotovnost = [
            0, 0, 0, 0, 0,
            0, 0, 0, #null lines
        ]
        self.gotovnost_map = {
            'engine': 0 ,       'generator': 1 ,
            'toplivo': 2 ,       'yakor': 3 ,
            'ballons': 4 ,
        }

    def on_start(self):
        self.device = SerialDevice.start(self.device_addr, self.slot).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        pass

    def reset(self):
        self.gotovnost = [
            0, 0, 0, 0, 0,
            0, 0, 0, #null lines
        ]
        self.device.set_lines(0, self.gotovnost)

    def jackpot(self):
        self.device.set_lines(0, [
            10, 10, 10, 10, 10,
            10, 10, 10,
        ])

    def gotov(self, *gotovs):
        for gotov in gotovs:
            self.gotovnost[self.gotovnost_map[gotov]] = 10
        self.device.set_lines(0, self.gotovnost)

    gotov_engine = lambda self: self.gotov('engine')
    gotov_toplivo = lambda self: self.gotov('toplivo')
    gotov_ballons = lambda self: self.gotov('ballons')
    gotov_generator = lambda self: self.gotov('generator')
    gotov_yakor = lambda self: self.gotov('yakor')

    def deactivate(self):
        pass

    def stopall(self):
        self.device.stop()


class Karta(Puzzle):
    glossary_name = u"Карта с маяками"
    glossary_code = u"G2R1O0"

    def __init__(self, device_addr, slot=None, **kwargs):
        super(Karta, self).__init__(**kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = KartaDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        pass

    def reset(self):
        self.device.send_cmd('reset')

    def go_to_game(self):
        self.device.go_to_game()

    def go_to_solved(self):
        self.trigger('solved')
        self.device.go_to_solved()

    def activate(self):
        self.device.subscribe(
            'stat.script_status',
            lambda data: self.proxy().status_event(data[0]),
            2000
        ).get()
        self.device.subscribe(
            'stat.achivka',
            lambda data: self.proxy().achivka_event(data[0]),
            2000
        ).get()

    def achivka_event(self, event):
        if event:
            self.trigger('achivka')

    def status_event(self, event):
        if event == 2:
            self.trigger('solved')

    def set_mayak(self, number):
        self.device.write_regs('conf.mayak{0}'.format(number), [1])

    def get_mayaks(self):
        return self.device.read_regs('conf.mayak0', 8).get()

    def light_crash(self):
        self.device.write_regs('conf.light', [0])
        self.device.write_regs('conf.light_rand', [40])

    def light_dontpress(self):
        self.device.write_regs('conf.light', [30])
        self.device.write_regs('conf.light_rand', [0])

    def light_on(self):
        self.device.write_regs('conf.light', [100])
        self.device.write_regs('conf.light_rand', [0])

    def light_off(self):
        self.device.write_regs('conf.light', [0])
        self.device.write_regs('conf.light_rand', [0])

    def deactivate(self):
        self.device.unsubscribe('stat.script_status')
        self.device.unsubscribe('stat.achivka')
        self.unsubscribe('solved')

    def stopall(self):
        self.device.stop()


class Vagonetki(Puzzle):
    glossary_name = u"Вагонетки"
    glossary_code = u"G2R1O4"

    def __init__(self, device_addr, slot=None, **kwargs):
        super(Vagonetki, self).__init__(**kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = ScriptDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        pass

    def go_to_standby(self):
        self.device.go_to_standby()

    def go_to_game(self):
        self.device.go_to_game()

    def go_to_solved(self):
        self.trigger('solved')
        self.device.go_to_solved()

    def reset(self):
        self.device.send_cmd('reset')

    def activate(self):
        self.device.subscribe(
            'stat.script_status',
            lambda data: self.proxy().status_event(data[0]),
            2000
        ).get()

    def status_event(self, event):
        if event == 2:
            self.trigger('solved')

    def deactivate(self):
        self.device.unsubscribe('stat.script_status')
        self.unsubscribe('solved')

    def stopall(self):
        self.device.stop()


#struct script_status_struct{
#struct script_common_status_struct{
#uint16_t status; // Статус скрипта. 0 - не запущен, 1 - выполняется, 2 - завершен
#uint16_t step; // Номер текущего шага
#} common;
#uint16_t mayak[8]; // У активированных маяков - 1
#uint16_t trubka; // Висит - 0, поднята - 1. Нужно для выдачи сообщения о недостаточной высоте для связи с маяком
#};

#struct script_control_struct{
#struct script_common_control_struct{
#uint16_t command; // Командный регистр для скрипта
#uint16_t reserve; // 
#} common;
#uint16_t language; // Язык 0 - русский, 1 - английскмй
#};
class Telefon(Puzzle):
    glossary_name = u"Телефон(секретер)+якорь+сидушки"
    glossary_code = u"G2R1O6"

    def __init__(self, device_addr, slot=None, **kwargs):
        super(Telefon, self).__init__(**kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = TelefonDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self, lang=''):
        if lang == '_eng':
            self.device.write_regs('conf.language', [1]).get()
        else:
            self.device.write_regs('conf.language', [0]).get()

    def reset(self):
        self.device.send_cmd('reset')

    def go_to_standby(self):
        self.device.go_to_standby()

    def go_to_game(self):
        self.device.go_to_game()

    def go_to_solved(self):
        self.device.go_to_solved()

    def activate(self):
        self.device.subscribe(
            'stat.trubka',
            lambda data: self.proxy().trubka_event(data[0]),
            1000
        ).get()

    def trubka_event(self, event):
        print "trubka_event: {0}".format(event)
        if event == 0:
            self.trigger('trubka')
            mayaki = self.device.read_regs('stat.mayak0', 8).get()
            pprint(mayaki)
            for mayak, val in enumerate(mayaki):
                if val == 0:
                    continue
                elif val == 1:
                    self.trigger('mayak', mayak)
                    break
            else:
                self.trigger('mayak', -1)


    def deactivate(self):
        self.device.unsubscribe('stat.trubka')
        self.unsubscribe('trubka')
        self.unsubscribe('mayak')

    def stopall(self):
        self.device.stop()


class Yakor(Puzzle):
    glossary_name = u"Якорь"
    glossary_code = u"G2R1O7"

    def __init__(self, device_addr, slot, *args, **kwargs):
        super(Yakor, self).__init__(*args, **kwargs)
        self.device_addr = device_addr
        self.slot = slot

        self.force_players_count  = False

    def on_start(self):
        self.device = SensorDevice.start(self.device_addr, self.slot).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        pass

    def go_to_solved(self):
        self.trigger('solved')

    def reset(self):
        self.device.send_cmd('reset')

    def activate(self):
        self.device.subscribe(
            'stat.line0_counter_tozero',
            lambda data: self.proxy().line_event(data[0]),
            2000
        ).get()

    def force_all_players_sat(self):
        self.force_players_count = True

    def get_players_count(self):
        if self.force_players_count:
            return 4

        pl1 = self.device.read_regs('stat.line1_status').get()[0]
        pl2 = self.device.read_regs('stat.line2_status').get()[0]
        pl3 = self.device.read_regs('stat.line3_status').get()[0]
        pl4 = self.device.read_regs('stat.line4_status').get()[0]
        return len(filter(lambda val: not val, [pl1, pl2, pl3, pl4]))

    def line_event(self, line):
        if line:
            self.trigger('solved')

    def deactivate(self):
        self.device.unsubscribe('stat.line0_counter_tozero')
        self.unsubscribe('solved')


class Sidushki(Puzzle):
    glossary_name = u"Сидушки"
    glossary_code = u"G2R1O7"

    def __init__(self, device_addr, slot, *args, **kwargs):
        super(Sidushki, self).__init__(*args, **kwargs)
        self.device_addr = device_addr
        self.slot = slot

    def on_start(self):
        self.device = LoadSDevice.start(self.device_addr, self.slot).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.setup_line(2)

    def reset(self):
        self.device.send_cmd('reset')

    def vibro_on(self):
        self.device.line_value(2, 1)

    def vibro_off(self):
        self.device.line_value(2, 0)


class PultRadio(Puzzle):
    glossary_name = u"Пульт+радио"
    glossary_code = u"G2R1O3"
    #device_addr = ?

    track_map = {
        'novost1':     1,
        'novost2':     2,
        'novost3':     3,
        'novost1_eng': 4,
        'novost2_eng': 5,
        'novost3_eng': 6,
    }

    def __init__(self, device_addr, slot=None, **kwargs):
        super(PultRadio, self).__init__(**kwargs)
        self.device_addr = device_addr

        self.crit_low = True
        self.crit_high = False
        self.vzlet = False

        self.scheduler = Scheduler()
        self.override_height = False
        self.override_height_clbk = lambda: self.trigger('height', 3400)
    def on_start(self):
        self.device = PultDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self, speed=0):
        self.device.write_regs('conf.speed', [speed])

    def reset(self):
        self.device.send_cmd('reset')

    def go_to_game(self):
        self.device.go_to_game()

    def go_to_solved(self):
        self.trigger('solved')
        self.device.go_to_solved()

    def disable_handles(self):
        self.device.write_regs('conf.enable', [0])

    def enable_handles(self):
        self.device.write_regs('conf.enable', [1])

    def can_reach_lodka(self):
        self.device.write_regs('conf.lodka', [1])

    def cant_reach_lodka(self):
        self.device.write_regs('conf.lodka', [0])

    def activate_script(self):
        self.device.subscribe(
            'stat.script_status',
            lambda data: self.proxy().status_event(data[0]),
            2000
        ).get()

    def achivka_event(self, event):
        print 'uletel', event
        if event:
            self.trigger('uletel')

    def activate_wind(self):
        self.device.subscribe(
            'stat.script_step',
            lambda data: self.proxy().wind_event(data[0]),
            2000
        ).get()
        self.device.subscribe(
            'stat.uletel',
            lambda data: self.proxy().achivka_event(data[0]),
            2000
        ).get()

    def activate_tumbler(self):
        self.device.subscribe(
            'conf.tumbler_switched',
            lambda data: self.proxy().tumbler_event(data[0]),
            2000
        ).get()

    def activate_height(self):
        self.device.subscribe(
            'stat.height',
            lambda data: self.proxy().height_event(data[0]),
            2000
        ).get()

    def activate_mayaki_height(self):
        self.device.subscribe(
            'stat.height_stable',
            lambda data: self.proxy().height_stable_event(data[0]),
            2000
        ).get()

    def activate_help(self):
        self.device.subscribe(
            'stat.help',
            lambda data: self.proxy().help_event(data[0]),
            2000
        ).get()

    def switch_tumbler(self):
        self.device.write_regs('conf.tumbler_switched', [1])

    def deactivate(self):
        self.device.unsubscribe('stat.script_status')
        self.device.unsubscribe('conf.tumbler_switched')
        self.device.unsubscribe('stat.height')
        self.device.unsubscribe('stat.height_stable')
        self.device.unsubscribe('stat.help')
        self.device.unsubscribe('stat.uletel')
        self.unsubscribe('solved')
        self.unsubscribe('tumbler_on')
        self.unsubscribe('help')
        self.unsubscribe('crit_low')
        self.unsubscribe('crit_high')
        self.unsubscribe('mayaki_height')

    def status_event(self, event):
        if event == 2:
            self.trigger('solved')

    def tumbler_event(self, event):
        if event:
            self.trigger('tumbler_on')

    def help_event(self, event):
        if event:
            self.trigger('help')

    def wind_event(self, event):
        if event:
            self.trigger('wind')

    def height_event(self, height):
        print 'height_event', height
        if self.override_height:
            return

        if height > 10000:
            return

        if not self.vzlet:
            self.vzlet = True
            self.trigger('vzlet')

        self.trigger('height', height)
        if height < 500:
            if not self.crit_low:
                self.crit_low = True
                self.trigger('crit_low')
        elif height > 4500:
            if not self.crit_high:
                self.crit_high = True
                self.trigger('crit_high')
        else:
            self.crit_high = False
            self.crit_low = False

    def height_stable_event(self, event):
        if event == 1:
            height = self.device.read_regs('stat.height').get()[0]
            if 2800 <= height <= 4500:
                self.trigger('mayaki_height')

    def get_height(self):
        if self.override_height:
            return 3400

        return self.device.read_regs('stat.height').get()[0]

    def manual_speed_forward(self):
        self.device.write_regs('conf.speed_override', [1, 2])

    def manual_speed_stop(self):
        self.device.write_regs('conf.speed_override', [1, 1])

    def manual_speed_back(self):
        self.device.write_regs('conf.speed_override', [1, 0])

    def manual_speed_disable(self):
        self.device.write_regs('conf.speed_override', [0])

    def manual_height_up(self):
        self.device.write_regs('conf.height_override', [1, 2])

    def manual_height_stop(self):
        self.device.write_regs('conf.height_override', [1, 1])

    def manual_height_down(self):
        self.device.write_regs('conf.height_override', [1, 0])

    def manual_height_disable(self):
        self.device.write_regs('conf.height_override', [0])

    def set_override_height(self):
        if not self.override_height:
            self.override_height = True
            self.scheduler.schedule(self.override_height_clbk, 2000)

    def unset_override_height(self):
        if self.override_height:
            self.override_height = False
            self.scheduler.unschedule(self.override_height_clbk)

    def breakage(self):
        #команда на аварийное снижение
        self.device.write_regs('conf.breakage', [1])

    def repaired(self):
        self.device.write_regs('conf.breakage', [0])

    def set_track(self, track_name):
        track = self.track_map[track_name]
        self.device.write_regs('conf.audio', [track])


class Dver(Puzzle):
    glossary_name = u"Дверь в машинный зал"
    glossary_code = u"G2R1O5"

    def __init__(self, device_addr, slot, *args, **kwargs):
        super(Dver, self).__init__(*args, **kwargs)
        self.device_addr = device_addr
        self.slot = slot

    def on_start(self):
        self.device = LoadSDevice.start(self.device_addr, self.slot).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.setup_line(0,value=1)
        self.device.setup_line(1)

    def reset(self):
        self.device.send_cmd('reset')

    def open_dver(self):
        self.device.line_value(0, 0)

    def close_dver(self):
        self.device.line_value(0, 1)

    def plate_on(self):
        self.device.line_value(1, 1)

    def plate_off(self):
        self.device.line_value(1, 0)


