# encoding: utf-8
from pprint import pprint
import json
import signal
import sys
from datetime import datetime
import copy
import types

import pykka
import pykka.debug
import gevent
import zmq.green as zmq
from pykka.gevent import GeventActor as Actor
from pykka.gevent import GeventFuture as Future

from net import Requester, Responder, Publisher
from net import zmq_ctx

from eng_settings import ADRS

from utils import Resources, Scheduler, Specs, shex

import pudb
pudb.DEFAULT_SIGNAL = None

from utils import ModifyFilter, PUBHandler, log_formatter
import logging
log = logging.getLogger('ENG')
log.setLevel(logging.ERROR)
log.addFilter(ModifyFilter())

pykka_log = logging.getLogger('pykka')
pykka_log.setLevel(logging.ERROR)
pykka_log.addFilter(ModifyFilter())

rm1_log = logging.getLogger('[rm1]')
rm1_log.setLevel(logging.ERROR)
rm1_log.addFilter(ModifyFilter(prefix='[rm1]', name='ENG'))

rm2_log = logging.getLogger('[rm2]')
rm2_log.setLevel(logging.ERROR)
rm2_log.addFilter(ModifyFilter(prefix='[rm2]', name='ENG'))

#logger_sock = zmq_ctx.socket(zmq.PUB)
#logger_sock.connect('tcp://{ip}:{port}'.format(**ADRS['LOGS']))
#handler = PUBHandler(logger_sock)
#handler.setFormatter(log_formatter)

#log.addHandler(handler)
#rm1_log.addHandler(handler)
#pykka_log.addHandler(handler)

console_handler = logging.StreamHandler()
console_handler.setFormatter(log_formatter)
#log.addHandler(console_handler)
#rm1_log.addHandler(console_handler)
pykka_log.addHandler(console_handler)

log.info(u'PING!')


class Events(object):
    def __init__(self):
        self.subs = {}
        self.once = []

    def subscribe(self, event, handlers, once=False):
        self.subs.setdefault(event, [])
        if type(handlers) is list:
            self.subs[event] += handlers
        else:
            self.subs[event].append(handlers)

        if once:
            self.once.append(event)

        return handlers # чтобы можно было удалить конкретный handler события

    def unsubscribe(self, event=None, handlers=None):

        def unsub(handlers_list, handler):
            try:
                handlers_list.remove(handler)
            except ValueError:
                pass

        if event is None:
            self.subs = {}

        if handlers:
            if isinstance(handlers, types.FunctionType):
                callback = handlers
                handlers = []
                handlers.append(callback)
            for handler in list(handlers):
                unsub(self.subs[event], handler)
        else:
            self.subs[event] = []

    def trigger(self, event, data=None):
        #import pudb; pu.db
        #print unicode(self)
        handlers = self.subs.get(event, None)
        if not handlers:
            log.warn('No handlers for such event: %s' % event)
            return

        if event in self.once:
            self.unsubscribe(event)

        for handle in handlers:
            if data is not None:
                handle(data)
            else:
                handle()

def make_regs_map(regs, stat_offset, conf_offset):
    REGS = {}
    # hack to update and return dict at once
    upd = lambda orig, new: (orig.update(new), orig,)[1]

    REGS['stat'] = { attrs['slug']: upd(attrs, {'addr': stat_offset + addr})
                     for addr, attrs in regs['stat'].iteritems()
                     if attrs is not None }
    REGS['conf'] = { attrs['slug']: upd(attrs, {'addr': conf_offset + addr})
                     for addr, attrs in regs['conf'].iteritems()
                     if attrs is not None }
    CMDS =         { attrs['slug']: upd(attrs, {'code': code})
                     for code, attrs in regs['cmd'].iteritems()
                     if attrs is not None  }
    return REGS, CMDS

DEVICE_SPECS = Specs().get('device_spec')
Resources().register('device_spec', DEVICE_SPECS)

def get_regs_map(dv_name, slot):
    dv_regs = copy.deepcopy(DEVICE_SPECS[dv_name])
    stat_offset = DEVICE_SPECS['slots_offset']['slot{0}_stat_offset'.format(slot)]
    conf_offset = DEVICE_SPECS['slots_offset']['slot{0}_conf_offset'.format(slot)]
    return make_regs_map(dv_regs, stat_offset, conf_offset)

# представление девайса в движке
# определяет поведение девайса
# берет на себя всю ответственность за связь с ХАЛ,
# и своим представление в ХАЛ
class ModbusDevice(Actor, Events):
    device_type = None

    def __init__(self, device_addr, slot, hal=None):
        if self.device_type is None:
            raise Exception('device_type must be set for device')

        super(ModbusDevice, self).__init__()
        Events.__init__(self)

        if hal is None:
            hal = Resources().require('HAL.default_modbus')
        self.hal =  hal
        self.device_addr = device_addr
        self.slot = slot
        self.inited = None

        self.REGS, self.CMDS = get_regs_map(self.device_type, self.slot)

    def proxy(self):
        return self.actor_ref.proxy()

    def init(self):
        if self.inited:
            return True

        resp = self.hal.request({
            'type': 'system',
            'cmd': 'init',
            'kwargs': {
                'device_addr': self.device_addr,
                'slot': self.slot,
            }
        }).get()
        if resp.get('status') == 'success':
            self.my_address = "tcp://{0}:{1}".format(ADRS['ENG']['ip'], resp.get('eng_device_port'))
            self.my_responder = Responder.start(self.my_address, self.actor_ref).proxy()

            self.hal_device_address = "tcp://{0}:{1}".format(ADRS['VHAL']['ip'], resp.get('hal_device_port'))
            self.hal_device = Requester.start(self.hal_device_address).proxy()
            print 'Device inited. Dv: {0} {1}'.format(self.slot, self.device_addr)
            self.inited = True
            return True
        elif resp.get('status') == 'fail':
            self.trigger('fail', [self, None])
            print 'Device failed! Dv: {0} {1}'.format(self.slot, self.device_addr)
            print resp
            self.inited = False
            return False
        else:
            self.inited = False
            print 'DEVICE TOTALY FAILED!!! Dv: {0} {1}'.format(self.slot, self.device_addr)

    #def reset(self):
        #self.send_cmd('reset')

    def reg(self, register_name):
        group, register = register_name.split('.')
        return self.REGS[group][register]['addr']

    def cmd(self, cmd_name):
        return self.CMDS[cmd_name]['code']

    def read_regs(self, register_name, registers_number=1, **kwargs):
        return self.mb_read(
            self.reg(register_name),
            registers_number=registers_number,
            **kwargs
        )

    def write_regs(self, register_name, data=None, **kwargs):
        return self.mb_write(
            self.reg(register_name),
            data=data,
            **kwargs
        )

    def send_cmd(self, cmd):
        if isinstance(cmd, (str, unicode)):
            cmd_code = self.cmd(cmd)
        else:
            cmd_code = cmd
        self.write_regs('conf.command', [cmd_code])

    def send2hal(self, msg):
        if not hasattr(self, 'hal_device'):
            return {'status': 'fail', 'info': 'Mock response because device init failed.'}
        resp = self.hal_device.request(msg).get()
        if resp.get('status', None) == u'fail':
            self.trigger('fail', [self, resp])
        return resp


    def mb_read(self, register_addr=None, registers_number=1, **kwargs):
        if register_addr is None:
            raise Exception("'register_addr' must not be None")

        resp = self.send2hal({
            'type': 'cmd',
            'cmd': 'read',
            'device_addr': self.device_addr,
            'register_addr': register_addr,
            'registers_number': registers_number
        })
        if resp.get('status', None) == u'success':
            return resp.get('data', None)
        else:
            #FIXME: не слишком ли жестко exception делать?
            raise Exception('READ operation failed.')

    def mb_write(self, register_addr=None, data=None, **kwargs):
        if register_addr is None:
            raise Exception("'register_addr' must not be None")
        if data is None:
            raise Exception("'data' must not be None")

        resp = self.send2hal({
            'type': 'cmd',
            'cmd': 'write',
            'device_addr': self.device_addr,
            'register_addr': register_addr,
            'registers_number': len(data),
            'data': data
        })

        if resp.get('status', None) == u'success':
            return True
        else:
            #import pudb; pu.db
            #FIXME: не слишком ли жестко exception делать?
            raise Exception('WRITE operation failed.')

    def subscribe(self, register_addr, handlers, time_interval=1000, **kwargs):
        #import pudb; pu.db
        try:
            int(register_addr, 16)
        except ValueError:
            try:
                register_addr = self.reg(register_addr)
            except ValueError:
                return super(ModbusDevice, self).subscribe(register_addr, handlers)

        self.send2hal({
            'type': 'sub',
            'device_addr': self.device_addr,
            'register_addr': register_addr,
            'time_interval': time_interval
        })
        return super(ModbusDevice, self).subscribe(register_addr, handlers)


    def unsubscribe(self, register_addr=None, handlers=None, **kwargs):
        if not (register_addr is None):
            try:
                int(register_addr, 16)
            except ValueError:
                try:
                    register_addr = self.reg(register_addr)
                except ValueError:
                    return super(ModbusDevice, self).unsubscribe(register_addr, handlers)

        super(ModbusDevice, self).unsubscribe(register_addr, handlers)
        if not self.subs[register_addr]:
            self.send2hal({
                'type': 'unsub',
                'device_addr': self.device_addr,
                'register_addr': register_addr,
            })


    def on_receive(self, msg):
        if msg.get('request', None):
            return self.handle(msg['request'])

    def handle(self, request):
        if request.get('type') == 'event':
            self.trigger(request.get('register_addr'), request.get('data', None))
        elif request.get('type') == 'fail':
            #import pudb; pu.db
            self.trigger('fail', [self, None])


    def stopall(self):
        if not self.inited:
            return
        self.subs = None
        self.hal_device.stop()
        resp = self.hal.request({
            'type': 'system',
            'cmd': 'destroy',
            'kwargs': {
                'device_addr': self.device_addr,
                'slot': self.slot,
            }
        }).get()
        #FIXME: почему в ответ приходит None
        #if resp['status'] == 'fail':
            #log.info(resp['info'])

        self.my_responder.stop()

    def on_stop(self):
        log.debug(u'Stop ModbusDevice: {0}'.format(self.device_addr))
        self.stopall()

    def on_failure(self, exception_type, exception_value, traceback):
        #import pudb; pu.db
        log.error(u"ModbusDevice %s failed: %s: %s" % (self.device_addr, exception_type, exception_value))
        self.stopall()


class RelayDevice(ModbusDevice):
    device_type = "RELAY"

    def on(self):
        self.write_regs('conf.line0', [1])

    def off(self):
        self.write_regs('conf.line0', [0])


class SensorDevice(ModbusDevice):
    device_type = "SENSOR"

    def reset_counter(self, line):
        self.send_cmd('reset_counter_{0}'.format(line))


class LoadSDevice(ModbusDevice):
    device_type = "LOAD_S"

    def start_shim(self, line0=None, line1=None, line2=None, line3=None):
        masks = self.CMDS['start_shim']['masks']
        L0, L1, L2, L3 = masks['line0'], masks['line1'], masks['line2'], masks['line3']
        shift = reduce(lambda shift, line: shift | (line[0] if line[1] else 0),
                       [(L0, line0), (L1, line1), (L2, line2), (L3, line3)] ,0 )
        self.send_cmd(self.cmd('start_shim') + shift)

    def stop_shim(self, line0=None, line1=None, line2=None, line3=None):
        masks = self.CMDS['stop_shim']['masks']
        L0, L1, L2, L3 = masks['line0'], masks['line1'], masks['line2'], masks['line3']
        shift = reduce(lambda shift, line: shift | (line[0] if line[1] else 0),
                       [(L0, line0), (L1, line1), (L2, line2), (L3, line3)] ,0 )
        self.send_cmd(self.cmd('stop_shim') + shift)

    def setup_line(self, line, work_mode='normal', freq=200, value=0,
                   log_scale='linear', vel=5000, time=0):
        work_mode = self.REGS['conf']['line{0}_work_mode'.format(line)]['modes'][work_mode]
        log_scale = self.REGS['conf']['line{0}_log_scale'.format(line)]['scales'][log_scale]
        #velH, velL = divmod(vel, int('0xFFFF', 16))
        #timeH, timeL = divmod(time, int('0xFFFF', 16))
        #CHECKIT:
        velH, velL = divmod(vel, int('0x10000', 16))
        timeH, timeL = divmod(time, int('0x10000', 16))
        self.write_regs('conf.line{0}_work_mode'.format(line),
                        [work_mode, freq, value, log_scale, velL, velH, timeL, timeH])

    def line_value(self, line, value):
        self.write_regs('conf.line{0}_value'.format(line), [value])

    def setup_profile(self, line, segments, repeat=0):
        count = len(segments)/2
        self.write_regs('conf.line{0}_segments_number'.format(line),
                        [count] + [repeat] + segments)

from itertools import izip

def pairwise(iterable):
    "s -> (s0,s1), (s2,s3), (s4, s5), ..."
    a = iter(iterable)
    return izip(a, a)

class SerialDevice(ModbusDevice):
    device_type = "SERIAL"

    def get_lines(self, line, number=2):
        assert number > 1 and divmod(number,2)[1] == 0
        regs = self.read_regs('stat.lines{0}_{1}'.format(line, line+1), number/2)
        lines = []
        for reg in regs:
            linea, lineb = divmod(reg, int('0x100', 16))
            lines.extend([linea, lineb])
        return lines

    def set_lines(self, line, values):
        assert len(values) > 1 and divmod(len(values),2)[1] == 0
        assert divmod(line, 2)[1] == 0
        regs = []
        for lineb, linea in pairwise(values):
            reg = (linea<<8) + lineb
            regs.append(reg)

        return self.write_regs('conf.lines{0}_{1}'.format(line, line+1), regs)


class MainDevice(ModbusDevice):
    device_type = "MAIN"

    def beep(self):
        self.send_cmd('beep')


class ScriptDevice(MainDevice):
    #device_type = None
    device_type = "SCRIPT"

    def __init__(self, device_addr, slot=None, hal=None):
        super(ScriptDevice, self).__init__(device_addr, slot=0, hal=hal)

    def send_script_cmd(self, cmd):
        if isinstance(cmd, (str, unicode)):
            cmd_code = self.cmd(cmd)
        else:
            cmd_code = cmd
        self.write_regs('conf.script_command', [cmd_code])

    def go_to_standby(self):
        self.send_script_cmd('go_to_standby')

    def go_to_game(self):
        self.send_script_cmd('go_to_game')

    def go_to_solved(self):
        self.send_script_cmd('go_to_solved')


class TestScriptDevice(ScriptDevice):
    device_type = "SCRIPT"

class TurelDevice(ScriptDevice):
    device_type = "TUREL"

class PechDevice(ScriptDevice):
    device_type = "PECH"

class MindDevice(ScriptDevice):
    device_type = "MIND"

class OkulyarDevice(ScriptDevice):
    device_type = "OKULYAR"

class UgolDevice(ScriptDevice):
    device_type = "UGOL"

class KartaDevice(ScriptDevice):
    device_type = "KARTA"

class PultDevice(ScriptDevice):
    device_type = "PULT"

class BalansDevice(ScriptDevice):
    device_type = "BALANS"

class TelefonDevice(ScriptDevice):
    device_type = "TELEFON"

class Puzzle(Actor, Events):
    glossary_name = None
    glossary_code = None

    def __init__(self, *args, **kwargs):
        super(Puzzle, self).__init__(*args, **kwargs)
        Events.__init__(self)
        self.PUB = Resources().require('PUB')

    def init(self):
        pass

    def activate(self):
        pass

    def proxy(self):
        return self.actor_ref.proxy()

    def stopall(self):
        pass

    def on_stop(self):
        log.debug(u"Stop Puzzle %s" % self)
        self.stopall()

    def on_failure(self, exception_type, exception_value, traceback):
        #import pudb; pu.db
        log.error(u"Puzzle %s failed: %s: %s" % (self, exception_type, exception_value))
        self.stopall()

    def __str__(self):
        return u"%s %s" % (unicode(self.glossary_code), unicode(self.glossary_name))


# обработчик запросов от клиентов
class Handler(Actor):
    def __init__(self, status):
        super(Handler, self).__init__()
        self.status = status
        self.GAME = Resources().require('game')
        self.game_started = None

    def on_receive(self, msg):
        if msg.get('request', None):
            return self.handle(msg['request'])

    def handle(self, request):
        print "Handle request: {0}".format(request)
        if request.get("type", None) == "cmd":
            func_name = request.get("cmd").replace('::', '_')
            func = getattr(self, func_name, None)
            if not func:
                print "Error! No such func"
                return {
                    "status": "fail",
                    "time": str(datetime.now())
                }

            data = request.get('data', None)
            result = None
            if data:
                result = func(data)
            else:
                result = func()

        return {
            "status": "success",
            "time": str(datetime.now()),
            "result": result

        }

    def system_ping(self):
        return 'pong'

    def game_init(self):
        print "game_init"
        self.game_started = True
        steps = self.GAME.game_init().get()
        result = []
        for step in steps:
            result.append({
                'id': step.step.get(),
                'name': unicode(step.name.get())
            })

        return result

    def game_prepare_new(self):
        steps = self.GAME.prepare_new_game().get()
        result = []
        for step in steps:
            result.append({
                'id': step.step.get(),
                'name': unicode(step.name.get())
            })
        return result

    def game_start(self):
        steps = self.GAME.game_start().get()
        result = []
        for step in steps:
            result.append({
                'id': step.step.get(),
                'name': unicode(step.name.get())
            })
        return result

    def game_repeat_step(self):
        self.GAME.repeat_step()

    def game_next_step(self):
        self.GAME.next_step(debounce=True)

    def game_open_close_vhod(self):
        self.GAME.open_close_vhod()

    def game_open_all(self):
        self.GAME.open_all()

    def game_control(self, data):
        self.GAME.control(data)

    def game_emerge_stop(self):
        self.GAME.emerge_stop()


    def game_stop(self):
        print "game_stop"
        if self.game_started:
            self.GAME.stop_game()
            self.status['stopped'] = True
            print "status stopped"
            print self.status['stopped']

def sigint_stop(status):
    status['stopped'] = True

def shutdown():
    g = gevent.spawn_later(5, lambda: zmq_ctx.destroy())
    pykka.ActorRegistry.stop_all(True)
    g.kill()
    print "Termination of zmq context..."
    zmq_ctx.destroy()
    sys.exit(0)

class Sound(Requester):
    def play(self, full_name, routes=None, volume=None, loop=None, paused=None):
        return self.request({
            'type': 'cmd',
            'cmd': 'play',
            'full_name': full_name,
            'routes': routes,
            'volume': volume,
            'loop': loop,
            'paused': paused,
        })

    def stop_play(self, full_name):
        return self.request({
            'type': 'cmd',
            'cmd': 'stop',
            'full_name': full_name,
        })

    def resume(self, full_name):
        return self.request({
            'type': 'cmd',
            'cmd': 'resume',
            'full_name': full_name,
        })

    def pause(self, full_name):
        return self.request({
            'type': 'cmd',
            'cmd': 'pause',
            'full_name': full_name,
        })

    def route(self, full_name, routes):
        return self.request({
            'type': 'cmd',
            'cmd': 'route',
            'full_name': full_name,
            'routes': routes
        })

    def unroute(self, full_name, routes):
        return self.request({
            'type': 'cmd',
            'cmd': 'unroute',
            'full_name': full_name,
            'routes': routes
        })

    def shutdown(self):
        return self.request({
            'type': 'system',
            'cmd': 'shutdown'
        })

    def check_cpu(self):
        return self.request({
            'type': 'cmd',
            'cmd': 'checkcpu',
        })

from sounds import Sounds
from net import Video
def run_game(game):
    try:
        Resources().register('scheduler', Scheduler())
        scheduler = Resources().require('scheduler')

        address = "tcp://{ip}:{port}".format(**ADRS['VHAL'])
        Resources().register('HAL.default_modbus', Requester.start(address).proxy())
        HAL = Resources().require('HAL.default_modbus')
        HAL.request({
            'type': 'system',
            'cmd': 'config',
            'kwargs': {
                'engine_ip': ADRS['ENG']['ip'],
            }
        })

        address = "tcp://{ip}:{port}".format(**ADRS['SND'])
        sound = Resources().register('SOUND', Sound.start(address).proxy())

        address = "tcp://{ip}:{port}".format(**ADRS['VIDEO_PUB'])
        video = Resources().register('VIDEO', Video.start(address).proxy())

        sounds = Sounds()
        Resources().register('SNDS', sounds)

        address = "tcp://{ip}:{port}".format(**ADRS['CLI_PUB'])
        PUB = Publisher.start(address).proxy()
        Resources().register('PUB', PUB)
        Resources().register('game', game.start().proxy())
        status = {"stopped": False}
        handler = Handler.start(status)
        address = "tcp://{ip}:{port}".format(**ADRS['CLI_SRV'])
        srv = Responder.start(address, handler).proxy()

        print 'Registering signal handler...'
        gevent.signal(signal.SIGINT, sigint_stop, status)
        gevent.signal(signal.SIGTERM, shutdown)
        while not status.get('stopped'):
            gevent.sleep(0.05)
            #print pykka.ActorRegistry.get_all()

        HAL = Resources().require('HAL.default_modbus')
        HAL.request({'type': 'system', 'cmd': 'shutdown'})
        HAL.stop()

        PUB.log("Game stopping.")
        PUB.stop()
        srv.stop()
        handler.stop()
        sound.shutdown()
        video.stop_video()

        Resources().require('game').stop()

    except (KeyboardInterrupt, SystemExit):
        print "System interrupt..."
    finally:
        try:
            HAL = Resources().require('HAL.default_modbus')
            HAL.request({'type': 'system', 'cmd': 'shutdown'})
            HAL.stop()

            PUB.log("Game stopping.")
            PUB.stop()
            srv.stop()
            handler.stop()
            sound.shutdown()

            Resources().require('game').stop()
        finally:
            g = gevent.spawn_later(5, lambda: zmq_ctx.destroy())
            pykka.ActorRegistry.stop_all(True)
            g.kill()
            zmq_ctx.destroy()
            print "Termination of zmq context..."
            zmq_ctx.destroy()


# основной скрипт, вынести в отдельный файл
if __name__ == '__main__':
    run_game(Game)




