# encoding: utf-8
from pprint import pprint
from functools import partial

from engine import Resources, shex
from engine import Puzzle
from engine import SerialDevice, LoadSDevice, SensorDevice


class Achivki(Puzzle):
    glossary_name = u"Ачивки перед входной дверью"
    glossary_code = u"G2R0O0"
    #device_addr = 1
    #slot = 3

    def __init__(self, device_addr, slot, *args, **kwargs):
        super(Achivki, self).__init__(*args, **kwargs)
        self.device_addr = device_addr
        self.slot = slot
        self.achivki = [
            0, 0, 0, 0, 0, 0,
            0, 0, #null lines
            0, 0, 0, 0, 0, 0,
            0, 0, #null lines
            0, 0, 0, 0, 0, 0,
            0, 0, #null lines
            #ranks
            0, 0, 0, 0, 0, 0, 0, 0,
        ]
        self.achivki_map = {
            u'Поехали!': 0 ,
            u'Быстрый старт': 1 ,
            u'Фанат': 2 ,
            u'1 на 360': 3 ,
            u'Энерджайзер': 4 ,
            u'Подгоревший': 5 ,
            u'Чистая энергия': 8 ,
            u'Икар': 9 ,
            u'Никто не забыт': 10 ,
            u'Настоящий друг': 11 ,
            u'На краю света': 12 ,
            u'Подрывник': 13 ,
            u'Снайпер': 16 ,
            u'Ласточка': 17 ,
            u'Прагматик': 18 ,
            u'Механик': 19 ,
            u'Сила воли': 20 ,
            u'Крепкий орешек': 21 ,

            u'rank_1': 24,
            u'rank_2': 25,
            u'rank_3': 26,
            u'rank_4': 27,
            u'rank_5': 28,
            u'rank_6': 29,
        }

    def on_start(self):
        self.device = SerialDevice.start(self.device_addr, self.slot).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        pass

    def reset(self):
        self.device.send_cmd('reset')
        self.achivki = [
            0, 0, 0, 0, 0, 0,
            0, 0, #null lines
            0, 0, 0, 0, 0, 0,
            0, 0, #null lines
            0, 0, 0, 0, 0, 0,
            0, 0, #null lines
            #ranks
            0, 0, 0, 0, 0, 0, 0, 0,
        ]
        self.device.set_lines(0, self.achivki)

    def jackpot(self):
        self.device.set_lines(0, [
            10, 10, 10, 10, 10, 10,
            0, 0,
            10, 10, 10, 10, 10, 10,
            0, 0,
            10, 10, 10, 10, 10 ,10
        ])

    def achieve(self, *achieves):
        for ach in achieves:
            self.achivki[self.achivki_map[ach]] = 10
        self.device.set_lines(0, self.achivki)

    def rank(self, rank):
        if rank is 0:
            return

        self.achivki[self.achivki_map['rank_{0}'.format(rank)]] = 10
        self.device.set_lines(0, self.achivki)

    def deactivate(self):
        pass

    def stopall(self):
        self.device.stop()


class Vhod(Puzzle):
    glossary_name = u"Вход"
    glossary_code = u"G2R0O1"

    def __init__(self, device_addr, slot, *args, **kwargs):
        super(Vhod, self).__init__(*args, **kwargs)
        self.device_addr = device_addr
        self.slot = slot

    def on_start(self):
        self.device = LoadSDevice.start(self.device_addr, self.slot).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.setup_line(0,value=1)
        self.device.setup_line(1)

    def reset(self):
        self.device.send_cmd('reset')

    def open_vhod(self):
        self.device.line_value(0, 0)

    def close_vhod(self):
        self.device.line_value(0, 1)

    def plate_on(self):
        self.device.line_value(1, 1)

    def plate_off(self):
        self.device.line_value(1, 0)


class ConfigPanel(Puzzle):
    glossary_name = u"Вход"
    glossary_code = u"G2R0O1"

    def __init__(self, device_addr, slot, *args, **kwargs):
        super(ConfigPanel, self).__init__(*args, **kwargs)
        self.device_addr = device_addr
        self.slot = slot

    def on_start(self):
        self.device = SensorDevice.start(self.device_addr, self.slot).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        pass

    def reset(self):
        self.device.send_cmd('reset')

    def activate(self):
        self.device.subscribe(
            'stat.line5_counter_tozero',
            lambda data: self.proxy().start_event(data[0]),
            2000
        ).get()

    def start_event(self, start):
        if start:
            config = {}
            player1 = self.device.read_regs('stat.line0_status').get()[0]
            player2plus = self.device.read_regs('stat.line1_status').get()[0]
            airship_tank = self.device.read_regs('stat.line2_status').get()[0]
            airship_fast = self.device.read_regs('stat.line3_status').get()[0]
            airship_shooter = self.device.read_regs('stat.line4_status').get()[0]

            if player1 == 0:
                config['players'] = 2
            elif player2plus == 0:
                config['players'] = 4
            else:
                config['players'] = 4
                self.trigger('need_config', 'players')

            if airship_fast == 0:
                config['airship'] = 'fast'
            elif airship_shooter == 0:
                config['airship'] = 'shooter'
            elif airship_tank == 0:
                config['airship'] = 'tank'
            else:
                config['airship'] = 'shooter'
                self.trigger('need_config', 'airship')

            config['lang'] = 'rus'

            self.trigger('start_game', config)

    def trigger_start(self):
        self.start_event(True)

    def deactivate(self):
        self.device.unsubscribe('stat.line5_counter_tozero')
        self.unsubscribe('start_game')
        self.unsubscribe('need_config')
