#! ../env/bin/python
# encoding: utf-8
from pprint import pprint
import json
import signal
import sys

import pykka
import pykka.debug
import gevent
import zmq.green as zmq

from pykka.gevent import GeventActor as Actor

from net import Requester, Responder, Publisher, Subscriber
from net import zmq_ctx

from utils import Resources, Scheduler, debug

from hal_settings import ADRS

from utils import ModifyFilter, PUBHandler, log_formatter
import logging

log = logging.getLogger('HAL')
log.setLevel(logging.ERROR)
log.addFilter(ModifyFilter())

pykka_log = logging.getLogger('pykka')
pykka_log.setLevel(logging.ERROR)
pykka_log.addFilter(ModifyFilter())

net_log = logging.getLogger('[net]')
net_log.setLevel(logging.ERROR)
net_log.addFilter(ModifyFilter(prefix='[net]', name='HAL'))

#logger_sock = zmq_ctx.socket(zmq.PUB)
#logger_sock.connect('tcp://{ip}:{port}'.format(**ADRS['LOGS']))
#handler = PUBHandler(logger_sock)
#handler.setFormatter(log_formatter)

#log.addHandler(handler)
#net_log.addHandler(handler)
#pykka_log.addHandler(handler)

console_handler = logging.StreamHandler()
console_handler.setFormatter(log_formatter)
log.addHandler(console_handler)
net_log.addHandler(console_handler)
pykka_log.addHandler(console_handler)



import signal
#import pudb
#pudb.DEFAULT_SIGNAL = signal.SIGUSR2

# виртайльный девайс, абстрагирует ограничения шины
# по которой HAL связывается с реальными девайсами
class VirtDevice(Actor):
    def __init__(self, device_addr=None, slot=None, eng_device_port=None, my_port=None, bal=None, **kwargs):
        super(VirtDevice, self).__init__()
        self.device_addr = device_addr
        self.slot = slot
        self.id = self.make_id(self.device_addr, self.slot)
        self.bal = bal
        self.eng_device_port = eng_device_port
        self.eng_device_address = "tcp://{0}:{1}".format(kwargs.get('engine_ip'), self.eng_device_port)
        self.my_port = my_port
        self.my_address = "tcp://{0}:{1}".format(ADRS['VHAL']['ip'], self.my_port)
        self._registers = {}

    def __str__(self):
        return str(self.id)

    @classmethod
    def make_id(self, device_addr, slot):
        return "{0}-slot{1:02}".format(int(device_addr), int(slot))

    def on_start(self):
        self.eng_device = Requester.start(self.eng_device_address).proxy()
        self.my_responder = Responder.start(self.my_address, self.actor_ref).proxy()


    def on_stop(self):
        #import pudb; pu.db
        self.destroy()
        self.eng_device.stop()
        self.my_responder.stop()

    def proxy(self):
        return self.actor_ref.proxy()

    def send2eng(self, msg):
        self.eng_device.request(msg)

    def on_receive(self, msg):
        if msg.get('request', None):
            return self.handle(msg['request'])

    def handle(self, request):
        if request.get('type') == 'cmd':
            #log.debug('Got new cmd: {0}'.format(str(request)))
            return self.send2bal(request)
        elif request.get('type') == 'sub':
            # указываем на какие события подписываевся
            # "активируем" девайс, чтобы он постоянно опрашивался
            #log.debug('Got new sub: {0}'.format(str(request)))
            return self.subscribe(request)
        elif request.get('type') == 'unsub':
            # указываем от какого события отписываемся
            # "деактивируем" девайс
            #log.debug('Got new unsub: {0}'.format(request.get('register_addr')))
            return self.unsubscribe(request)
        elif request.get('type') == 'event':
            #log.debug('Got new event: {0}'.format(str(request)))
            self.send2eng(request)
        elif request.get('type') == 'fail':
            log.error('Got fail: {0}'.format(str(request)))
            self.send2eng(request)

    def send2bal(self, request):
        return self.bal.request(request).get()

    def check_register(self, addr):
        resp = self.bal.request({
            'type': 'cmd',
            'cmd': 'read',
            'device_addr': self.device_addr,
            'register_addr': addr,
            'registers_number': 1
        }).get()

        if resp.get('status', None) == 'fail':
            self.proxy().handle({
                'type': 'fail',
                'device_addr': self.device_addr,
            })
            self.unsubscribe({'register_addr': addr})
            return

        if not (addr in self._registers):
            self.unsubscribe({'register_addr': addr})
            return

        value = resp.get('data')[0]
        #log.debug("Check_register: value=%s" % value)
        #log.debug("Previous value: %s" % self._registers[addr]['value'])
        if not (self._registers[addr]['value'] == value):
            self.proxy().handle({
                'type': 'event',
                'device_addr': self.device_addr,
                'register_addr': addr,
                'data': [value]
            })
            self._registers[addr]['value'] = value

    def subscribe(self, request):
        register_addr = request.get('register_addr')
        scheduler = Resources().require('scheduler')
        func = lambda: self.proxy().check_register(register_addr)
        resp = self.bal.request({
            'type': 'cmd',
            'cmd': 'read',
            'device_addr': self.device_addr,
            'register_addr': register_addr,
            'registers_number': 1
        }).get()

        if resp.get('status', None) == 'fail':
            return {'status': 'fail'}

        self._registers[register_addr] = {
                'value': resp.get('data')[0],
                'func': func
        }
        interval = int(request.get('time_interval'))
        #FIXME: if subscribe 3 times - can't fully unsubscibe
        scheduler.schedule(func, interval)
        return {'status': 'success'}

    def unsubscribe(self, request):
        #import pudb; pu.db
        register_addr = request.get('register_addr', None)
        scheduler = Resources().require('scheduler')

        funcs = list()
        if register_addr:
            if self._registers.get(register_addr, None):
                funcs.append((self._registers[register_addr]['func'], register_addr))
        else:
            funcs += [(self._registers[register_addr]['func'], register_addr)
                     for register_addr in self._registers.keys()]

        for func, register_addr in funcs:
            del self._registers[register_addr]
            scheduler.unschedule(func)

        return {'status': 'success'}

    def init(self):
        resp = self.bal.request({
            'type': 'cmd',
            'cmd': 'read',
            'device_addr': self.device_addr,
            #TODO: в зависимости от слота будут разные адреса
            #TODO: более надженый способ что девайс работает, специальный регистр со статусом?
            'register_addr': '0x0000',
            'registers_number': 1
        }).get()
        if resp.get('status', None) == 'success':
            return True
        else:
            return False


    def destroy(self):
        #import pudb; pu.db
        scheduler = Resources().require('scheduler')
        log.debug("Destroing of device {0}".format(self))
        #TODO: нужно ли тут что-то передавать девайсу?
        for key, value in self._registers.iteritems():
            func = value['func']
            scheduler.unschedule(func)
        self._registers = {}
        return True

# Bus Abstraction Layer
# абстракция Modbus, по сути Requester 
# который хранит в себе данный для связи 
class BAL(Actor):
    def __init__(self):
        super(BAL, self).__init__()
        address = "tcp://{ip}:{port}".format(**ADRS['VBAL'])
        self.requester = Requester.start(address).proxy()

    def request(self, request):
        for _ in range(0, 3):
            resp = self.requester.request(request).get()
            if resp and resp.get('status') == 'fail':
                continue
            else:
                break
        if resp:
            return resp
        else:
            return {'status': 'fail'}

    def on_stop(self):
        self.requester.request({'type': 'system', 'cmd': 'shutdown'}).get()
        self.requester.stop()


# обработчик запросов от движка
class EngHandler(Actor):
    def __init__(self, target, status):
        super(EngHandler, self).__init__()
        self.target = target
        self.status = status

    def on_receive(self, msg):
        if msg.get('request', None):
            return self.handle(msg['request'])

    def handle(self, request):
        #log.debug("Handle request: {0}".format(request))
        if request.get('type') == 'system':
            if request.get('cmd') == 'shutdown':
                #import pudb; pu.db
                self.status['stop'] = True
                self.target.stop_devices().get()
                return
            if request.get('cmd') == 'reset':
                #import pudb; pu.db
                self.target.stop_devices().get()
                return
            if request.get('cmd') == 'init':
                kwargs = request.get('kwargs')
                return self.target.init_device(**kwargs).get()
            if request.get('cmd') == 'destroy':
                kwargs = request.get('kwargs')
                #import pudb; pu.db
                return self.target.destroy_device(**kwargs).get()
            if request.get('cmd') == 'config':
                kwargs = request.get('kwargs')
                config = self.target.config.get()
                config.update(**kwargs)
                return {'status': 'success', 'config': config }



# внутренняя логика ХАЛ, тут будут хранится
# экземпляры вирт девайсов и определятся как 
# с помощью них будут опрашивться реальный двеайся
# абстрагирует ограничение шины на необходимость
# постоянно опрашивать девайсы, т.е. от них нельзя получать события,
# только опрашивать изменилось ли что
class HAL(Actor) :
    def __init__(self, bal):
        super(HAL, self).__init__()
        self.bal = bal
        self.config = {}

    def on_start(self):
        self._check_bal()

        self.last_dv_port = int(ADRS['VHAL']['port'])
        self.devices = dict()

    def _check_bal(self):
        self.bal.request({'type': 'system', 'cmd': 'ping'}).get()

    def _get_port4device(self):
        self.last_dv_port += 2
        return self.last_dv_port - 1, self.last_dv_port

    def init_device(self, **kwargs):
        device_addr = kwargs.get('device_addr')
        slot = kwargs.get('slot')
        device_id = VirtDevice.make_id(device_addr, slot)
        if self.devices.get('device_id'):
            return {'status': 'fail', 'info': 'Device with this device_id({0}) already inited.'.format(device_id)}

        eng_device_port, hal_device_port = self._get_port4device()
        device = VirtDevice.start(**{
            'device_addr': device_addr,
            'slot': slot,
            'eng_device_port': eng_device_port,
            'my_port': hal_device_port,
            'bal': self.bal,
            'engine_ip': self.config.get('engine_ip')
        }).proxy()
        success = device.init().get()

        if success:
            self.devices[device_id] = device
            #TODO: тут нужен slot?
            return {'status': 'success', 'device_addr': device_addr,
                    'eng_device_port': eng_device_port,
                    'hal_device_port': hal_device_port}
        else:
            return {'status': 'fail', 'info': 'Failed to init physical device: %s' % device_id }

    def destroy_device(self, **kwargs):
        device_addr = kwargs.get('device_addr')
        slot = kwargs.get('slot')
        device_id = VirtDevice.make_id(device_addr, slot)
        device = self.devices.get(device_id)
        if not device:
            return {'status': 'fail', 'info': 'No device with this device_id({0}).'.format(device_id)}

        try:
            device.stop()
        except pykka.Timeout:
            self.devices.pop(device_id)
            return {'status': 'fail', 'info': 'Exceeded timeout to destroy physical device: {0}'.format(device_id) }
        except pykka.ActorDead:
            self.devices.pop(device_id)
            return {'status': 'fail', 'info': 'Physical device already destroyed: {0}'.format(device_id) }


        self.devices.pop(device_id)
        return {'status': 'success', 'device_addr': device_addr}

    def stop_devices(self, *args, **kwags):
        for device_id, device in self.devices.iteritems():
            try:
                device.stop().get()
            except pykka.Timeout:
                log.error('Exceeded timeout to destroy physical device: {0}'.format(device_id))
            except pykka.ActorDead:
                log.error('Physical device already destroyed: {0}'.format(device_id))

    def on_stop(self):
        self.stop_devices()




def sigint_stop(status):
    print 'SIGINT'
    status['stop'] = True

def shutdown():
    print 'SIGTERM'
    g = gevent.spawn_later(5, lambda: zmq_ctx.destroy())
    pykka.ActorRegistry.stop_all(True)
    g.kill()
    zmq_ctx.destroy()
    sys.exit(0)


if __name__ == '__main__':
    #zmq_ctx = zmq.Context()
    try:
        #signal.signal(signal.SIGUSR1, pykka.debug.log_thread_tracebacks)
        #import pudb; pu.db
        bal = BAL.start().proxy()
        hal = HAL.start(bal).proxy()
        status = {"stop": False}
        eng_handler = EngHandler.start(hal, status)
        address = "tcp://{ip}:{port}".format(**ADRS['VHAL'])
        responder = Responder.start(address, eng_handler).proxy()

        Resources().register('scheduler', Scheduler())

        print 'Registering signal handler...'
        gevent.signal(signal.SIGINT, sigint_stop, status)
        gevent.signal(signal.SIGTERM, shutdown)
        while not status.get('stop'):
            gevent.sleep(0.05)

        responder.stop()
        eng_handler.stop()
        hal.stop()
        #TODO: корректно стопить bal если он уже отвалился
        bal.stop()


    except (KeyboardInterrupt, SystemExit):
        log.debug("System interrupt...")
    finally:
        g = gevent.spawn_later(5, lambda: zmq_ctx.destroy())
        pykka.ActorRegistry.stop_all(True)
        g.kill()
        zmq_ctx.destroy()

