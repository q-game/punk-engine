# encoding: utf-8
import gevent
import pykka
import yaml
import logging

class ModifyFilter(logging.Filter):
    def __init__(self, *args, **kwargs):
        super(ModifyFilter, self).__init__()
        self.prefix = kwargs.get('prefix', '[   ]')
        self.name = kwargs.get('name', None)

    def filter(self, record):
        record.prefix = self.prefix
        record.short_levelname = {
            logging.DEBUG: 'DEBG',
            logging.INFO: 'INFO',
            logging.WARNING: 'WARN',
            logging.ERROR: 'ERRR',
            logging.CRITICAL: 'CRIT'
        }.get(record.levelno, '????')
        if self.name:
            record.name = self.name

        return True

class PUBHandler(logging.Handler):
    def __init__(self, sock):
        super(PUBHandler, self).__init__()
        self.sock = sock

    def emit(self, record):
        msg = self.format(record)
        try:
            self.sock.send_string(msg)
        except Exception as e:
            #import pudb; pu.db
            raise e

log_formatter = logging.Formatter("%(asctime)s|%(name)s%(prefix)s|%(short_levelname)s|%(message)s")

def debug():
    actors = pykka.ActorRegistry.get_all()
    import pudb; pu.db

#функция для преобразования десятичного числа в 16ричное представление ввиде строки
def shex(num):
    # если число отрицательное "какбы" преобразовываем его к signed int 16
    # нужно для того чтобы bal не задумывался, что и куда он передает
    if num < 0:
        num += 1 << 16
    return '0x{:04X}'.format(num)


class ResourceError(Exception):
    def __init__(self,  msg):
        self.msg = msg

    def __str__(self):
        return self.msg

    def __unicode__(self):
        return unicode(str(self))

import os
import yaml
from jinja2 import Environment, FileSystemLoader

PATH = os.path.dirname(os.path.abspath(__file__))

to_int = lambda val: int(val, 16)

def reg_addr(offset, addr, offsetN='0x0000', N=0, offsetS='0x0000', S=0):
    _ = to_int
    return '0x{0:04X}'.format((_(offset) + _(addr) + _(offsetN)*N + _(offsetS)*S))

def get_jinja_env():
    jenv = Environment(
        autoescape=False,
        trim_blocks=True,
        keep_trailing_newline=True,
        loader=FileSystemLoader(os.path.join(PATH, 'specs')),
        auto_reload=True,
        extensions=[
            "jinja2.ext.do",
            "jinja2.ext.loopcontrols",
            "jinja2.ext.with_",
        ]

    )
    jenv.filters['hex'] = to_int
    jenv.globals['reg'] = reg_addr
    #jenv.tests['test_func'] = test_func
    return jenv

class Specs(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Specs, cls).__new__(cls)
            cls.instance.jenv = get_jinja_env()
        return cls.instance

    def _render(self, spec, context=None):
        return self.jenv.get_template(spec).render(context or {})

    def get(self, spec_name, context=None):
        spec = self._render(spec_name + '.yaml', context)
        if False:
            specfile = open(os.path.join(PATH, 'specs/_device_spec.yaml'),'w')
            specfile.write(spec)
            specfile.close()
            return None

        return yaml.load(spec)


class Resources(object):
    resources = dict()
    # Singleton
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Resources, cls).__new__(cls)
        return cls.instance

    def register(self, name, resource):
        if self.resources.get(name, None):
            print "Resource already registered!!"
            #raise ResourceError("Resource already registered!")

        self.resources[name] = resource
        return resource

    def require(self, name):
        resource = self.resources.get(name, None)
        if resource is None:
            raise ResourceError("No such resource!")

        return resource

class Scheduler(object):
    def __init__(self):
        self.plan = {}

    def _run(self, func):
        attrs = self.plan.get(func, None)
        if attrs:
            func()
            if attrs.get('once'):
                self.unschedule(func)
                return
            gevent.spawn_later(attrs['interval'], lambda: self._run(func))

    def schedule(self, func, interval, once=False):
        """Schedule func call in future

        For multiple callbacks use such construction:
            scheduler.schedule(
                lambda: [func() for func in [
                    lambda: pprint(1),
                    lambda: pprint(2)]
                ]
                1100,
                once=True
            )
        """
        interval = float(interval)/1000
        self.plan[func] = {
            'interval': interval,
            'once': once
        }
        gevent.spawn_later(interval, lambda: self._run(func))
        return func


    def unschedule(self, func):
        #import pudb; pu.db
        if func in self.plan:
            del self.plan[func]
        else:
            print u"No function {0} to unschedule".format(func)

def load_device_addrs():
    with open('./specs/device_addrs.yaml', 'r') as stream:
        dv_addrs = yaml.safe_load(stream)
    return dv_addrs

